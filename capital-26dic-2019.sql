-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-12-2019 a las 20:00:07
-- Versión del servidor: 5.5.64-MariaDB
-- Versión de PHP: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `capital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptos_torre`
--

CREATE TABLE IF NOT EXISTS `aptos_torre` (
  `id` int(10) unsigned NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `torre` varchar(100) NOT NULL,
  `naptos` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptos_torre`
--

INSERT INTO `aptos_torre` (`id`, `id_proyecto`, `torre`, `naptos`) VALUES
(1, 1, 'Torre1', 10),
(2, 1, 'Torre2', 10),
(3, 2, 'Torre1', 10),
(4, 2, 'Torre2', 10),
(5, 3, 'Torre1', 5),
(6, 3, 'Torre2', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campos_entrega`
--

CREATE TABLE IF NOT EXISTS `campos_entrega` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `apto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `propietario` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `cedula` int(100) NOT NULL,
  `torre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `parqueadero` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `c_util` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `proyecto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `municipio` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `energia` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `acueducto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `gas` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `municipio2` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `ingeniero` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `firma1` text COLLATE latin1_spanish_ci NOT NULL,
  `firma2` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `archivo` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `campos_entrega`
--

INSERT INTO `campos_entrega` (`id`, `idapto`, `apto`, `propietario`, `cedula`, `torre`, `parqueadero`, `c_util`, `proyecto`, `direccion`, `municipio`, `energia`, `acueducto`, `gas`, `municipio2`, `ingeniero`, `firma1`, `firma2`, `fecha`, `archivo`) VALUES
(45, 3, '102', 'PEPE', 6868, 'Torre1', '67', '67', 'PROY1', '567', 'MEDELLIN', '67', '67', '67', 'MEDELLIN', 'LEYLA', 'uzNiSI6nyz.png', 'kNb3VKxuMS.png', '27 de Noviembre del 2019', 'ENTREGA_INMUEBLE_1522134836.pdf'),
(48, 11, '102', 'alejo', 1234, 'Torre1', '1', '1', 'orquideas', 'medellin', 'antioquia', '12', '13', '14', 'antioquia', 'Inj.Alejo', 'k2g6SI2bvc.png', 'L8AlSBi2j6.png', 'clientes de Noviembre del 2019', 'ENTREGA_INMUEBLE_164647241.pdf'),
(52, 70, '504', 'carlos a', 1234566, 'Torre2', '1', '1', 'a1', 'calle2', 'Medellín', '2', '56', '1', 'mede', '', 'Lf93RExRP4.png', 'o9vgN8QeHG.png', '2 de Diciembre del 2019', 'ENTREGA_INMUEBLE_554422923.pdf'),
(53, 71, '101', 'qwe', 123, 'Torre1', '1', '1', 'b1', '1', '1', '1', '1', '1', '1', 'sdasdasdasdas', 'vJCQeiWf9o.png', 'T1ZmVAKN1v.png', '23 de Diciembre del 2019', 'ENTREGA_INMUEBLE_712579416.pdf'),
(57, 1, '101', 'PEDRO PEREZ', 798797889, 'Torre1', '67', '67', 'PROY1', 'CR 35', 'MEDELLIN', '78', '78', '78', 'MEDELLIN', 'LEYLA', 'tk2rtwBWz9.png', 'bfJc21rETQ.png', '12 de Diciembre del 2019', 'ENTREGA_INMUEBLE_721161838.pdf'),
(60, 23, '201', 'q', 1, 'Torre1', '1', '1', 'Bitall', 'medellin', 'anqui', '1', '1', '1', 'ASD1', 'PEPITO', 'cOZ4L5clwE.png', '2uamKuv1H5.png', '1 de Diciembre del 2019', 'ENTREGA_INMUEBLE_1958238921.pdf'),
(62, 22, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 'FKgw7jTaQ6.png', 'Vya0eY57aa.png', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracs_custom`
--

CREATE TABLE IF NOT EXISTS `caracs_custom` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracs_default`
--

CREATE TABLE IF NOT EXISTS `caracs_default` (
  `id` int(10) unsigned NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura`
--

CREATE TABLE IF NOT EXISTS `nomenclatura` (
  `id` int(10) unsigned NOT NULL,
  `idtorre` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `desde_piso` int(11) NOT NULL,
  `hasta_piso` int(11) NOT NULL,
  `naptos` int(11) NOT NULL,
  `nomenc1` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nomenc2` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nomenclatura` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nomenclatura`
--

INSERT INTO `nomenclatura` (`id`, `idtorre`, `idproyecto`, `desde_piso`, `hasta_piso`, `naptos`, `nomenc1`, `nomenc2`, `nomenclatura`) VALUES
(1, 1, 1, 2, 4, 2, '1', '2', ''),
(2, 1, 1, 1, 1, 2, '1', '2', ''),
(3, 2, 1, 1, 5, 2, '1', '2', ''),
(4, 3, 2, 1, 5, 2, '1', '2', ''),
(5, 4, 2, 1, 5, 2, '3', '4', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura_history`
--

CREATE TABLE IF NOT EXISTS `nomenclatura_history` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `clase` int(11) NOT NULL,
  `nomenclatura` int(11) NOT NULL,
  `reforma` varchar(100) NOT NULL,
  `obs_generales` text NOT NULL,
  `imgs_generales` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `entrega` int(11) NOT NULL,
  `entregado` int(11) NOT NULL,
  `desistido` int(11) NOT NULL,
  `informe` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nomenclatura_history`
--

INSERT INTO `nomenclatura_history` (`id`, `idapto`, `clase`, `nomenclatura`, `reforma`, `obs_generales`, `imgs_generales`, `aprobado`, `entrega`, `entregado`, `desistido`, `informe`, `fecha`) VALUES
(1, 22, 2, 0, 'hm4EEezOBp.xls,Q6n1ALwGN1.xls,71Tyv5LpGT.xls,', '', '', 1, 1, 0, 0, 'historico_264178734.pdf', '2019-12-17 06:45:38'),
(2, 21, 2, 0, 'au8130u1rF.xls,', '', '', 0, 0, 0, 0, 'historico_1212895683.pdf', '2019-12-17 06:46:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura_oficial`
--

CREATE TABLE IF NOT EXISTS `nomenclatura_oficial` (
  `id` int(10) unsigned NOT NULL,
  `idtorre` int(11) NOT NULL,
  `nomenclatura` int(11) NOT NULL,
  `clase` int(11) NOT NULL,
  `reforma` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `obs_generales` text COLLATE latin1_spanish_ci NOT NULL,
  `imgs_generales` text COLLATE latin1_spanish_ci NOT NULL,
  `aprobado` int(11) NOT NULL,
  `entrega` int(11) NOT NULL,
  `entregado` int(11) NOT NULL COMMENT '0 cuando el propietario no lo ha aprobado, 1 cuando todo está 100% ok',
  `desistido` int(11) NOT NULL,
  `informe` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nomenclatura_oficial`
--

INSERT INTO `nomenclatura_oficial` (`id`, `idtorre`, `nomenclatura`, `clase`, `reforma`, `obs_generales`, `imgs_generales`, `aprobado`, `entrega`, `entregado`, `desistido`, `informe`, `fecha`) VALUES
(1, 1, 101, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:33'),
(2, 1, 102, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:33'),
(3, 1, 202, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:45'),
(4, 1, 201, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:45'),
(5, 1, 302, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:45'),
(6, 1, 301, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:45'),
(7, 1, 401, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:45'),
(8, 1, 402, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:45'),
(9, 1, 501, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:52'),
(10, 1, 502, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:05:52'),
(11, 2, 101, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(12, 2, 102, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(13, 2, 202, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(14, 2, 301, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(15, 2, 201, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(16, 2, 302, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(17, 2, 401, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(18, 2, 402, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(19, 2, 501, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(20, 2, 502, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-17 05:06:00'),
(21, 3, 101, 2, 'aAJOu1a8NC.xls,NeC9chLSvZ.xls,au8130u1rF.xls,', '', '', 0, 0, 0, 0, 'historico_650794423.pdf', '2019-12-17 06:46:32'),
(22, 3, 102, 2, 'hm4EEezOBp.xls,Q6n1ALwGN1.xls,71Tyv5LpGT.xls,', '', '', 1, 1, 1, 0, 'historico_264178734.pdf', '2019-12-20 09:21:08'),
(23, 3, 201, 2, 'CS9jWBhNYE.xls,', '', '', 1, 1, 1, 0, '', '2019-12-17 06:50:44'),
(24, 3, 202, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:13'),
(25, 3, 302, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:13'),
(26, 3, 301, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:13'),
(27, 3, 401, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:14'),
(28, 3, 402, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:14'),
(29, 3, 501, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:14'),
(30, 3, 502, 2, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:14'),
(31, 4, 103, 3, 'Pk6MBw44gP.xls,', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(32, 4, 104, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(33, 4, 203, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(34, 4, 204, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(35, 4, 303, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(36, 4, 304, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(37, 4, 403, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(38, 4, 404, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(39, 4, 503, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(40, 4, 504, 3, '', '', '', 0, 0, 0, 0, '', '2019-12-17 06:40:32'),
(41, 5, 102, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:31'),
(42, 5, 101, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:31'),
(43, 5, 202, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:31'),
(44, 5, 201, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:31'),
(45, 5, 301, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:41'),
(46, 6, 104, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:53'),
(47, 6, 103, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:53'),
(48, 6, 203, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:53'),
(49, 6, 204, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-20 09:15:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_entrega`
--

CREATE TABLE IF NOT EXISTS `obs_entrega` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `caracs` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `obs` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `obs_entrega`
--

INSERT INTO `obs_entrega` (`id`, `idapto`, `caracs`, `custom`, `obs`, `aprobado`, `fecha`) VALUES
(1, 22, 6, 0, '', 1, '2019-12-20 09:21:08'),
(2, 22, 7, 0, '', 1, '2019-12-20 09:21:08'),
(3, 22, 8, 0, '', 1, '2019-12-20 09:21:08'),
(4, 22, 9, 0, '', 1, '2019-12-20 09:21:08'),
(5, 22, 10, 0, '', 1, '2019-12-20 09:21:08'),
(6, 23, 6, 0, '', 1, '2019-12-17 06:50:44'),
(7, 23, 7, 0, '', 1, '2019-12-17 06:50:44'),
(8, 23, 8, 0, '', 1, '2019-12-17 06:50:44'),
(9, 23, 9, 0, '', 1, '2019-12-17 06:50:44'),
(10, 23, 10, 0, '', 1, '2019-12-17 06:50:44'),
(11, 23, 6, 0, '', 1, '2019-12-17 06:50:44'),
(12, 23, 7, 0, '', 1, '2019-12-17 06:50:44'),
(13, 23, 8, 0, '', 1, '2019-12-17 06:50:44'),
(14, 23, 9, 0, '', 1, '2019-12-17 06:50:44'),
(15, 23, 10, 0, '', 1, '2019-12-17 06:50:44'),
(16, 23, 6, 0, '', 1, '2019-12-17 06:50:44'),
(17, 23, 7, 0, '', 1, '2019-12-17 06:50:44'),
(18, 23, 8, 0, '', 1, '2019-12-17 06:50:44'),
(19, 23, 9, 0, '', 1, '2019-12-17 06:50:44'),
(20, 23, 10, 0, '', 1, '2019-12-17 06:50:44'),
(21, 23, 6, 0, '', 1, '2019-12-17 06:50:44'),
(22, 23, 7, 0, '', 1, '2019-12-17 06:50:44'),
(23, 23, 8, 0, '', 1, '2019-12-17 06:50:44'),
(24, 23, 9, 0, '', 1, '2019-12-17 06:50:44'),
(25, 23, 10, 0, '', 1, '2019-12-17 06:50:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_generales`
--

CREATE TABLE IF NOT EXISTS `obs_generales` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_generales_entrega`
--

CREATE TABLE IF NOT EXISTS `obs_generales_entrega` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text COLLATE latin1_spanish_ci NOT NULL,
  `imgs` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE IF NOT EXISTS `proyectos` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `n_aptos` int(11) NOT NULL,
  `n_torres` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `nombre`, `n_aptos`, `n_torres`, `imagen`, `aprobado`, `fecha_creacion`) VALUES
(1, 'Prueba 1', 20, 2, 'f46bnjAamc.png', 0, '2019-12-17 05:05:06'),
(2, 'Bitall', 20, 2, 'tU0GtH8cea.jpg', 0, '2019-12-17 06:38:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto_complete`
--

CREATE TABLE IF NOT EXISTS `proyecto_complete` (
  `id` int(10) unsigned NOT NULL,
  `iduser` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fechap` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas`
--

CREATE TABLE IF NOT EXISTS `reformas` (
  `id` int(10) unsigned NOT NULL,
  `indice` int(11) NOT NULL COMMENT 'indice de la reforma',
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reformas`
--

INSERT INTO `reformas` (`id`, `indice`, `idapto`, `descripcion`, `cantidad`, `valorx`, `valory`, `observaciones`, `fotos`, `aprobado`, `activa`, `nuevo`, `fecha`) VALUES
(1, 0, 22, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:45:38'),
(2, 0, 22, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:45:38'),
(3, 0, 22, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:45:38'),
(4, 0, 22, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(5, 1, 22, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(6, 1, 22, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(7, 1, 22, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(8, 2, 22, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(9, 1, 22, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(10, 2, 22, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(11, 2, 22, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(12, 2, 22, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(13, 0, 21, 'enchape nuevo', 2, '10000', '100000', '', '', 1, 0, 0, '2019-12-17 06:46:32'),
(14, 0, 21, 'Bla Bla ', 2, '1000', '10000', '', '', 1, 0, 0, '2019-12-17 06:46:32'),
(15, 0, 21, 'bla yap nuevo', 3, '2000', '300000', '', '', 1, 0, 0, '2019-12-17 06:46:32'),
(16, 0, 21, 'cajones madera', 1, '123000', '234000', '', '', 0, 0, 0, '2019-12-17 06:46:32'),
(17, 0, 23, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:50:14'),
(18, 0, 23, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:50:16'),
(19, 0, 23, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:50:16'),
(20, 0, 23, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:50:16'),
(21, 0, 23, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:50:17'),
(22, 0, 23, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:50:17'),
(23, 0, 23, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:50:17'),
(24, 0, 23, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:50:17'),
(25, 0, 23, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:50:18'),
(26, 0, 23, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:50:18'),
(27, 0, 23, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:50:18'),
(28, 0, 23, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:50:19'),
(29, 0, 23, 'enchape', 1, '$10000', '$100000', '', '', 1, 0, 0, '2019-12-17 06:50:19'),
(30, 0, 23, 'Bla Bla ', 2, '$1000', '$10000', '', '', 1, 0, 0, '2019-12-17 06:50:19'),
(31, 0, 23, 'bla yap', 1, '$2000', '$300000', '', '', 1, 0, 0, '2019-12-17 06:50:19'),
(32, 0, 23, 'Yupy', 1, '$123000', '$234000', '', '', 1, 0, 0, '2019-12-17 06:50:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas_entrega`
--

CREATE TABLE IF NOT EXISTS `reformas_entrega` (
  `id` int(10) unsigned NOT NULL,
  `indice` int(11) NOT NULL COMMENT 'indice de la reforma',
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas_h`
--

CREATE TABLE IF NOT EXISTS `reformas_h` (
  `id` int(10) unsigned NOT NULL,
  `indice` int(11) NOT NULL,
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reformas_h`
--

INSERT INTO `reformas_h` (`id`, `indice`, `idapto`, `descripcion`, `cantidad`, `valorx`, `valory`, `observaciones`, `fotos`, `aprobado`, `activa`, `nuevo`, `fecha`) VALUES
(1, 0, 22, 'enchape', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:38'),
(2, 0, 22, 'Bla Bla ', 2, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:38'),
(3, 0, 22, 'bla yap', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:38'),
(4, 0, 22, 'Yupy', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(5, 1, 22, 'enchape', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(6, 1, 22, 'Bla Bla ', 2, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(7, 1, 22, 'bla yap', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(8, 2, 22, 'enchape', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(9, 1, 22, 'Yupy', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(10, 2, 22, 'Bla Bla ', 2, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(11, 2, 22, 'bla yap', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(12, 2, 22, 'Yupy', 1, '', '', '', '', 1, 0, 0, '2019-12-17 06:45:39'),
(13, 0, 21, 'enchape nuevo', 2, '', '', '', '', 1, 0, 0, '2019-12-17 06:46:32'),
(14, 0, 21, 'Bla Bla ', 2, '', '', '', '', 1, 0, 0, '2019-12-17 06:46:32'),
(15, 0, 21, 'bla yap nuevo', 3, '', '', '', '', 1, 0, 0, '2019-12-17 06:46:32'),
(16, 0, 21, 'cajones madera', 1, '', '', '', '', 0, 0, 0, '2019-12-17 06:46:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revision_apto`
--

CREATE TABLE IF NOT EXISTS `revision_apto` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `carac` int(11) NOT NULL,
  `custom` int(11) NOT NULL COMMENT 'me dice si es carac custom',
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `revision_apto`
--

INSERT INTO `revision_apto` (`id`, `idapto`, `obs`, `imgs`, `carac`, `custom`, `aprobado`, `fecha`) VALUES
(1, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(2, 0, '', '', 2, 0, 0, '0000-00-00 00:00:00'),
(3, 0, '', '', 3, 0, 0, '0000-00-00 00:00:00'),
(4, 0, '', '', 4, 0, 0, '0000-00-00 00:00:00'),
(5, 0, '', '', 5, 0, 0, '0000-00-00 00:00:00'),
(6, 0, '', '', 6, 0, 0, '0000-00-00 00:00:00'),
(7, 0, '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(8, 0, '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(9, 0, '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(10, 0, '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(11, 0, '', '', 11, 0, 0, '0000-00-00 00:00:00'),
(12, 0, '', '', 12, 0, 0, '0000-00-00 00:00:00'),
(13, 0, '', '', 13, 0, 0, '0000-00-00 00:00:00'),
(14, 0, '', '', 14, 0, 0, '0000-00-00 00:00:00'),
(15, 0, '', '', 15, 0, 0, '0000-00-00 00:00:00'),
(16, 22, '', '', 6, 0, 1, '2019-12-17 06:45:38'),
(17, 22, '', '', 7, 0, 1, '2019-12-17 06:45:38'),
(18, 22, '', '', 8, 0, 1, '2019-12-17 06:45:38'),
(19, 22, '', '', 9, 0, 1, '2019-12-17 06:45:38'),
(20, 22, '', '', 10, 0, 1, '2019-12-17 06:45:38'),
(21, 21, '', '', 6, 0, 1, '2019-12-17 06:46:32'),
(22, 21, '', '', 7, 0, 1, '2019-12-17 06:46:32'),
(23, 21, '', '', 8, 0, 1, '2019-12-17 06:46:32'),
(24, 21, '', '', 9, 0, 1, '2019-12-17 06:46:32'),
(25, 21, '', '', 10, 0, 1, '2019-12-17 06:46:32'),
(26, 23, '', '', 6, 0, 1, '2019-12-17 06:50:19'),
(27, 23, '', '', 7, 0, 1, '2019-12-17 06:50:19'),
(28, 23, '', '', 8, 0, 1, '2019-12-17 06:50:19'),
(29, 23, '', '', 9, 0, 1, '2019-12-17 06:50:19'),
(30, 23, '', '', 10, 0, 1, '2019-12-17 06:50:19'),
(31, 0, '', '', 16, 0, 0, '0000-00-00 00:00:00'),
(32, 0, '', '', 17, 0, 0, '0000-00-00 00:00:00'),
(33, 0, '', '', 18, 0, 0, '0000-00-00 00:00:00'),
(34, 0, '', '', 19, 0, 0, '0000-00-00 00:00:00'),
(35, 0, '', '', 20, 0, 0, '0000-00-00 00:00:00'),
(36, 31, '', '', 11, 0, 0, '0000-00-00 00:00:00'),
(37, 31, '', '', 12, 0, 0, '0000-00-00 00:00:00'),
(38, 31, '', '', 13, 0, 0, '0000-00-00 00:00:00'),
(39, 31, '', '', 14, 0, 0, '0000-00-00 00:00:00'),
(40, 31, '', '', 15, 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE IF NOT EXISTS `secciones` (
  `id` int(10) unsigned NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `proyecto` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `seccion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`id`, `imagen`, `proyecto`, `tipo`, `seccion`, `fecha_creacion`) VALUES
(1, 'kitchen', 1, 1, 'Cocina', '2019-12-17 05:05:18'),
(2, 'kitchen', 2, 2, 'Cocina', '2019-12-17 06:39:06'),
(3, 'king_bed', 2, 3, 'Habitacion', '2019-12-17 06:39:43'),
(4, 'kitchen', 3, 4, 'Cocina', '2019-12-20 09:14:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_apto`
--

CREATE TABLE IF NOT EXISTS `secciones_apto` (
  `id` int(10) unsigned NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `seccion` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_caracs`
--

CREATE TABLE IF NOT EXISTS `secciones_caracs` (
  `id` int(10) unsigned NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `secciones_caracs`
--

INSERT INTO `secciones_caracs` (`id`, `imagen`, `seccion`, `caracs`, `fecha_creacion`) VALUES
(1, '', 1, '1', '2019-12-17 05:05:22'),
(2, '', 1, '2', '2019-12-17 05:05:22'),
(3, '', 1, '3', '2019-12-17 05:05:22'),
(4, '', 1, '4', '2019-12-17 05:05:22'),
(5, '', 1, '5', '2019-12-17 05:05:22'),
(6, '', 2, '1', '2019-12-17 06:39:22'),
(7, '', 2, '2', '2019-12-17 06:39:22'),
(8, '', 2, '3', '2019-12-17 06:39:22'),
(9, '', 2, '4', '2019-12-17 06:39:22'),
(10, '', 2, '5', '2019-12-17 06:39:22'),
(11, '', 3, 'a', '2019-12-17 06:39:49'),
(12, '', 3, 'b', '2019-12-17 06:39:49'),
(13, '', 3, 'c', '2019-12-17 06:39:49'),
(14, '', 3, 'd', '2019-12-17 06:39:49'),
(15, '', 3, 'e', '2019-12-17 06:39:49'),
(16, '', 4, 'q', '2019-12-20 09:14:20'),
(17, '', 4, 'w', '2019-12-20 09:14:20'),
(18, '', 4, 'e', '2019-12-20 09:14:20'),
(19, '', 4, 'r', '2019-12-20 09:14:20'),
(20, '', 4, 't', '2019-12-20 09:14:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_caracs_history`
--

CREATE TABLE IF NOT EXISTS `secciones_caracs_history` (
  `id` int(10) unsigned NOT NULL,
  `idapto` int(11) NOT NULL,
  `carac` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_caracs_history`
--

INSERT INTO `secciones_caracs_history` (`id`, `idapto`, `carac`, `custom`, `obs`, `imgs`, `aprobado`, `fecha`) VALUES
(1, 22, 7, 0, '', '', 1, '2019-12-17 06:45:38'),
(2, 22, 8, 0, '', '', 1, '2019-12-17 06:45:38'),
(3, 22, 6, 0, '', '', 1, '2019-12-17 06:45:38'),
(4, 22, 10, 0, '', '', 1, '2019-12-17 06:45:38'),
(5, 22, 9, 0, '', '', 1, '2019-12-17 06:45:38'),
(6, 21, 6, 0, '', '', 1, '2019-12-17 06:46:32'),
(7, 21, 7, 0, '', '', 1, '2019-12-17 06:46:32'),
(8, 21, 8, 0, '', '', 1, '2019-12-17 06:46:32'),
(9, 21, 9, 0, '', '', 1, '2019-12-17 06:46:32'),
(10, 21, 10, 0, '', '', 1, '2019-12-17 06:46:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_custom`
--

CREATE TABLE IF NOT EXISTS `secciones_custom` (
  `id` int(11) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `apto` int(11) NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_default`
--

CREATE TABLE IF NOT EXISTS `secciones_default` (
  `id` int(10) unsigned NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_default`
--

INSERT INTO `secciones_default` (`id`, `imagen`, `seccion`, `fecha_creacion`) VALUES
(1, 'kitchen', 'Cocina', '2019-12-17 05:05:18'),
(2, 'king_bed', 'Habitacion', '2019-12-17 06:39:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_aptos`
--

CREATE TABLE IF NOT EXISTS `tipos_aptos` (
  `id` int(10) unsigned NOT NULL,
  `proyecto` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tipos_aptos`
--

INSERT INTO `tipos_aptos` (`id`, `proyecto`, `nombre`, `fecha_creacion`) VALUES
(1, 1, 'A', '2019-12-17 05:05:13'),
(2, 2, 'Tipo1', '2019-12-17 06:38:50'),
(3, 2, 'Tipo2', '2019-12-17 06:39:34'),
(4, 3, 'Tipo A', '2019-12-20 09:14:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torres_proyecto`
--

CREATE TABLE IF NOT EXISTS `torres_proyecto` (
  `id` int(10) unsigned NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `torres_proyecto`
--

INSERT INTO `torres_proyecto` (`id`, `id_proyecto`, `nombre`) VALUES
(1, 1, 'Torre1'),
(2, 1, 'Torre2'),
(3, 2, 'Torre1'),
(4, 2, 'Torre2'),
(5, 3, 'Torre1'),
(6, 3, 'Torre2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `pass` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `nivel` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `pass`, `nivel`) VALUES
(1, 'Capital', 'admin@capital.com', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 'Carlos', 'carlos@capital.com', 'dc599a9972fde3045dab59dbd1ae170b', 1),
(3, 'Cesar', 'cesar@capital.com', '6f597c1ddab467f7bf5498aad1b41899', 1),
(4, 'Pedro', 'pedro@capital.com', 'c6cc8094c2dc07b700ffcc36d64e2138', 2),
(5, 'Pepe', 'pepe@capital.com', '926e27eecdbc7a18858b3798ba99bddd', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE IF NOT EXISTS `visitas` (
  `id` int(10) unsigned NOT NULL,
  `idp` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `idapto` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `visitas`
--

INSERT INTO `visitas` (`id`, `idp`, `user`, `idapto`, `fecha`) VALUES
(1, 2, 2, 22, '2019-12-17 06:41:58'),
(2, 2, 2, 22, '2019-12-17 06:43:52'),
(3, 2, 2, 21, '2019-12-17 06:45:46'),
(4, 2, 2, 21, '2019-12-17 06:46:06'),
(5, 2, 2, 23, '2019-12-17 06:49:17'),
(6, 2, 2, 21, '2019-12-17 06:59:16'),
(7, 2, 2, 21, '2019-12-20 09:17:02'),
(8, 2, 2, 21, '2019-12-20 09:17:28'),
(9, 2, 2, 21, '2019-12-20 09:17:43'),
(10, 2, 2, 31, '2019-12-20 09:17:55'),
(11, 2, 2, 31, '2019-12-20 09:18:03'),
(12, 2, 2, 31, '2019-12-20 09:18:14');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aptos_torre`
--
ALTER TABLE `aptos_torre`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `campos_entrega`
--
ALTER TABLE `campos_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracs_custom`
--
ALTER TABLE `caracs_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracs_default`
--
ALTER TABLE `caracs_default`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura`
--
ALTER TABLE `nomenclatura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura_history`
--
ALTER TABLE `nomenclatura_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura_oficial`
--
ALTER TABLE `nomenclatura_oficial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_entrega`
--
ALTER TABLE `obs_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_generales`
--
ALTER TABLE `obs_generales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_generales_entrega`
--
ALTER TABLE `obs_generales_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proyecto_complete`
--
ALTER TABLE `proyecto_complete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas`
--
ALTER TABLE `reformas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas_entrega`
--
ALTER TABLE `reformas_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas_h`
--
ALTER TABLE `reformas_h`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `revision_apto`
--
ALTER TABLE `revision_apto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_apto`
--
ALTER TABLE `secciones_apto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_caracs`
--
ALTER TABLE `secciones_caracs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `secciones_caracs_history`
--
ALTER TABLE `secciones_caracs_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_custom`
--
ALTER TABLE `secciones_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_default`
--
ALTER TABLE `secciones_default`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_aptos`
--
ALTER TABLE `tipos_aptos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `torres_proyecto`
--
ALTER TABLE `torres_proyecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aptos_torre`
--
ALTER TABLE `aptos_torre`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `campos_entrega`
--
ALTER TABLE `campos_entrega`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `caracs_custom`
--
ALTER TABLE `caracs_custom`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `caracs_default`
--
ALTER TABLE `caracs_default`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `nomenclatura`
--
ALTER TABLE `nomenclatura`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `nomenclatura_history`
--
ALTER TABLE `nomenclatura_history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `nomenclatura_oficial`
--
ALTER TABLE `nomenclatura_oficial`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `obs_entrega`
--
ALTER TABLE `obs_entrega`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `obs_generales`
--
ALTER TABLE `obs_generales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `obs_generales_entrega`
--
ALTER TABLE `obs_generales_entrega`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `proyecto_complete`
--
ALTER TABLE `proyecto_complete`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `reformas`
--
ALTER TABLE `reformas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `reformas_entrega`
--
ALTER TABLE `reformas_entrega`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reformas_h`
--
ALTER TABLE `reformas_h`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `revision_apto`
--
ALTER TABLE `revision_apto`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `secciones_apto`
--
ALTER TABLE `secciones_apto`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `secciones_caracs`
--
ALTER TABLE `secciones_caracs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `secciones_caracs_history`
--
ALTER TABLE `secciones_caracs_history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `secciones_custom`
--
ALTER TABLE `secciones_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `secciones_default`
--
ALTER TABLE `secciones_default`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipos_aptos`
--
ALTER TABLE `tipos_aptos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `torres_proyecto`
--
ALTER TABLE `torres_proyecto`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
