﻿<?php  
//DATOS PARA CONEXIÓN A DB
$host="localhost";
$userdb="root";
$passdb="";
$db="capital";

$host="http://mysql.industrialfumigaciones.com";
$userdb="capitaldb";
$passdb="capitaldb2019";
$db="capital";

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if(isset($_POST['accion']) and $_POST['accion']=='nproyecto'){
	$imagenp = '';
	if(isset($_FILES["imagen"]["name"])){
		switch($_FILES['imagen']['type']){
			case 'image/jpeg':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['nombrep'])).'.jpg';
				break;
			case 'image/jpg':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['nombrep'])).'.jpg';
				break;
			case 'image/JPG':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['nombrep'])).'.jpg';
				break;
			case 'image/JPEG':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['nombrep'])).'.jpg';
				break;
			case 'image/png':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['nombrep'])).'.png';
				break;
		}
		
		$sourcePath = $_FILES['imagen']['tmp_name']; // Storing source path of the file in a variable
		$targetPath = "uploads/proyectos/".$_FILES["imagen"]["name"]; // Target path where file is to be stored
		//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
		move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
		$imagenp = $_FILES['imagen']['name'];
	}

	$aptosxtorre=$_POST['nxtorre'];
	$axtorre='';
	
	
	$link = mysqli_connect($host, $userdb, $passdb, $db);
				
	$sql = "INSERT INTO proyectos(nombre,n_aptos,n_torres,imagen,fecha_creacion)
	VALUES ('".$_POST['nombrep']."',".$_POST['naptos'].",".$_POST['ntorres'].",
	'".$imagenp."',now())";
	
	$link->query($sql);
	$idproyecto=$link->insert_id;
	$arr_torres = array();

	//GUARDAR LAS TORRES
	for($r=0;$r<$_POST['ntorres'];$r++){

		$sql = "INSERT INTO torres_proyecto(id_proyecto, nombre)
		VALUES (".$idproyecto.",'Torre".($r+1)."')";
		
		$link->query($sql);
		$idtorre=$link->insert_id;
		array_push($arr_torres, $idtorre);
	}

	//GUARDAR APTOS X TORRE
	for($x=0;$x<$aptosxtorre;$x++){
		if(!(empty( $_POST['pisosxtorre'.$x] ))){
			$sql = "INSERT INTO aptos_torre(id_proyecto, torre, naptos)
			VALUES (".$idproyecto.",'Torre".($x+1)."',".$_POST['pisosxtorre'.$x].")";
			
			$link->query($sql);
		}
	}
	
	if($idproyecto){ 
		$arr_res = array( 'res' => 1, 'idp' => $idproyecto, 'idtorres' => $arr_torres );
		echo json_encode($arr_res);
	}
}

if(isset($_POST['accion']) and $_POST['accion']=='ntipo'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
				
	$sql = "INSERT INTO tipos_aptos(proyecto,nombre,fecha_creacion)
	VALUES (".$_POST['proyecto'].",'".$_POST['ntipo']."',now())";
	
	$link->query($sql);
	$insert=$link->insert_id;
	
	if($insert){ 
		$arr_res = array( 'res' => 1, 'idtipo' => $insert );
		echo json_encode($arr_res);
	}
}

if(isset($_POST['accion1']) and $_POST['accion1']=='nseccion'){
	$imagenp='';
	if(isset($_FILES["imagen"]["name"])){
		switch($_FILES['imagen']['type']){
			case 'image/jpeg':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['seccion'])).'.jpg';
				break;
			case 'image/jpg':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['seccion'])).'.jpg';
				break;
			case 'image/JPG':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['seccion'])).'.jpg';
				break;
			case 'image/JPEG':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['seccion'])).'.jpg';
				break;
			case 'image/png':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_POST['seccion'])).'.png';
				break;
		}
		
		$sourcePath = $_FILES['imagen']['tmp_name']; // Storing source path of the file in a variable
		$targetPath = "uploads/proyectos/".$_FILES["imagen"]["name"]; // Target path where file is to be stored
		//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
		move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
		$imagenp = $_FILES['imagen']['name'];
	}
	
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$imagenp = $_POST['rutaimg'];
				
	$sql = "INSERT INTO secciones(imagen,proyecto,tipo,seccion,fecha_creacion)
	VALUES ('".$imagenp."',".$_POST['idproyecto1'].",".$_POST['idtipo1'].",'".$_POST['seccion']."',now())";
	
	$link->query($sql);
	$idseccion=$link->insert_id;
	
	if($idseccion){ 
		$arr_res = array( 'res' => 1, 'idseccion' => $idseccion );
		echo json_encode($arr_res);
	}
}

if(isset($_POST['accion2']) and $_POST['accion2']=='ncarac'){
	$imagenp='';
	if(isset($_FILES["imagen"]["name"])){
		switch($_FILES['imagen']['type']){
			case 'image/jpeg':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_FILES["imagen"]["name"])).'.jpg';
				break;
			case 'image/jpg':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_FILES["imagen"]["name"])).'.jpg';
				break;
			case 'image/JPG':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_FILES["imagen"]["name"])).'.jpg';
				break;
			case 'image/JPEG':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_FILES["imagen"]["name"])).'.jpg';
				break;
			case 'image/png':
				$_FILES["imagen"]["name"]=str_replace(' ','',strtolower($_FILES["imagen"]["name"])).'.png';
				break;
		}
		
		$sourcePath = $_FILES['imagen']['tmp_name']; // Storing source path of the file in a variable
		$targetPath = "uploads/proyectos/".$_FILES["imagen"]["name"]; // Target path where file is to be stored
		//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
		move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
		$imagenp = $_FILES['imagen']['name'];
	}
	
	$link = mysqli_connect($host, $userdb, $passdb, $db);
				
	//capturar las características
	$n_caracs=$_POST['ncaracs'];
	$caracs='';
	
	for($x=0;$x<$n_caracs;$x++){
		//if(!(empty( $_POST['carac'.($x+1)] ))) $caracs.=$_POST['carac'.($x+1)].';';
		if(!(empty( $_POST['carac'.($x+1)] ))){
			$caracs=$_POST['carac'.($x+1)];
			$sql = "INSERT INTO secciones_caracs(seccion,caracs,fecha_creacion)
					VALUES (".$_POST['idseccion'].",'".$caracs."',now())";
			
			$link->query($sql);
		}
	}
	
	/*$sql = "INSERT INTO secciones_caracs(seccion,caracs,fecha_creacion)
	VALUES (".$_POST['idseccion'].",'".$caracs."',now())";
	
	$link->query($sql);
	$idcaracs=$link->insert_id;*/
	
	/*if($idcaracs){ 
		
	}*/
	$arr_res = array( 'res' => 1 );
	echo json_encode($arr_res);
}

if(isset($_POST['accion']) and $_POST['accion']=='tipos'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$idp = $_POST['idp'];
	$idtipo = '';
	$ntipo = '';
	$naptos = '';
	
	$sql = "SELECT * FROM tipos_aptos WHERE proyecto=".$idp;
	
	$resul=$link->query($sql);
	$cont="";
	while($datos=$resul->fetch_assoc()){
		$idtipo .= $datos['id'].';';
		$ntipo .= $datos['nombre'].';';
	}
	
	
	$arr_res = array( 'res' => 1, 'idtipo' => $idtipo, 'ntipo' => $ntipo );
	echo json_encode($arr_res);
	exit;
}

if(isset($_POST['accion']) and $_POST['accion']=='savenomenc'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
				
	$sql = "INSERT INTO nomenclatura(idtorre,idproyecto,desde_piso,hasta_piso,naptos,nomenc1,nomenc2)
	VALUES (".$_POST['idtorre'].",".$_POST['idproyecto'].",".$_POST['desde'].",".$_POST['hasta'].",".$_POST['naptos'].",'".$_POST['nm1']."','".$_POST['nm2']."')";
	
	$link->query($sql);
	$insert=$link->insert_id;
	
	/*if($insert){ 
		$arr_res = array( 'res' => 1, 'idtipo' => $insert );
		echo json_encode($arr_res);
	}*/
	
	echo '1';
	exit;
}

if(isset($_POST['accion']) and $_POST['accion']=='savenomenc1'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
				
	$sql = "INSERT INTO nomenclatura_oficial(idtorre,nomenclatura)
	VALUES (".$_POST['idtorre'].",'".$_POST['nomenc']."')";
	
	$link->query($sql);
	$insert=$link->insert_id;
	
	/*if($insert){ 
		$arr_res = array( 'res' => 1, 'idtipo' => $insert );
		echo json_encode($arr_res);
	}*/
	
	echo '1';
	exit;
}


/*var 
for(k=1;k<=localStorage.getItem('p_torres');k++){
   for(m=$('#nomenc'+index_x+'_1').val();m<=$('#nomenc'+index_x+'_2').val();m++){
		// ******************************* HTML DE TABLA ************************
			if(xx>9) nomenc_tabla = pisonomenc+xx;
			else nomenc_tabla = pisonomenc+ceronomenc+xx;

			//alert('elcero->'+pisonomenc+ceronomenc+xx);

			if(index_tabla==1){
				html_tabla+='<tr><td rowspan="10">'+m+'</td><td>'+nomenc_tabla+'</td><td><select name=""><option value="">Seleccionar...</option>';
				html_tabla+='<option value="">Tipo A</option><option vlue="">Tipo B</option><option vlue="">Tipo C</option>';
				html_tabla+='</select></td><td><img onclick="$(\'#file_reforma\').click();" src="img/doc.png" width="35"></td></tr>';
			}else{
				html_tabla+='<tr><td>'+nomenc_tabla+'</td><td><select name=""><option value="">Seleccionar...</option>';
				html_tabla+='<option value="">Tipo A</option><option vlue="">Tipo B</option><option vlue="">Tipo C</option>';
				html_tabla+='</select></td><td><img onclick="$(\'#file_reforma\').click();" src="img/doc.png" width="35"></td></tr>';
			}
		//alert('torres->'+localStorage.getItem('p_torres'));
		xx++;
		index_tabla++;
   }

   xx = xx_nom;
   index_tabla = 1;
   pisonomenc++;
}

*/



if(isset($_POST['accion']) and $_POST['accion']=='dotabla'){
	$contador=0;
	$conttorres=0;
	$arr_nomenc=array();
	$arr_torres=array();
	$cadena_nom='';
	$nomen1=0;
	$nomen2=0;
	$elcount=0;
	
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$html_tabla='<table><tr><th>TORRE</th><th>APARTAMENTO</th>
	<th>CLASE DEL APARTAMENTO</th>
	<th>AÑADIR REFORMAS</th>			
	<th>GUARDAR</th></tr>';			
	
	$sql = "SELECT * FROM torres_proyecto WHERE id_proyecto=".$_POST['idp'];
	$resul=$link->query($sql);
	while($datos=$resul->fetch_assoc()){
		//almacenar ids de torres
		array_push($arr_torres, $datos['id']);
	}
	
	//echo 'array->'.$arr_torres[0];
	
	for($x=0;$x<count($arr_torres);$x++){
		$sql="SELECT COUNT(nomenclatura) as elcount FROM nomenclatura_oficial WHERE idtorre=".$arr_torres[$x];
		$resul=$link->query($sql);
		while($datos=$resul->fetch_assoc()){
			$elcount=$datos['elcount'];
		}
		
		$sql="SELECT * FROM nomenclatura_oficial WHERE idtorre=".$arr_torres[$x].' order by nomenclatura asc';
		$resul=$link->query($sql);
		$j=0;
	
		while($datos=$resul->fetch_assoc()){
			if($contador==0){ $html_tabla.='<tr>
						<td rowspan="'.$elcount.'"><b>Torre '.($x+1).'</b></td>
						<td>'.$datos['nomenclatura'].'</td>
						<td><select id="tipoapto'.$datos['id'].'" onchange="setClaseapto(this, '.$datos['id'].');" name="tipoapto" class="tipoapto">
                        </select></td>
						<td><form enctype="multipart/form-data" name="frm_reformas" id="frm_reformas'.$datos['id'].'" method="post" action="">
						<input type="hidden" name="idnomenc" value="'.$datos['id'].'" />
						<input type="hidden" name="clase" id="clase'.$datos['id'].'" />
						<input type="hidden" name="accion" value="savereforma" />
						<input onchange="alert(\'Archivo adjuntado con éxito\');" type="file" class="file_reforma" name="file_reforma" id="file_reforma'.$datos['id'].'" style="display:none" />
						<img onclick="$(\'#file_reforma'.$datos['id'].'\').click();" src="img/doc.png" width="35"></form></td>
                        <td><a id="btnreforma'.$datos['id'].'" class="btnreforma waves-effect waves-light btn modal-trigger" 
							href="javascript:void(0);" onclick="saveReformas('.$datos['id'].');">GUARDAR</a></td>
					</tr>';
			}else{
				$html_tabla.='<tr>
						<td>'.$datos['nomenclatura'].'</td>
						<td><select id="tipoapto'.$datos['id'].'" onchange="setClaseapto(this, '.$datos['id'].');" name="tipoapto" class="tipoapto">
                        </select></td>
						<td><form enctype="multipart/form-data" name="frm_reformas" id="frm_reformas'.$datos['id'].'" method="post" action="">
						<input type="hidden" name="idnomenc" value="'.$datos['id'].'" />
						<input type="hidden" name="clase" id="clase'.$datos['id'].'" />
						<input type="hidden" name="accion" value="savereforma" />
						<input onchange="alert(\'Archivo adjuntado con éxito\');" type="file" class="file_reforma" name="file_reforma" id="file_reforma'.$datos['id'].'" style="display:none" />
						<img onclick="$(\'#file_reforma'.$datos['id'].'\').click();" src="img/doc.png" width="35"></form></td>
                        <td><a id="btnreforma'.$datos['id'].'" class="btnreforma waves-effect waves-light btn modal-trigger" 
							href="javascript:void(0);" onclick="saveReformas('.$datos['id'].');">GUARDAR</a></td>
					</tr>';
			}
			$contador++;
			$j++;
		}
		$contador=0;
	}
	$html_tabla.='</table>';
	$html_tabla.='<p>&nbsp;</p>';
	$html_tabla.='<a class="waves-effect waves-light btn modal-trigger" 
				  href="javascript:void(0);" onclick="location.reload();">TERMINAR</a>';
	
	
	$arr_res = array( 'res' => 1, 'tabla' => $html_tabla );
	echo json_encode($arr_res);
	
	//echo $html_tabla;
	exit;
}

if(isset($_POST['accion']) and $_POST['accion']=='savereforma'){
	$reforma = '';
	$subido = true;
	if(isset($_FILES["file_reforma"]["name"]) and !(empty( $_FILES["file_reforma"]["name"] ))){
		$temporary = explode(".", $_FILES["file_reforma"]["name"]);
		$file_extension = end($temporary);
		$_FILES["file_reforma"]["name"]=generateRandomString().'.'.$file_extension;
		$sourcePath = $_FILES['file_reforma']['tmp_name']; // Storing source path of the file in a variable
		$targetPath = "uploads/proyectos/reformas/".$_FILES["file_reforma"]["name"]; // Target path where file is to be stored
		//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
		$subido=move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
		$reforma = $_FILES['file_reforma']['name'];
	}

	
	$link = mysqli_connect($host, $userdb, $passdb, $db);
				
	$sql = "UPDATE nomenclatura_oficial set clase=".$_POST['clase'].", reforma='".$reforma."' 
			where id=".$_POST['idnomenc'];
	
	$link->query($sql);
	//$idproyecto=$link->insert_id;
	
	
	if($subido) $arr_res = array( 'res' => 1 );
	else $arr_res = array( 'res' => 0 ); 
	echo json_encode($arr_res);
}

if(isset($_POST['accion']) and $_POST['accion']=='getproyectos'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	
	$sql = "SELECT * FROM proyectos";
	
	$resul=$link->query($sql);
	$html_proyectos="";
	while($datos=$resul->fetch_assoc()){
		$clase="";
		$estado="";
		if($datos['aprobado']==1){ 
			$clase='aprobado';
			$estado='APROBADO';
		}else{
			$clase='pendiente';
			$estado='PENDIENTE';
		}
		$html_proyectos.='<div class="col s12 m6">
		<div class="card">
			<div class="card-image">
			<img class="img-proyectos responsive-img" src="uploads/proyectos/'.$datos['imagen'].'">
			</div>
			<div class="card-content">
				<h3>'.strtoupper($datos['nombre']).'</h3>
				<span>PROYECTO</span>
				<span class="'.$clase.'">
				'.$estado.'</span>

			</div>
			<div class="card-action">
				<div class="detalles" onclick="detalleProyecto('.$datos['id'].');">DETALLES
				</div>
			</div></div></div>';
	}
	
	
	//$arr_res = array( 'res' => 1, 'html' => $html_proyectos );
	//echo json_encode($arr_res);
	echo $html_proyectos;
	exit;
}

if(isset($_POST['accion']) and $_POST['accion']=='getaptos'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	
	$sql = "SELECT id FROM torres_proyecto where id_proyecto=".$_POST['idp'];
	
	$resul=$link->query($sql);
	$html_aptos="";
	$count_torres=0;
	$arr_tid=array();
	
	while($datos=$resul->fetch_assoc()){
		$clase="";
		$estado="";
		//GUARDAR EN UN ARRAY LOS IDS DE LAS TORRES DEL PROYECTO
		$arr_tid[$count_torres]=$datos['id'];
		$count_torres++;
		$j=0;
	}
	
	
	$cont_torres=1;
	for($x=0;$x<$count_torres;$x++){
		$sql = "SELECT * FROM nomenclatura_oficial where idtorre=".$arr_tid[$x].' 
				order by nomenclatura asc';
		$resul=$link->query($sql);
		$html_aptos.='<li>
				  <div class="collapsible-header"><h3>Torre '.($x+1).'</h3></div>
				  <div class="collapsible-body">';
		
		while($aptos=$resul->fetch_assoc()){
			$laclase=($aptos['aprobado']==0)?'naprobados':'aprobados';
			$html_aptos.='<span><h4 class="'.$laclase.'" onclick="goSecciones('.$aptos['id'].');">APTO '.$aptos['nomenclatura'].'</h4></span>';
			$cont_torres++;
		}
		$html_aptos.='</div></li>';
	}
	
	//echo 'lajota->'.$j;
	
	
	$arr_res = array( 'res' => 1, 'html' => $html_aptos );
	echo json_encode($arr_res);
	//echo $html_aptos;
	exit;
}

if(isset($_POST['accion']) and $_POST['accion']=='getsecs'){
	/*$html_sec.='<div class="col m6 s12">
					<div class="proyecto-l aptos">
						<div class="img a"><img src="'.$datos['imagen'].'" /></div>
						<h3>'.strtoupper($datos['seccion']).'</h3>
						<div class="img b"><img onclick="goCaracs('.$datos['id'].');" src="img/arrow.jpg" /></div>
						<div class="clear"></div>
					</div>
				</div>';*/
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	
	$sql = "SELECT * FROM secciones where proyecto=".$_POST['idp'];
	
	$resul=$link->query($sql);
	$html_sec="";
	$contsecs=0;
	
	while($datos=$resul->fetch_assoc()){
		$html_sec.='<li>
				  <div class="collapsible-header"><img src="'.$datos['imagen'].'" width="30" /><h3>'.$datos['seccion'].'</h3></div>
				  <div class="collapsible-body"><div class="clist">';
		//seleccionar las características de cada sección
		$sql="SELECT * FROM secciones_caracs WHERE seccion=".$datos['id'];
		$resul2=$link->query($sql);
		if($resul2->num_rows>0){
			while($caracs=$resul2->fetch_assoc()){
				//$arr_caracs=explode(";", $caracs['caracs']);
				//recorrer características y listarlas
				/*for($c=0;$c<count($arr_caracs);$c++){
					if(!(empty( $arr_caracs[$c] ))){
						$html_sec.='<span>
						<div class="sec_state">
							<img width="20" src="img/ok.png" onclick="secState(1, '.$caracs['id'].');" />
							<img width="20" src="img/xx.png" onclick="secState(2, '.$caracs['id'].');" />
							<div class="clear"></div>
						</div>
						<h4>'.strtoupper($arr_caracs[$c]).'</h4></span><div class="clear"></div>';
					}
				}*/
				$html_sec.='<span>
					<div class="sec_state">
						<img id="ok'.$caracs['id'].'" width="20" src="img/ok.png" onclick="secState(1, '.$caracs['id'].');" />
						<img id="nok'.$caracs['id'].'" width="20" src="img/xx.png" onclick="secState(2, '.$caracs['id'].');" />
						<div class="clear"></div>
					</div>
					<div class="cright">
						<h4>'.strtoupper($caracs['caracs']).'</h4>
					
						<div class="input-field">
							<textarea class="materialize-textarea" id="obs'.$caracs['id'].'" ></textarea>
							<label for="obs'.$caracs['id'].'">Observaciones...</label>
						</div>
					</div>
					<div class="clear"></div>
					</span>';
			}
		}
		
		$html_sec.='</div>
					<div class="cfotos" id="cfotos'.$datos['id'].'"><img onclick="tomarFoto('.$datos['id'].');" id ="foto'.$datos['id'].'" src="img/camera.png" width="30%" /></div>
					<div class="clear"></div>
					</div></li>';
		$contsecs++;
	}
	//echo 'conta->'.$contsecs;
	//echo 'lajota->'.$j;
	
	
	$arr_res = array( 'res' => 1, 'secs' => $html_sec );
	echo json_encode($arr_res);
	//echo $html_aptos;
	exit;
}


if(isset($_POST['accion']) and $_POST['accion']=='getcaracs'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$arr_caracs=array();
	
	$sql = "SELECT * FROM secciones_caracs where seccion=".$_POST['id'];
	
	$resul=$link->query($sql);
	$html_c="";
	
	while($datos=$resul->fetch_assoc()){
		$arr_caracs=explode(";", $datos['caracs']);
	}
	
	for($x=0;$x<count($arr_caracs);$x++){
		if(!(empty($arr_caracs[$x]))){
			$html_c.='<div class="col m6 s12">
				<div class="proyecto-l aptos">
					<div class="img a"><img src="'.$datos['imagen'].'" /></div>
					<h3>'.strtoupper($arr_caracs[$x]).'</h3>
					<div class="img b"><img onclick="document.location.href=\'draw.html\'" src="img/arrow.jpg" /></div>
					<div class="clear"></div>
				</div>
			</div>';
		}
	}
	
	//echo 'lajota->'.$j;
	
	
	$arr_res = array( 'res' => 1, 'caracs' => $html_c );
	echo json_encode($arr_res);
	//echo $html_aptos;
	exit;
}

if(isset($_POST['accion']) and $_POST['accion']=='getvisita'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$arr_caracs=array();
	
	$sql = "SELECT * FROM visitas WHERE idp=".$_POST['idp']." and 
			user=".$_POST['iduser']." and idapto=".$_POST['idapto'];
	
	$resul=$link->query($sql);
	//echo 'rows->'.$resul->num_rows;
	//exit;
	if($resul->num_rows>0){
		$arr_res = array( 'res' => 1 );
		echo json_encode($arr_res);
		exit;
	}else{
		$arr_res = array( 'res' => 0 );
		echo json_encode($arr_res);
		exit;
	}
}

if(isset($_POST['accion']) and $_POST['accion']=='setvisita'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$arr_caracs=array();
	
	$sql = "INSERT INTO visitas(idp, user, idapto, fecha) 
			VALUES(".$_POST['idp'].", ".$_POST['iduser'].", ".$_POST['idapto'].", now())";
	
	$resul=$link->query($sql);

	$arr_res = array( 'res' => 1 );
	echo json_encode($arr_res);
	//echo $html_aptos;
	exit;
}


if(isset($_POST['accion']) and $_POST['accion']=='login'){
	$link = mysqli_connect($host, $userdb, $passdb, $db);
	$arr_caracs=array();
	
	$sql = "SELECT * FROM usuarios where email='".$_POST['email']."' and pass='".md5($_POST['pass'])."'";
	
	if($resul=$link->query($sql)){
		while($datos=$resul->fetch_assoc()){
			$arr_res = array( 'res' => 1, 'iduser' => $datos['id'], 'emailu' => $datos['email'] );
			echo json_encode($arr_res);
			exit;
		}
	}else{
		$arr_res = array( 'res' => 0 );
		echo json_encode($arr_res);
		exit;
	}
}
?>