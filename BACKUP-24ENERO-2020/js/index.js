﻿//var server='http://200.122.212.101/';
//var server='http://10.10.20.101/';
var server='';
var p_notcomplete=false;

//se ejecuta cuando se encuentra un error de javascript
window.onerror = function(msg, url, line) {
   var idx = url.lastIndexOf("/");
   if(idx > -1) {url = url.substring(idx+1);}
     alert("ERROR in " + url + " (line #" + line + "): " + msg);
   return false; //suppress Error Alert;
};

$(window).on("beforeunload", function() { 
    if(p_notcomplete){ 
		if( confirm("Realmente deseas salir? Al hacerlo, algunos datos pueden no guardarse correctamente") ) return true;
		else return false;
	}	
});
 
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        /*var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);*/
    }
};

app.initialize();

var incordova = false;
var inweb = false;

$(window).on('load', function(){
	if($('.modal-overlay').length>0) $('.modal-overlay').unbind();
});

//me dice si estoy conectado a internet *****************
var is_online=false;
var db = '';
$(document).ready(function(){
	//para q al entrar a 'secciones' por primera vez, pregunte si es nueva visita
	if($('body#bodysections').length==0) localStorage.setItem('saved_custom', '0');
	
	if( (localStorage.getItem('iduser') == null || localStorage.getItem('nivelu') == null) && typeof enindex === 'undefined') document.location.href='index.html';
	
	//alert(localStorage.getItem('iduser'));
	var nivelu = localStorage.getItem('nivelu');
	//revisar si hay conexión a internet
	axios.get(server+'api/product/read.php?accion=isonline')
	.then(function (response) {
		if(response.data.res==1){
			is_online=true;
		}else is_online=false;
	})
	.catch(function (error) {
		is_online=false;
	});
	
	setTimeout(function(){
			if(nivelu==2) $('.itemadmin').css('display', 'none');
	}, 500);
	
	//si el usuario logueado no es administrador
	if(nivelu==2) $('#miembros_btn, #nproyecto_op, #nuser_op').css('display', 'none');
	
	$('#logologin2').click(function(){
		document.location.href="index.html";
	});
	
	//Detectar desde donde se está accediendo a la app
	var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
	if ( app ) {
		// cordova
		incordova = true;
		inweb = false;
	} else {
		// pc
		incordova = false;
		inweb = true;
	}  
	//POUCHDB TEST *****************
	/*db = new PouchDB('capital');
	var remoteCouch = false;
	
	var proyectos_tbl = {
		_id: new Date().toISOString(),
		nombre: '',
		n_aptos: '',
		n_torres: '',
		imagen: '',
		aprobado: 0
	};
	//insertar
	db.put(proyectos_tbl, function callback(err, result){
		if(!err){
			console.log('Creado!');
			pouchProyectos();
		}else console.log(err);
	});*/
	
});

function pouchProyectos(){
	db.allDocs({
		include_docs:true,
		descending: true
	}, function(err, doc){
		console.log(doc.rows);
	});
}

function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /* Loop through a collection of all HTML elements: */
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /* Make an HTTP request using the attribute value as the file name: */
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /* Remove the attribute, and call this function once more: */
          elmnt.removeAttribute("w3-include-html");
		  $('.dropdown-trigger').dropdown();
          includeHTML();
        }
      }
      xhttp.open("GET", file, true);
      xhttp.send();
      /* Exit the function: */
      return;
    }
  }
}

function logOut(){
	localStorage.removeItem('iduser');
	localStorage.removeItem('nombreu');
	localStorage.removeItem('nivelu');
	localStorage.clear();
	document.location.href='index.html';
}

//******* TOMAR FOTO ************//
var secglobal="";
var contglobal=0;
var tipofoto=0;
var arr_imgs=new Array(); //array q guardará la ruta de cada una de las imágenes tomadas

function tomarFotoGen(idsec){
	secglobal=idsec;
	contglobal+=1;
	navigator.camera.getPicture(cameraSuccess, cameraError, {
		quality: 50,
		destinationType: Camera.DestinationType.FILE_URL //Camera.DestinationType.FILE_URL
	});
}

function tomarFoto(idcarac, x){
	//arr_imgs[idcarac][0]=new Array();
	//alert(typeof arr_imgs[idcarac][0]);
	tipofoto=x; //saber si son fotos de caracs, de reformas, etc.
	secglobal=idcarac;
	contglobal+=1;
	navigator.camera.getPicture(cameraSuccess, cameraError, {
		quality: 50,
		destinationType: Camera.DestinationType.FILE_URL //Camera.DestinationType.FILE_URL
	});
}

contgen=0;
primer_foto=0;
var itemsphoto1 = new Array(); //array q contiene imgs de photoswipe (caracs)
var itemsphoto2 = new Array(); //array q contiene imgs de photoswipe (reformas)
var itemsphoto3 = new Array(); //array q contiene imgs de photoswipe (generales)
var arr_uri1 = new Array();
var arr_uri2 = new Array();
var arr_uri3 = new Array();
var pswpElement = "";
var items = new Array();
var items2 = new Array();
var items3 = new Array();
var options = "";
var oldies=false; //determina si existen imagenes ya guardadas
var nimgs=0;
var uri_global=""; //almacena la uri de la imagen cada vez que se toma una foto
var arr_imgs="";
var arr_imgs_old="";

 function cameraSuccess(imageURI) { 
      //var image = document.getElementById('myImage'); 
      //image.src = "data:image/jpeg;base64," + imageURI;
	  //caracs
	  if(tipofoto==0){
		//$('#cfotos'+secglobal+' .scroll-imgs section').append('<img data-carac="'+secglobal+'" width="100%" id="fotosec'+contglobal+'" src="" /><br>');
		//var image = document.getElementById('fotosec'+contglobal); 
		//image.src = "data:image/jpeg;base64," + imageURI;
		//image.src = imageURI;
		
		uri_global=imageURI;
		
		if(Array.isArray(arr_uri1[secglobal])){
			
		}else arr_uri1[secglobal]=new Array();
		
		if(Array.isArray(itemsphoto1[secglobal])){
			
		}else itemsphoto1[secglobal]=new Array();
		
		//itemsphoto1[secglobal]=new Array();
		
		//arr_uri1[secglobal].push(imageURI);
		$('#imgs'+secglobal).val( $('#imgs'+secglobal).val()+imageURI.substr(imageURI.lastIndexOf('/') + 1)+';' );
		$('#imgs_uris'+secglobal).val( $('#imgs_uris'+secglobal).val()+imageURI+';' );
		
		arr_imgs=$('#imgs'+secglobal).val().split(';');
		arr_uri1[secglobal]=$('#imgs_uris'+secglobal).val().split(';');
		
		
		//alert(itemsphoto1[0].src);
		//return false;
		$('#countimg'+secglobal).val( ( parseInt($('#cont_imgs'+secglobal).text()) + 1 ) );
		$('#cont_imgs'+secglobal).text( '+'+( parseInt($('#cont_imgs'+secglobal).text()) + 1 ) );
		/*$('#cont_imgs'+secglobal).click(function(){
			c=$(this).attr('datac');
			//set config photoswipe
			setPhotoswipe(c);
			// Initializes and opens PhotoSwipe
			var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
			gallery.init();
		});*/
		
	  }
	  //fotos de reformas
	  if(tipofoto==1){
		//$('#cfotos'+secglobal+' .scroll-imgs').css('height', '12rem');
		//$('#cfotos'+secglobal+' .scroll-imgs section').append('<img data-carac="'+secglobal+'" width="100%" id="fotosec'+contglobal+'" src="" /><br>');
		//var image = document.getElementById('fotosec'+contglobal); 
		//image.src = "data:image/jpeg;base64," + imageURI;
		//image.src = imageURI;  
		$('#fotosr'+secglobal).val( $('#fotosr'+secglobal).val()+imageURI.substr(imageURI.lastIndexOf('/') + 1)+';' );
	  }
	  //fotos generales
	  
	  if(tipofoto==2){
		index_acum=0;
		arr_uri3=new Array();
		itemsphoto3=new Array();
		gindice=0;
		$('#imgsg').val( $('#imgsg').val()+imageURI.substr(imageURI.lastIndexOf('/') + 1)+';' );
		$('#imgsg_uris').val( $('#imgsg_uris').val()+imageURI+';' );
		
		arr_imgsg=$('#imgsg').val().split(';');
		arr_uri3=$('#imgsg_uris').val().split(';');
		
		arr_imgsg_old=$('#imgsg_old').val().split(';');
		for(x=0;x<arr_imgsg_old.length;x++){ 
			if(!(arr_imgsg_old[x]=='')){
				itemsphoto3[x]={};
				itemsphoto3[x].src=arr_imgsg_old[x];
				itemsphoto3[x].w=1200;
				itemsphoto3[x].h=900;
			}
		}
		
		for(x=0;x<arr_imgsg.length;x++){
			if(!(arr_imgsg[x]=='') && !(arr_uri3[x]=='')){
				if(itemsphoto3.length>0){
					indice_existente=itemsphoto3.length;
					if(gindice==0) gindice=indice_existente;
					itemsphoto3[gindice]={};
					itemsphoto3[gindice].src=arr_uri3[x];
					itemsphoto3[gindice].w=1200;
					itemsphoto3[gindice].h=900;
					gindice++;
				}else{
					nimgs++;
					//almacenar galería por cada característica
					itemsphoto3[x]={};
					itemsphoto3[x].src=arr_uri3[x];
					itemsphoto3[x].w=1200;
					itemsphoto3[x].h=900;
					index_acum++;
				}
			}
		}
		
		
		$('#cont_imgsg').text( '+'+( parseInt($('#cont_imgsg').text()) + 1 ) );
		
		/*$('#cont_imgsg').click(function(){
			if($('#imgsg_old').val() != '' || $('#imgsg').val() != ''){
				setPhotoswipe(0);
				// Initializes and opens PhotoSwipe
				var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items3, options);
				gallery.init();
			}
		});*/
	  }
	  
	 
	//primer_foto=1;
	  
	  //alert($('#fotosec'+contglobal).length);
	  //alert($('#fotosec'+contglobal).attr('src'));
	  //añadir ruta de la imagen al array de rutas de imagen
	  
	  uploadFoto(imageURI);
   }

function setPhotoswipe(c){
	/********** PHOTOSWIPE *********/
	//alert(c);
	$('.pswp').remove();
	/*html_pswp='<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">';
	html_pswp+='<div class="pswp__bg"></div><div class="pswp__scroll-wrap"><div class="pswp__container">';
	html_pswp+='<div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div>';
	html_pswp+='<div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div>';
	html_pswp+='<div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div>';
	html_pswp+='<button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button>';
	html_pswp+='<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>';
	html_pswp+='<div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut">';
	html_pswp+='<div class="pswp__preloader__donut"></div></div></div></div></div>';
	html_pswp+='<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div>';
	html_pswp+='</div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>';
	html_pswp+='<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>';
	html_pswp+='<div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>';*/
	html_pswp='<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"><!-- Background of PhotoSwipe. Its a separate element as animating opacity is faster than rgba(). --> <div class="pswp__bg"></div><div class="pswp__scroll-wrap"><!-- Container that holds slides. PhotoSwipe keeps only 3 of them in the DOM to save memory. Dont modify these 3 pswp__item elements, data is added later on. --> <div class="pswp__container"> <div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"> <div class="pswp__top-bar"> <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <!--<button class="pswp__button pswp__button--share" title="Share"></button>--> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button> <div class="pswp__preloader"> <div class="pswp__preloader__icn"> <div class="pswp__preloader__cut"> <div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"> <div class="pswp__share-tooltip"></div></div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button> <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button> <div class="pswp__caption"> <div class="pswp__caption__center"></div></div></div></div></div>';
	
	$('body').prepend(html_pswp);
	//alert($('.pswp').length);
	pswpElement = document.querySelectorAll('.pswp')[0];

	// build items array
	/*items = [
		{
			src: 'https://placekitten.com/600/400',
			w: 600,
			h: 400
		},
		{
			src: 'https://placekitten.com/1200/900',
			w: 1200,
			h: 900
		}
	];*/
	items=new Array();
	items2=new Array();
	items3=new Array();
	//alert('items->'+items.length+' c->'+c);
	if(tipofoto==0) items = itemsphoto1[c];
	if(tipofoto==1) items = itemsphoto2;
	if(tipofoto==2) items3 = itemsphoto3;
	//alert(itemsphoto1[c][0].src);

	// define options (if needed)
	options = {
		// optionName: 'option value'
		// for example:
		index: 0, // start at first slide
		modal: true
	};

	/********** PHOTOSWIPE *********/
}   
   
   function cameraError(message) { 
      alert('Failed because: ' + message); 
   }

   function uploadFoto(img){
	    //variable que concatena las imagenes para bd
		str_imgs='';
		/*if(arr_imgs.length>0){
			for(x=0;x<arr_imgs.length;x++){
				options = new FileUploadOptions();
				options.fileKey = "file";
				options.fileName = arr_imgs[x].substr(arr_imgs[x].lastIndexOf('/') + 1);
				options.mimeType = "image/jpeg";
				str_imgs+=arr_imgs[x].substr(arr_imgs[x].lastIndexOf('/') + 1)+';';

				params = {};
				params.value1 = "test";
				params.value2 = "param";

				options.params = params;
				options.chunkedMode = false;

				ft = new FileTransfer();
				ft.upload(arr_imgs[x], "api/product/read.php?accion=camera", function(result){
					//alert('successfully uploaded ' + result.response);
				}, function(error){
					alert('error : ' + JSON.stringify(error));
				}, options);
			}
		}*/
		
		options = new FileUploadOptions();
		options.fileKey = "file";
		options.fileName = img.substr(img.lastIndexOf('/') + 1);
		options.mimeType = "image/jpeg";
		str_imgs+=img.substr(img.lastIndexOf('/') + 1)+';';

		params = {};
		params.value1 = "test";
		params.value2 = "param";

		options.params = params;
		options.chunkedMode = false;

		ft = new FileTransfer();
		ft.upload(img, "api/product/read.php?accion=camera", function(result){
			//alert('successfully uploaded ' + result.response);
		}, function(error){
			alert('error : ' + JSON.stringify(error));
		}, options);
   }
 
 
function print_r(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects 
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += print_r(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}



/*********************************** SECCIONES ******************************/
$(document).ready(function(){
	/*var pswpElement = document.querySelectorAll('.pswp')[0];

	// build items array
	var items = [
		{
			src: 'https://placekitten.com/600/400',
			w: 600,
			h: 400
		},
		{
			src: 'https://placekitten.com/1200/900',
			w: 1200,
			h: 900
		}
	];

	// define options (if needed)
	var options = {
		// optionName: 'option value'
		// for example:
		index: 0 // start at first slide
	};

	// Initializes and opens PhotoSwipe
	var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
	gallery.init();*/
	$('.sidenav').sidenav(); 
 includeHTML(); 
 $('.dropdown-trigger').dropdown();
	$('#tm1').text(localStorage.getItem('pactual'));
	$('#tm2').text(localStorage.getItem('torre_actual')+': '+localStorage.getItem('nomenactual')+' - '+$('#tm2').text());
	$('.modal').modal();
	
	$('#narchivo').change(function(e){
		var fileName = e.target.files[0].name;
		$('#narchivo_txt').text(fileName);
	});
  });
  
  function openImgs(idc){
	//si existen imgs previas o si se han tomado fotos
	tipofoto=0;
	if($('#imgs_old'+idc).val() != '' || $('#imgs'+idc).val() != ''){
		secglobal=idc;
		
		if(Array.isArray(arr_uri1[secglobal])){
			
		}else arr_uri1[secglobal]=new Array();
		
		if(Array.isArray(itemsphoto1[secglobal])){
			
		}else itemsphoto1[secglobal]=new Array();
		
		if(Array.isArray(arr_imgs)){
			arr_uri1[secglobal]=$('#imgs_uris'+secglobal).val().split(';');
			for(x=0;x<arr_imgs.length;x++){
				if(!(arr_imgs[x]=='') && !(arr_uri1[secglobal][x]=='')){
					nimgs++;
					//almacenar galería por cada característica
					itemsphoto1[secglobal][x]={};
					itemsphoto1[secglobal][x].src=arr_uri1[secglobal][x];
					itemsphoto1[secglobal][x].w=1200;
					itemsphoto1[secglobal][x].h=900;
				}
			}
		}
		
		arr_imgs_old=$('#imgs_old'+secglobal).val().split(';');
		//indica en que posicionse encuentra el array 
		//de fotos en caso q ya el array contenga elementos:
		var indice_existente=0;
		var cindice=0;
		//ALMACENAR EN ARRAY LAS IMAGENES Q YA EXISTÍAN DESDE LA REV. ANTERIOR
		for(x=0;x<arr_imgs_old.length;x++){
			if(!(arr_imgs_old[x]=='')){
				nimgs++;
				//almacenar galería por cada característica
				if($('#imgs'+secglobal).val()==''){ //si no se han tomado fotos aún
					itemsphoto1[secglobal][x]={};
					itemsphoto1[secglobal][x].src=arr_imgs_old[x];
					itemsphoto1[secglobal][x].w=1200;
					itemsphoto1[secglobal][x].h=900;
				}else{ //si ya se han tomado fotos
					//alert('l->'+itemsphoto1[secglobal].length);
					indice_existente=itemsphoto1[secglobal].length;
					if(cindice==0) cindice=indice_existente;
					itemsphoto1[secglobal][cindice]={};
					itemsphoto1[secglobal][cindice].src=arr_imgs_old[x];
					itemsphoto1[secglobal][cindice].w=1200;
					itemsphoto1[secglobal][cindice].h=900;
					cindice++;
				}
				oldies=true;
			}
		}
		
		//set config photoswipe
		setPhotoswipe(idc);
		// Initializes and opens PhotoSwipe
		var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
		gallery.init();
		arr_uri1[secglobal]=new Array();
		itemsphoto1[secglobal]=new Array();
	}
  }
  
  function secSaveAll(estado){
	//si se seleccona q no es una nueva visita, no se tienen en cuenta los datos
	//pero si nvisita=false pero se ha creado una custom, entonces si deja
	if(localStorage.getItem('saved_custom')=='1') nvisita=true;
	if(!nvisita && !localStorage.getItem('entrega')=='e' && !localStorage.getItem('saved_custom')=='1'){
		alert("No se ha confirmado que ésta es una nueva vista, por lo tanto los datos no se guardarán");
		document.location.href="apartamentos.html";
		return false;
	}
	
	//recorrer primer grupo de campos
	nok=0;
	ok=0;
	for(x=0;x<$('.collapsible.secs li').length;x++){
		//alert($('.collapsible-header').eq(0).find('.aprobado_sec').length);
		for(i=0;i<$('.collapsible.secs li').eq(x).find('.aprobado_sec').length;i++){
			valor=$('.collapsible.secs li').eq(x).find('.aprobado_sec').eq(i).val();
			if(estado==0){
				if(valor==1) ok++;
				if(valor==0) nok++;
			}else{
				if(valor==0) nok++;
			}
		}
	}
	
	//recorrer segundo grupo de campos (tabla)
	if($('.trdata').length>0){
		$('.trdata').each(function(x){
			valor=$('.trdata').eq(x).find('.aprobado').val();
			if(estado==0){
				if(valor==1) ok++;
				if(valor==0) nok++;
			}else{
				if(valor==0) nok++;
			}
		});
	}
	
	//reprogramado
	if(estado==0 && nok==0){
		alert('Para que un apartamento sea reprogramado, debe contener al menos una característica pendiente');
		return false;
	}
	//if( (estado==1 && nok>0) || (estado==1 && nok==0 && ok==0) )
	if( estado==1 && nok>0 ){ 
		alert('Una o más características no han sido aprobadas');
		return false;
	}
	
	//actualizar características de secciones
	for(x=0;x<$('.collapsible.secs li').length;x++){
		
		for(i=0;i<$('.collapsible.secs li').eq(x).find('.aprobado_sec').length;i++){
			//SABER SI ES CUSTOM
			licustom=($('.collapsible.secs li').eq(x).find('.aprobado_sec').eq(i).hasClass('custom'))?1:0;
			valor=$('.collapsible.secs li').eq(x).find('.aprobado_sec').eq(i).val();
			obs=$('.collapsible.secs li').eq(x).find('.obs_sec').eq(i).val();
			idc=$('.collapsible.secs li').eq(x).find('.spancarac').eq(i).attr('data-carac');
			//alert(idc);
			arr_old=new Array();
			$('#imgs_old'+idc).val('');
			if($('#imgs_old'+idc).length>0) arr_old=$('#imgs_old'+idc).val().split(';');
			arr_old_str="";
			for(m=0;m<arr_old.length;m++){
				if(!(arr_old[m]=='')) arr_old_str+=arr_old[m].substr(arr_old[m].lastIndexOf('/') + 1)+';';
			}
			$('#imgs_old'+idc).val(arr_old_str);
			/*alert($('#imgs_old'+idc).val());
			return false;*/
			$('#imgs'+idc).val( $('#imgs_old'+idc).val()+$('#imgs'+idc).val() );
			$('#imgs_old'+idc).val('');
			var formData = {
				'accion': 'estadocarac',
				'estado': valor,
				'estadogen': estado,
				'obs': obs,
				'idapto': localStorage.getItem('apto_actual'),
				'imgs': $('#imgs'+idc).val(),
				'idcarac': idc,
				'custom': licustom
			};
			//si estamos en entregas
			if(localStorage.getItem('entrega')=='e') formData.entrega='e';
			console.log('formData->');
			console.log(formData);
			
			if(nvisita) formData.nvisita='1';
			
			$.post(server+'api/product/create.php', formData, function(data){
				if(data.res==1){
				
				}//else alert('Ha ocurido un error'+data)
			});
		}
		//actualizar estado del apto
		var nreforma_save="";
		if(!(localStorage.getItem('nreforma') == null) && !(localStorage.getItem('nreforma') == '')) nreforma_save=localStorage.getItem('nreforma');
		arr_old=new Array();
		if($('#imgsg').length>0) arr_old=$('#imgsg_old').val().split(';');
		arr_old_str="";
		for(m=0;m<arr_old.length;m++){
			if(!(arr_old[m]=='')) arr_old_str+=arr_old[m].substr(arr_old[m].lastIndexOf('/') + 1)+';';
		}
		$('#imgsg_old').val(arr_old_str);
		$('#imgsg').val( $('#imgsg_old').val()+$('#imgsg').val() );
		arr_idsc=new Array();
		//almacena ids de las caracs
		$('span.spancarac').each(function(i){
			arr_idsc[i]=$(this).attr('data-carac');
		});
		var formData = {
			'accion': 'estadoapto',
			'estadogen': estado,
			'nreforma': nreforma_save,
			'obsg': $('#obsg').val(),
			'idapto': localStorage.getItem('apto_actual'),
			'imgs': $('#imgsg').val(),
			'caracs': arr_idsc,
			'tipoid': localStorage.getItem('tipoactual_id')
		};
		//si estamos en entregas
		if(localStorage.getItem('entrega')=='e') formData.entrega='e';
		
		if(nvisita) formData.nvisita='1';
		
		$.post(server+'api/product/create.php', formData, function(data){
			if(data.res==1){
				localStorage.setItem('noexit', '0');
			
			}//else alert('Ha ocurido un error'+data)
		});
	}
	
	//guardar tabla de reformas (en entregas no se guardan ni se añaden reformas)
	if($('.trdata').length>0){
		//alert('helouu');
		if(!(localStorage.getItem('entrega')=='e')){
			$('#tablar section table').each(function(t){
				indice_r=t;
				idtabla=$(this).attr('id');
				//alert($('#'+idtabla+' .trdata').length);
				$('#'+idtabla+' .trdata').each(function(x){
					idref=$(this).attr('data-id');
					indice=$('#'+idtabla+' .trdata').eq(x).attr('data-tr');
					valor=$('#'+idtabla+' .trdata').eq(x).find('.aprobado').val();
					obsr=$('#'+idtabla+' .trdata').eq(x).find('.obsr').val();
					imgs=$('#'+idtabla+' .trdata').eq(x).find('.fotosr').val();
					var formData = {
						'accion': 'savetablar',
						'desc': $('#'+idtabla+' #tdesc'+indice).val(),
						'cant': $('#'+idtabla+' #tcant'+indice).val(),
						'vx': $('#'+idtabla+' #tvx'+indice).val(),
						'vy': $('#'+idtabla+' #tvy'+indice).val(),
						'estado': valor,
						'estadogen': estado,
						'obsr': obsr,
						'imgs': imgs,
						'cont': x,
						'idref': idref,
						'indice': indice_r,
						'idapto': localStorage.getItem('apto_actual')
					};
					console.log(formData);
					//si estamos en entregas
					if(localStorage.getItem('entrega')=='e') formData.entrega='e';
					
					if(nvisita) formData.nvisita='1';
					
					$.post(server+'api/product/create.php', formData, function(data){
						if(data.res==1){
							//$('guardado');
						}//else alert('Ha ocurido un error'+data)
					});
				});
			});
		}//else //alert('wtf');
	}
	
	//guardar observ. y fotos generales
	localStorage.setItem('saved_custom', 0);
	
	var formData = {
		'accion': 'generales',
		'idapto': localStorage.getItem('apto_actual'),
		'fotos': $('#imgsg').val(),
		'obs': $('#obsg').val()
	};
	//si estamos en entregas
	if(localStorage.getItem('entrega')=='e') formData.entrega='e';
	
	$.post(server+'api/product/create.php', formData, function(data){
		if(data.res==1){
			//si estamos en entregas y el estado es ok (listo para ser entregado)
			if(localStorage.getItem('entrega')=='e' && estado==1){
				$('#ependiente, #aptok').fadeOut(150);
				$('#e_siguiente').css('display', 'block');
			}else{ 
				alert('Datos guardados con éxito');
				document.location.href='apartamentos.html';
			}
		}//else alert('Ha ocurido un error'+data)
	});
	
  }
  
  //guarda los datos cuando se añade una nueva reforma
  function saveNreforma(){
	$('#idaptoh').val(localStorage.getItem('apto_actual'));
	
	var formData = new FormData($("#frm_nreforma")[0]);

	$.ajax({
		url:server+'api/product/create.php',
		type:"POST",
		data:formData,
		processData: false,
		contentType: false,
		success:function(res){
			//res = JSON.parse(res);
			//$('.mk-spinner-ring.loader').css('display','none');
			if(res.res==1){
				alert('El nuevo archivo de reformas ha sido reemplazado con éxito');
				localStorage.setItem('nreforma', res.nreforma);
				//document.location.reload();
					//(localStorage.getItem('apto_actual'));
				$('#modal2').modal('close');
				location.reload();
				//Obtener el id del proyecto actual
				//localStorage.setItem('idseccion', res.idseccion);
				
			}else alert('Ha ocurido un error'+res);
		}

	});
  }
  
  function e_siguiente(){
	$('#secciones_cont').fadeOut(150);
	$('#form_entrega').fadeIn(150);
  }
  
  //guarda cada uno de los items de la reforma en la tabla reformas
  function saveRitems(){
	var formData = {
        'accion': 'ritems',
	}
	$.post(server+'api/product/create.php', formData, function(data){
		if(data.res==1){
		}else alert('Ha ocurido un error'+data)
	});
  }
  var nvisita=false;
 
  
  function secState(state, x, custom){
	if(!($('#ok'+x).hasClass('activo'))){
		elm=(custom)?'_custom':'';
		if(state==1){
			$('#obs'+x+elm).attr('disabled', true);
			$('#ok'+x+elm).css('opacity', 1);
			$('#nok'+x+elm).css('opacity', 0.4);
		}else{
			$('#obs'+x+elm).attr('disabled', false);
			$('#ok'+x+elm).css('opacity', 0.4);
			$('#nok'+x+elm).css('opacity', 1);
		}
		$('#aprobado_sec'+x+elm).val(state);
	}
  }
	
function secStater(state, x){
	x=x.attr('data-id');
	if(state==1){
		$('#okk'+x).css('opacity', 1);
		$('#nokk'+x).css('opacity', 0.4);
		$('#obsr'+x).attr('disabled', true);
	}else{
		$('#okk'+x).css('opacity', 0.4);
		$('#nokk'+x).css('opacity', 1);
		$('#obsr'+x).attr('disabled', false);
	}
	$('#aprobado'+x).val(state);
}
	
  
var apto_ok=0;
var entregado=0;
function loadSecs(idp){
	$('.mk-spinner-ring.loader').css('display','block');
	
	edit=($('.vertipos').length>0)?1:'';
	//centrega=(localStorage.getItem('entrega')=='e')?'&centrega':'';
	centrega=(localStorage.getItem('entrega')=='e')?'&centrega':'';
	
	if(localStorage.getItem('entrega')=='e') $('#secciones_cont').addClass('secs_entrega');
	
	axios.get(server+'api/product/read.php?accion=getsecs&idp='+idp+'&idapto='+localStorage.getItem('apto_actual')+'&edit='+edit+centrega)
	.then(function (response) {
		//console.log(response.data.html);
		//elhtml=response.data.html;
		$('.mk-spinner-ring.loader').css('display','none');
		if(response.data.res==1){ 
			cont_imgsg=0;
			$('#tipoapto').text('Tipo de apartamento: '+response.data.ntipo);
			localStorage.setItem('tipoactual', response.data.ntipo);
			localStorage.setItem('tipoactual_id', response.data.tipoid);
			$('.collapsible').append(response.data.secs);
			$('#obsg').val(response.data.obsg);
			$('#imgsg_old').val(response.data.imgsg);
			entregado=response.data.entregado;
			//arr_imgsg_old='';
			arr_imgsg=response.data.imgsg.split(';');
			for(x=0;x<arr_imgsg.length;x++){ 
				if(!(arr_imgsg[x]=='')){
					cont_imgsg++;
					itemsphoto3[x]={};
					itemsphoto3[x].src=arr_imgsg[x];
					itemsphoto3[x].w=1200;
					itemsphoto3[x].h=900;
				}
			}
			$('#cont_imgsg').text('+'+cont_imgsg);
			$('#cont_imgsg').click(function(){
				if($('#imgsg_old').val() != '' || $('#imgsg').val() != ''){
					tipofoto=2;
					setPhotoswipe(0);
					// Initializes and opens PhotoSwipe
					var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items3, options);
					gallery.init();
				}
			});
			if(response.data.estadoapto==1){
				apto_ok=1;
				$('#secciones_cont').prepend('<div class="checkhist">Este apartamento se encuentra en estado aprobado, para conocer detalles sobre las revisiones del mismo, dirigirse a la sección de historial.<div>');
				$('#secciones_cont a').eq(0).css('display', 'none');
				$('.clist label, #reprogramado, #aptok, #edit_reforma, #edit_reformat, .sec_state, .camerac, #pics_general, #obsg').css('display', 'none');
				$('#edit_reforma').unbind(); //quitar evento de click si está aprobado (solo lectura)
				$('.clist .obs_sec').attr('disabled', true); //deshabilitar textareas y botones (solo lectura)
				
				if(localStorage.getItem('entrega')=='e'){ 
					//$('#secciones_cont').addClass('secs_entrega');
					$('.obs_sec, .sec_state').css('display', 'block');
					$('.obs_sec').attr('disabled', false);
					//$('.obs_sec').val('');
					$('#obsg, .obsg').css('display', 'block');
					$('#aptok').css('display', 'inline-block');
					$('#pics_general').after('<a style="width: 48%" id="ependiente" class="waves-effect waves-light btn" href="javascript:void(0);" onclick="secSaveAll(0);">No entregado</a>');
					
					if(entregado==1){
						$('#ependiente, #aptok').fadeOut(80);
						$('#e_siguiente').css('display', 'block');
					}
					//$('#obsg').val('');

				}else $('.input-field.obsg').append('<b>Observaciones generales: </b><br>'+response.data.obsg);
			}
			//cargar tabla de reformas
			loadTablar(localStorage.getItem('apto_actual'));
		}else alert('No se encontraron secciones');
	})
	.catch(function (error) {
		console.log(error);
	});
	
  }
  
  function delReforma(apto, index){
	if(confirm('¿Eliminar reforma?')){
		var formData = {
			'accion': 'delreforma',
			'idapto': apto,
			'indice': index
		};
		$.post(server+'api/product/create.php', formData, function(data){
			if(data.res==1){
				$('#tablar section').fadeOut(280, function(){
					$('#tablar section').html('');
					loadTablar(localStorage.getItem('apto_actual'));
				});
			}else alert('Ha ocurido un error'+data)
		});
	}
  }
  
  function loadTablar(apto, fecha){
	//rentrega=(localStorage.getItem('entrega')=='e')?'&entrega':'';
	$('#tablar section').html('');
	rentrega='';
	//alert('tablar');
	lafecha=(fecha)?'&fecha='+fecha:'';
	axios.get(server+'api/product/read.php?accion=readxls&idapto='+apto+rentrega+lafecha)
	.then(function (response) {
		if(response.data.res==1){ 
			if(! localStorage.getItem('entrega')=='e') $('#edit_reforma').css('display','block');
			arr_tabla=response.data.tabla;
			if(response.data.bd==0){ //si la resforma no tiene observaciones ni visitas registradas (primera vez) 
				for(x=0;x<arr_tabla.length;x++){
					campos_reforma='<td class="tddata" style="border:1px solid #c1b8b8">';
					campos_reforma+='<div class="input-field"><textarea placeholder="Observaciones" class="materialize-textarea" name="obsr" ></textarea>';
					campos_reforma+='<label for="obst"></div>';
					campos_reforma+='<input type="hidden" class="descr" name="descr" />';
					campos_reforma+='<input type="hidden" name="cantr" class="cantr" />';
					campos_reforma+='<input type="hidden" name="valorx" class="valorx" />';
					campos_reforma+='<input type="hidden" name="valory" class="valory" />';
					campos_reforma+='<input type="hidden" name="fotosr" class="fotosr" />';
					campos_reforma+='<input type="hidden" name="aprobado" class="aprobado" value="0" />';
					campos_reforma+='<div class="clear"></div></td>';
					
					$('#tablar section').append(arr_tabla[x]);
					
					//alert(apto_ok);
					$('#treforma'+x+' .trtop').eq(0).prepend('<td class="tdhead" style="border:1px solid #c1b8b8">Aprobar</td>');
					$('#treforma'+x+' .trtop').eq(0).append('<td class="tdhead" style="border:1px solid #c1b8b8;width:28%">Observaciones</td>');
					$('#treforma'+x+' .trtop').eq(0).append('<td class="tdhead" style="border:1px solid #c1b8b8">Fotos <span class="delreforma" onclick="delReforma('+localStorage.getItem('apto_actual')+', '+x+');">delete</span></td>');
					$('#treforma'+x+' .trdata').prepend('<td class="tddata" style="border:1px solid #c1b8b8"><div class="sec_state"><img id="ok1_1" width="20" src="img/ok.png"><img id="nok1_1" width="20" src="img/xx.png" ><div class="clear"></div></div></td>');
					$('#treforma'+x+' .trdata').append(campos_reforma);
					$('#treforma'+x+' .trdata').append('<td class="tddata" style="border:1px solid #c1b8b8"><img class="lacamera" src="img/camera.png" width="20%"></td>');
					$('#treforma'+x+' .tddata textarea').addClass('obsr');
					
					$('.obsr').each(function(i){
						$('#treforma'+x+' .tddata .sec_state').eq(i).find('img').eq(0).attr('id', 'okk'+i+'_'+x);
						$('#treforma'+x+' .tddata .sec_state').eq(i).find('img').eq(1).attr('id', 'nokk'+i+'_'+x);
						
						$('#treforma'+x+' .tddata .obsr').eq(i).attr('id', 'obsr'+i+'_'+x);
						$('#treforma'+x+' .tddata .descr').eq(i).attr('id', 'descr'+i+'_'+x);
						$('#treforma'+x+' .tddata .cantr').eq(i).attr('id', 'cantr'+i+'_'+x);
						$('#treforma'+x+' .tddata .valorx').eq(i).attr('id', 'valorx'+i+'_'+x);
						$('#treforma'+x+' .tddata .valory').eq(i).attr('id', 'valory'+i+'_'+x);
						$('#treforma'+x+' .tddata .fotosr').eq(i).attr('id', 'fotosr'+i+'_'+x);
						$('#treforma'+x+' .tddata .aprobado').eq(i).attr('id', 'aprobado'+i+'_'+x);
						
						$('#treforma'+x+' .tddata .lacamera').eq(i).attr('id', 'lacamera'+i+'_'+x);
						$('#treforma'+x+' .tddata .lacamera').eq(i).click(function(){
							//tomarFoto(x, 1);
						});
					});
				}
				
				for(j=0;j<arr_tabla.length;j++){
					$('#treforma'+j+' .tddata .sec_state').each(function(i){
						$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(0).attr('data-id', i+'_'+j);
						$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(1).attr('data-id', i+'_'+j);
						
						if($('.secs_entrega').length==0){
							$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(0).click(function(){
								secStater(1, $(this));
							});
							$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(1).click(function(){
								secStater(0, $(this));
							});
						}
					});
				}
			}else{ 
				for(i=0;i<arr_tabla.length;i++){
					$('#tablar section').append(arr_tabla[i]);
					
					$('#treforma'+i+' .obsr').each(function(x){
						$('.tddata .sec_state').eq(x).find('img').eq(0).attr('id', 'okk'+x+'_'+i);
						$('.tddata .sec_state').eq(x).find('img').eq(1).attr('id', 'nokk'+x+'_'+i);
						
						$('#treforma'+i+' .tddata .obsr').eq(x).attr('id', 'obsr'+x+'_'+i);
						$('#treforma'+i+' .tddata .descr').eq(x).attr('id', 'descr'+x+'_'+i);
						$('#treforma'+i+' .tddata .cantr').eq(x).attr('id', 'cantr'+x+'_'+i);
						//$('.tddata .valorx').eq(x).attr('id', 'valorx'+x);
						//$('.tddata .valory').eq(x).attr('id', 'valory'+x);
						$('#treforma'+i+' .tddata .fotosr').eq(x).attr('id', 'fotosr'+x+'_'+i);
						$('#treforma'+i+' .tddata .aprobado').eq(x).attr('id', 'aprobado'+x+'_'+i);
						
						$('#treforma'+i+' .tddata .lacamera').eq(x).attr('id', 'lacamera'+x+'_'+i);
						$('#treforma'+i+' .tddata .lacamera').eq(x).click(function(){
							//tomarFoto(x, 1);
						});
					});
				}
				
				for(j=0;j<arr_tabla.length;j++){
					$('#treforma'+j+' .tddata .sec_state').each(function(i){
						$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(0).attr('data-id', i+'_'+j);
						$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(1).attr('data-id', i+'_'+j);
						$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(0).attr('id', 'okk'+i+'_'+j);
						$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(1).attr('id', 'nokk'+i+'_'+j);
						
						if($('.secs_entrega').length==0){
							$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(0).click(function(){
								secStater(1, $(this));
							});
							$('#treforma'+j+' .tddata .sec_state').eq(i).find('img').eq(1).click(function(){
								secStater(0, $(this));
							});
						}
					});
				}
			}
			
			if(apto_ok==0){
				
			}else{ 
				$('.obsr').attr('disabled', true);
				$('span.delreforma').remove();
			}
		}
		//else alert('No se encontraron secciones');
	})
	.catch(function (error) {
		console.log(error);
	});
  }
  
  function goCaracs(id){
	localStorage.setItem('sec_actual', id);
	document.location.href='caracteristicas.html';
  }
  
/*********************************** SECCIONES ******************************/


/*********************************** PROYECTOS ******************************/
var contprev=0;
var numaptos_g=0;
var alpiso_g=0;
var multiplicacion=0;

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    //var instances = M.Modal.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
	setTimeout(function(){
		$('.sidenav').sidenav();
	}, 500);
 includeHTML(); 
 $('.dropdown-trigger').dropdown();
	$('.modal').modal({dismissible: false});

    $('#ntorres').keyup(function(){
        if(!($('#ntorres').val()=='')){
            $('#frm_nproyecto #savetorres').fadeIn(60);
        }
    });
  });
  
  var updsec=false;
  var updsec_id=0;
  function editSec(id, x, nsec){
	//alert(id);
	if($('#psec'+id+' .txt').length==0){
		updsec=true;
		newSection();
		updsec_id=id;
		localStorage.setItem('idseccion', id);
		$('#nseccion').val(nsec);
		$('#editseccion').css('display', 'inline-block');
		$('#crearseccion').css('display', 'none');
	}else{
		//alert($('#psec'+id+' .txt').text());
		newSection();
		updsec_id=id;
		localStorage.setItem('idseccion', id);
		$('#nseccion').val($('#psec'+id+' .txt').text());
		$('#editseccion').css('display', 'inline-block');
		$('#crearseccion').css('display', 'none');
		
		$('#savetipo').css('display','none');
		$('#step1 h4, #step1 #ntipo').css('display','none');
		$('#btnseccion').css('display','inline-block');
		$('#btn_actions').css('display','block');
		$('#modal1').modal('open');
	}
	$('#nseccion').css('display', 'block');
	$('#btn_actions, #sdefault_cont').css('display', 'none');
  }
  
  function updSection(txt){
	var formData = {
		'accion': 'updsec',
		'id': updsec_id,
		'sec': txt
	};
	$.post(server+'api/product/create.php', formData, function(data){
		if(data.res==1){
			$('#frm_seccion').fadeOut(60);
			$('#frm_seccion2').fadeIn(60);
			$('#editcaracs').css('display', 'block');
			$('#saveseccion').css('display', 'none');
			$('.prev-cont').each(function(x){
				if($(this).attr('data-id')==updsec_id){
					$(this).find('.secprev span').text(txt);
					padre=$(this);
					padre.find('.cprev').each(function(i){
						$('.caracsdn').eq(i).val($(this).text());
					});
				}
			});
			
		}else alert('Ha ocurido un error'+data)
	});
  }
  
  function editCaracs(){
	saveCaracs();
  }
  
  function deleteSec(id, capa){
	if(confirm("¿Eliminar sección?")){
		$('#prev-cont'+capa).fadeOut(120,function(){
			$('#prev-cont'+capa).remove();
		});
		var formData = {
			'accion': 'delsec',
			'id': id,
		};
		$.post(server+'api/product/create.php', formData, function(data){
			if(data.res==1){
				nomenc_ok = 1;
				//alert('full');
				
			}else alert('Ha ocurido un error'+data)
		});
	}
  }
  
  var toglecheck=0;
  function checkAll(val){
	//alert($('.checkitem').attr('checked'));
	//checar todos
	if(toglecheck==0){
		$('.checkitem input[type=checkbox]').each(function(){
			if(!($(this).is(':checked'))) $(this).parent().find('span').click();
		});
		toglecheck=1;
	}else{
		$('.checkitem input[type=checkbox]').each(function(){
			if($(this).is(':checked')) $(this).parent().find('span').click();
		});
		toglecheck=0;
	}
	
	/*$('.checkitem span').each(function(){
		$(this).click();
	});*/
	
  }
  
  function setClaseg(val){
	if(val != ''){
		$('.checkitem input[type=checkbox]').each(function(i){
			if($(this).is(":checked")){
				//alert('disab->'+$(this).attr('disabled'));
				if(!($(this).parent().parent().find('.tdclase').hasClass('cassigned'))){
					$(this).attr('disabled', true);
					$(this).parent().parent().css('opacity', 0.7);
					$(this).parent().parent().append('<a href="javascript:void(0);" onclick="dropClase('+$(this).parent().parent().attr('data-id')+');" class="dropc"">Eliminar clase</a>');
					$(this).parent().parent().find('.tdclase').text('Clase: '+$("#tipoaptog option:selected").text());
					$(this).parent().parent().find('.tdclase').addClass('cassigned');
					$('#clase'+$(this).parent().parent().attr('data-id')).val($("#tipoaptog").val());
				}
			}else{
				$(this).parent().parent().find('.tdclase').text('');
				$(this).parent().parent().find('.tdclase').removeClass('cassigned');
				$('#clase'+$(this).parent().parent().attr('data-id')).val('');
			}
		});
	}
  }
  
  function dropClase(i){
	$('#td'+i).css('opacity', 1).find('.dropc').remove();
	$('#td'+i).find('input[type=checkbox]').attr('disabled', false);
	$('#td'+i).find('.tdclase').removeClass('cassigned');
	$('#td'+i).find('.tdclase').text('');
	$('#td'+i+' label span').click();
	$('#clase'+i).val('');
  }
  
  function setClaseapto(obj, index){
	if(!(obj.value=='')) $('#clase'+index).val(obj.value);
  }
  
  function saveReformas(){
	//$('#accion1').val('nseccion');
	noclase=0;
	$('.checkitem input[type=checkbox]').each(function(i){
		if($(this).parent().parent().find('.tdclase').html()=='') noclase++;
	});
	
	if(noclase>0){
		alert('Debes seleccionar una clase para cada apartamento');
		return false;
	}
	
	$('.mk-spinner-ring.loader').css('display','block');
	//alert($(".frm_reformas").eq(0).length);
	var lares="";
	nforms=$('.frm_reformas').length;
	$('#btnsaver').attr('disabled', true);
	$('.frm_reformas').each(function(i){
		//alert(i);
		
		var formData = new FormData($(".frm_reformas").eq(i)[0]);
		
		$.ajax({
			url:server+'api/product/create.php',
			type:"POST",
			data:formData,
			processData: false,
			contentType: false,
			success:function(res){
				//res = JSON.parse(res);
				$('.mk-spinner-ring.loader').css('display','none');
				if(res.res==1){
					//alert('Datos guardados con éxito');
					//Obtener el id del proyecto actual
					//localStorage.setItem('idseccion', res.idseccion);
					lares=res.res;
					//if((i+1) == nforms) alert('Datos guardados con éxito');
					if((i+1) == nforms) document.location.href='proyectos.html';
					else console.log('i->'+i+' nforms->'+nforms);
					
				}else alert('Ha ocurido un error'+res);
			}

		});
		
	});
	
	p_notcomplete=false;
	/*document.location.reload();
	if(lares==1){
		alert('Datos guardados con éxito');
		document.location.reload();
	}*/
  }
  
  //$(document).ajaxStop(function() { //se ejecuta cuando las llamadas ajax hayan terminado
	  //document.location.href='proyectos.html';
  //});

  function getIconoRuta(ruta, img){
    $('#rutaimg').val(ruta);
	$('.cuadro span').css('color', '#383a3c');
	$(img).css('color', '#039be5');
    //alert($('#rutaimg').val());
  }

  function saveTorres(){
    if(!($('#ntorres').val()=='')){
        html_naptos='';
        cont_torres=0;
        if(!($('#ntorres').val()=='')){
            for(x=0;x<$('#ntorres').val();x++){
                html_naptos+='<label>Apartamentos para torre '+(x+1)+' </label><input class="pisosxtorre" type="number" name="pisosxtorre'+x+'" id="pisosxtorre'+x+'" placeholder="N&uacute;mero de apartamentos para  torre '+(x+1)+'">';
                cont_torres++;
            }
            $('#nproyecto_fields').html(html_naptos);
            if(acep_nproy==0){
				$('#save_proyecto').fadeIn(60);
				acep_nproy++;
			}
        }
    }
  }
  
  //AÑADIR NUEVA CARACTERÍSTICA
  var xcaracs = 5;
  function newCarac(){
	xcaracs++;
	$('#caracs_container').append('<input type="text" required class="caracsdn" name="carac'+xcaracs+'" placeholder="CARACTERÍSTICA" /><br>');
  }

  var contadory = 0;
  var pisos_creados = 0;
  var pisos_creados2 = 0;
  
  /*function aptos_piso(frm, x, btn){
	var delpiso = $('#frm_pisos'+frm+' #del'+x).val();
  	var alpiso = $('#frm_pisos'+frm+' #alpiso'+x).val();
  	var npisos = $('#frm_pisos'+frm+' #pisos'+x).val();
	var p_xtorre = $('#pisosxtorre'+frm).val(); //aptos x torre
	var amultiplicar = 0; //variable que guarda la diferencia de números entre 'delpiso' y 'alpiso'

	if(!(piso=='') && !(alpiso=='') && !(npisos=='')){
		for(d=delpiso;d<=alpiso;d++){
			amultiplicar++;
		}
		
		if( parseInt(npisos) * parseInt(amultiplicar) > parseInt(p_xtorre)  ){
			alert('El número de apartamentos totales no coincide con el número de apartamentos para esta torre');
			return false;
		}
		
		piso = parseInt(alpiso);
		
  		//alert(contadory);

        //VALIDAR NOMENCLATURA
        var nomenc1 = parseInt($('#frm_pisos'+frm+' #nomenc'+x+'_1').val());
        var nomenc2 = parseInt($('#frm_pisos'+frm+' #nomenc'+x+'_2').val());
		
		if(nomenc1=='01') nomenc1=1;
		if(nomenc1=='02') nomenc1=2;
		if(nomenc1=='03') nomenc1=3;
		if(nomenc1=='04') nomenc1=4;
		if(nomenc1=='05') nomenc1=5;
		if(nomenc1=='06') nomenc1=6;
		if(nomenc1=='07') nomenc1=7;
		if(nomenc1=='08') nomenc1=8;
		if(nomenc1=='09') nomenc1=9;
		
		if(nomenc2=='01') nomenc2=1;
		if(nomenc2=='02') nomenc2=2;
		if(nomenc2=='03') nomenc2=3;
		if(nomenc2=='04') nomenc2=4;
		if(nomenc2=='05') nomenc2=5;
		if(nomenc2=='06') nomenc2=6;
		if(nomenc2=='07') nomenc2=7;
		if(nomenc2=='08') nomenc2=8;
		if(nomenc2=='09') nomenc2=9;

        if(nomenc1=='' || nomenc2=='') return false;

        if(nomenc1>nomenc2){
            alert('El segundo número ingresado en la nomenclatura no puede ser menor al primero');
            return false;
        }
	}
  }*/
  
  
  function aptos_piso(frm, x, btn){
	$('#savepisosxapto').attr('disabled', true);
	var contador_pisos = 0;
  	var piso = $('#frm_pisos'+frm+' #del'+x).val();
  	var delpiso = $('#frm_pisos'+frm+' #del'+x).val();
  	var alpiso = $('#frm_pisos'+frm+' #alpiso'+x).val();
  	var npisos = $('#frm_pisos'+frm+' #pisos'+x).val();
    var index_x = x;
	var p_xtorre = $('#pisosxtorre'+frm).val(); //aptos x torre
	var amultiplicar = 0; //variable que guarda la diferencia de números entre 'delpiso' y 'alpiso'
	var numaptos_total = 0; //variable que el núm de aptos totales en la torre
	
  	
  	console.log('*** Aptos totales creados x torre MSG 1 ->'+numaptos_total+' ***');
	if(!(piso=='') && !(alpiso=='') && !(npisos=='')){
  		contador_pisos += npisos;
  		piso = parseInt(alpiso);
		
		
		//calcular n de aptos para toda la torre
		for(n=0;n<$('#frm_pisos'+frm+' .numaptos').length;n++){
			multip=0;
			if($('#frm_pisos'+frm+' #del'+n).val() != '' && $('#frm_pisos'+frm+' #alpiso'+n).val() != ''){
				for(d=$('#frm_pisos'+frm+' #del'+n).val();d<=$('#frm_pisos'+frm+' #alpiso'+n).val();d++){
					multip++;
				}
				if($('#frm_pisos'+frm+' .numaptos').eq(n).val() != '') numaptos_total += ( parseInt($('#frm_pisos'+frm+' .numaptos').eq(n).val()) * multip );
			}
		}
		console.log('*** Aptos totales creados x torre->'+numaptos_total+' ***');
		
		for(d=delpiso;d<=alpiso;d++){
			amultiplicar++;
			//alert(amultiplicar);
		}
		
		/*if( parseInt(npisos) * parseInt(amultiplicar) > parseInt(p_xtorre)  ){
			alert('El número de apartamentos totales no coincide con el número de apartamentos para esta torre');
			return false;
		}*/
		if( parseInt(numaptos_total) > parseInt(p_xtorre)  ){
			alert('El número de apartamentos totales no coincide con el número de apartamentos para esta torre');
			return false;
		}
		
		var btn = $(btn);
		var parent_form = btn.parents('form');
		//return false;
		
		
		
  		//alert(contadory);

        //VALIDAR NOMENCLATURA
        var nomenc1 = parseInt($('#frm_pisos'+frm+' #nomenc'+x+'_1').val());
        var nomenc2 = parseInt($('#frm_pisos'+frm+' #nomenc'+x+'_2').val());
		
		if(nomenc1=='01') nomenc1=1;
		if(nomenc1=='02') nomenc1=2;
		if(nomenc1=='03') nomenc1=3;
		if(nomenc1=='04') nomenc1=4;
		if(nomenc1=='05') nomenc1=5;
		if(nomenc1=='06') nomenc1=6;
		if(nomenc1=='07') nomenc1=7;
		if(nomenc1=='08') nomenc1=8;
		if(nomenc1=='09') nomenc1=9;
		
		if(nomenc2=='01') nomenc2=1;
		if(nomenc2=='02') nomenc2=2;
		if(nomenc2=='03') nomenc2=3;
		if(nomenc2=='04') nomenc2=4;
		if(nomenc2=='05') nomenc2=5;
		if(nomenc2=='06') nomenc2=6;
		if(nomenc2=='07') nomenc2=7;
		if(nomenc2=='08') nomenc2=8;
		if(nomenc2=='09') nomenc2=9;

        if(nomenc1=='' || nomenc2=='') return false;

        if(nomenc1>nomenc2){
            alert('El segundo número ingresado en la nomenclatura no puede ser menor al primero');
            return false;
        }
		
		//alert('amultiplicar->'+amultiplicar);
		console.log('amultiplicar->'+amultiplicar);
		for(n=parseInt(nomenc1);n<=parseInt(nomenc2);n++){
			pisos_creados2++;
			//console.log('pi_creados2->'+pisos_creados2);
		}
		
		pisos_creados2 = (pisos_creados2 * amultiplicar);
		console.log('pisos_creados2->'+pisos_creados2+' xtorre->'+p_xtorre);
		
		/*if(numaptos_total>parseInt(p_xtorre)){
			alert('El número de apartamentos excede el número de apartamentos por torre');
			pisos_creados2=0; //se resetea para q se corrija y se valide de nuevo
			console.log('xtorre->'+p_xtorre+' xcreados2->'+pisos_creados2);
			return false;
		}*/
		
		//if(numaptos_total==parseInt(p_xtorre)) pisos_creados2=0;
		
		
		
		//for(x=0;x<)
		//console.log('CREADOS->'+pisos_creados);

        var arr_nomenc = new Array();
        var cont_nomenc = 0;
		
        for(x=parseInt(nomenc1);x<=parseInt(nomenc2);x++){
            cont_nomenc++;
			//alert('cont_nomenc->'+cont_nomenc);
            arr_nomenc[nomenc1];
        }

         if(parseInt(cont_nomenc) != parseInt(npisos)){
            alert('El número de apartamentos por piso no es acorde a la nomenclatura');
			pisos_creados2=0; //se resetea para q se corrija y se valide de nuevo
            return false;
            //alert(cont_nomenc+'--'+npisos);
         }else{
			//alert(cont_nomenc+'--'+npisos);
            //return false;
		 }

        contadory+=1;
		var elindice = parseInt(index_x) + parseInt(1);
		//$('#frm_pisos'+frm+' #del'+elindice).val(parseInt(piso)+parseInt(1));
		//alert('');
		nuevo_valor=parseInt(piso)+parseInt(1)
		//$('#frm_pisos'+frm+' #del0').val(nuevo_valor);
		$('#frm_pisos'+frm+' #del'+elindice).val(nuevo_valor);
		//$('#frm_pisos'+frm+' #del'+elindice).val(nuevo_valor).attr('disabled', true);

  		var xy = alpiso;
		
		localStorage.setItem('alpiso1', alpiso);

		
		//$('#btn_pisos'+index_x).attr('disabled', true);

        xx = nomenc1;
        xx_nom = xx;
        xx_piso_current = xx;
        pisos_concat = '';
        //pisonomenc = 1;
        pisonomenc = $('#frm_pisos'+frm+' #del'+index_x).val();
		//alert('pisonomenc->'+pisonomenc);
        piso_current = 0;
        ceronomenc = '0';
		index_mayora_nueve=1;
		var concat = '';

        //alert(parseInt(delpiso)<=parseInt(alpiso));
        //alert(delpiso+'--'+alpiso);
		//alert(localStorage.getItem('idtorre'+frm));
		for(l=parseInt(delpiso);l<=parseInt(alpiso);l++){
            //console.log('del piso->'+piso);
			//alert('entra al for 1');
			/*numaptos_g=$('#frm_pisos'+frm+' #pisos'+index_x).val();
			multiplicacion=multiplicacion + parseInt( numaptos_g * amultiplicar );
			alert(multiplicacion);*/
			
            for(n=parseInt(nomenc1);n<=parseInt(nomenc2);n++){
				//alert('frm->'+frm+' index_x->'+index_x);
				//alert('entra al ciclo');
				elvalor="";
				elvalor2="";
				pisos_creados++;
				if(xx>9){ 
					
					//console.log('piso->'+(l)+' nomenclatura->'+elvalor);
					concat+=pisonomenc+xx+';';
					elvalor=xx_piso_current.toString()+ceronomenc+index_mayora_nueve;
					elvalor=pisonomenc+xx.toString();
					//console.log('piso->'+(l)+' nomenclatura->'+elvalor);
					//alert('piso->'+(l)+' nomenclatura->'+elvalor);
					
					
					
					var formData = {
                        'accion': 'savenomenc1',
                        'idtorre': localStorage.getItem('idtorre'+frm),
                        'nomenc': elvalor
                        /*'desde': $('#frm_pisos'+index_x+' #del'+index_x).val(),
                        'hasta': $('#frm_pisos'+index_x+' #alpiso'+index_x).val(),
                        'naptos': $('#frm_pisos'+index_x+' #pisos'+index_x).val(),
                        'nm1': $('#frm_pisos'+index_x+' #nomenc'+index_x+'_1').val(),
                        'nm2': $('#frm_pisos'+index_x+' #nomenc'+index_x+'_2').val()*/
                    };
                    $.post(server+'api/product/create.php', formData, function(data){
                        //reistro exitoso
                        //alert(data);
                        if(data.res==1){
                            nomenc_ok = 1;
							//alert('full');
                            
                        }else alert('Ha ocurido un error'+data)
                    });
                }else{
					elvalor2=pisonomenc+ceronomenc+xx;
					//console.log('piso->'+(l)+' nomenclatura->'+elvalor2);
					concat+=pisonomenc+ceronomenc+xx+';';
					//alert('piso->'+(l)+' nomenclatura->'+elvalor2);
					
					var formData = {
                        'accion': 'savenomenc1',
                        'idtorre': localStorage.getItem('idtorre'+frm),
                        'nomenc': elvalor2
                    };
                    $.post(server+'api/product/create.php', formData, function(data){
                        //reistro exitoso
                        //alert(data);
                        if(data.res==1){
                            nomenc_ok = 1;
							//alert('full');
                            
                        }else alert('Ha ocurido un error'+data)
                    });
				}
				
				//alert('pisonomenc->'+pisonomenc+' xx->'+xx);
                
                xx++;
				index_mayora_nueve++;
            }
            xx = xx_nom;
			//alert('inicializa xx->'+xx);
			index_mayora_nueve = 1;
            pisonomenc++;
            xx_piso_current++;
        }

        xx = nomenc1;
        xx_nom = xx;
        pisonomenc = 1;
        nomenc_tabla = 0;
        index_tabla = 1;
		
		// ------------------- DESHABILITAR BOTÓN DE ACEPTAR ----------------------------------- //
		$('#frm_pisos'+frm+' #btn_pisos'+index_x).attr('disabled', true);
		$('#frm_pisos'+frm+' #del'+index_x).attr('disabled', true);
		$('#frm_pisos'+frm+' #alpiso'+index_x).attr('disabled', true);
		$('#frm_pisos'+frm+' #pisos'+index_x).attr('disabled', true);
		$('#frm_pisos'+frm+' #nomenc'+index_x+'_1').attr('disabled', true);
		$('#frm_pisos'+frm+' #nomenc'+index_x+'_2').attr('disabled', true);
		if( numaptos_total >= parseInt(p_xtorre)  ){
			$('#frm_pisos'+frm+' .btn_pisos').attr('disabled', true);
			$('#frm_pisos'+frm).css('opacity', 0.4);
			pisos_creados=0;
			pisos_creados2=0;
			setTimeout(function(){
				$('#savepisosxapto').attr('disabled', false);
			}, 650);
		}else{
			//mostrar siguiente fila
			$('#alpiso'+elindice).focus();
			console.log('creados->'+pisos_creados2+' pxtorre->'+p_xtorre);
			$('#frm_pisos'+frm+' .container.contpisos').eq(elindice).fadeIn(80);
		}
		
		//document.getElementById('frm_pisos0').reset();
		//$('#frm_pisos'+frm+' #del0').val(nuevo_valor);

        var html_tabla='<table><tr><th>TORRE</th><th>APARTAMENTO</th><th>CLASE DEL APARTAMENTO</th><th>AÑADIR REFORMAS</th></tr>';
		for(k=1;k<=localStorage.getItem('p_torres');k++){
           for(m=$('#nomenc'+index_x+'_1').val();m<=$('#nomenc'+index_x+'_2').val();m++){
                // ******************************* HTML DE TABLA ************************
                    if(xx>9) nomenc_tabla = pisonomenc+xx;
                    else nomenc_tabla = pisonomenc+ceronomenc+xx;

                    //alert('elcero->'+pisonomenc+ceronomenc+xx);

                    if(index_tabla==1){
                        html_tabla+='<tr><td rowspan="10">'+m+'</td><td>'+nomenc_tabla+'</td><td><select name=""><option value="">Seleccionar...</option>';
                        html_tabla+='<option value="">Tipo A</option><option vlue="">Tipo B</option><option vlue="">Tipo C</option>';
                        html_tabla+='</select></td><td><img onclick="$(\'#file_reforma\').click();" src="img/doc.png" width="35"></td></tr>';
                    }else{
                        html_tabla+='<tr><td>'+nomenc_tabla+'</td><td><select name=""><option value="">Seleccionar...</option>';
                        html_tabla+='<option value="">Tipo A</option><option vlue="">Tipo B</option><option vlue="">Tipo C</option>';
                        html_tabla+='</select></td><td><img onclick="$(\'#file_reforma\').click();" src="img/doc.png" width="35"></td></tr>';
                    }
                //alert('torres->'+localStorage.getItem('p_torres'));
                xx++;
                index_tabla++;
           }

           xx = xx_nom;
           index_tabla = 1;
           pisonomenc++;
        }

        html_tabla+="</table>";

        //console.log(html_tabla);

  	}
  }

  function step2(){
	//AGREGAR FOMULARIO PARA CONFIGURAR LOS APTOS POR PISO
    //alert('torres->'+localStorage.getItem('p_torres'));
	$('#back1,#back2').fadeOut(60);
	$('.modal-footer, #current_tipo, #btn_actions').fadeOut(60);
	$('#preview_secs').fadeOut(120);
	for(j=0;j<localStorage.getItem('p_torres');j++){
		$('#aptos_piso').append('<div class="toogle_aptos" id="toogle'+j+'"><div onclick="toogleAptos('+j+');" class="toogle_tit"><h5>Torre '+(j+1)+'</h5></div><div class="toogle_content"></div></div>');
		$('#toogle'+j+' .toogle_content').append('<form name="frm_pisos'+j+'" id="frm_pisos'+j+'" method="post" action=""><input type="hidden" name="torrehidden" id="torrehidden'+j+'" value="'+(j+1)+'" /></form>');
	}
	
	for(j=0;j<localStorage.getItem('p_torres');j++){
		visible="";
		for(x=0;x<10;x++){
			if(x==1) visible="style='display:none'";
			$('#toogle'+j+' .toogle_content form').append('<div class="container contpisos" '+visible+'><div class="row"><div class="col s3 m3">Del piso <input id="del'+x+'" type="number" name="del'+x+'" /></div> <div class="col s3 l3">Al piso <input type="number" id="alpiso'+x+'" name="alpiso'+x+'" /></div> <div class="col s3 l3"># de apartamentos<input class="numaptos" type="number" id="pisos'+x+'" name="pisos'+x+'" /></div><div class="col s3 l3">Nomenclatura<div class="nomenc_container"><input placeholder="Desde" type="text" id="nomenc'+x+'_1" name="nomenc'+x+'_1" class="nmargin" /><input placeholder="Hasta" type="text" id="nomenc'+x+'_2" name="nomenc'+x+'_2" /><div class="clear"></div></div></div><div class="col s12 l12"><input type="button" value="Aceptar" class="btn_pisos btn" onclick="aptos_piso('+j+', '+x+', this);" id="btn_pisos'+x+'" /></div></div></div>');
			//if(x>0) $('#del'+x).attr('disabled',true);
		}
	}
	
	$('#aptos_piso').append('<br><br><a class="waves-effect waves-light btn" href="javascript:void(0);" id="savepisosxapto" onclick="saveNpisos();">GUARDAR</a>');
	$('#aptos_piso').append('<a class="waves-effect waves-light btn" href="javascript:void(0);" id="paso3" onclick="step3();" style="display:none">SIGUIENTE</a>');
	
	$('.steps').fadeOut(60,function(){
		$('#step2').fadeIn(60);
	});
	$('.modal-footer').fadeOut(60);
	//id de proyecto
	idp = localStorage.getItem('idp_actual');
  }

  function toogleAptos(x){
	$('.toogle_aptos .toogle_content').fadeOut(60, function(){
		$('#toogle'+x+' .toogle_content').fadeIn(60);
	});
  }
  
  
  function saveNpisos(){
	//localStorage.setItem('idtorre'+a, res.idtorres[a]);
	//var num_torres = 0;
	
    var error_conf = false;
	var cont_numaptos= 0;
	var totalconfig = 0;
	
	var alpiso_total=0;
	var amultiplicar = 0; //variable que guarda la diferencia de números entre 'delpiso' y 'alpiso'
	
	for(x=0;x<localStorage.getItem('p_torres');x++){
		var total_xtorre=0;
		$('#frm_pisos'+x+' .numaptos').each(function(indice){
			if( !( $('#frm_pisos'+x+' #pisos'+indice).val()=='' && $('#frm_pisos'+x+' #alpiso'+indice).val()=='' ) ){
				amultiplicar=0;
				//alert($('#frm_pisos'+x+' #del'+indice).val()+' '+$('#frm_pisos'+x+' #alpiso'+indice).val());
				for(d=parseInt($('#frm_pisos'+x+' #del'+indice).val());d<=parseInt($('#frm_pisos'+x+' #alpiso'+indice).val());d++){
					amultiplicar++;
					//alert('amultiplicar-1->'+amultiplicar);
				}
				//alert('amultiplicarss->'+amultiplicar);
				total_xtorre += parseInt($('#frm_pisos'+x+' #pisos'+indice).val()) * parseInt(amultiplicar);
				
			}
		});
		
		if( total_xtorre != parseInt($('#pisosxtorre'+x).val())  ){
			alert('Una o más torres no están configuradas correctamente, revisa la configuración para continuar');
			//$('.btn_pisos').attr('disabled', false);
			$('.toogle_aptos').css({
				'border': '1px solid #a0c4ce',
				'background': '#fff'
			});
			$('#toogle'+x).css({
				'border': '1px solid #f7bdc9',
				'background': '#f9ebee'
			});
			contadory = 0;
			console.log('total_xtorre->'+total_xtorre+' pisosxtorre->'+$('#pisosxtorre'+x).val());
			return false;
		}
	}


    if(error_conf==false){
        var nomenc_ok = 0;
		for(x=0;x<localStorage.getItem('p_torres');x++){
            for(c=0;c<$('#frm_pisos'+c+' .numaptos').length;c++){
                //GUARDAR CONF EN LA TABLA 'NOMENCLATURA'
                if(!($('#frm_pisos'+x+' #alpiso'+c).val()=='') && !($('#pisos'+x+' #alpiso'+c).val()=='')){
                    var formData = {
                        'accion': 'savenomenc',
                        'idtorre': localStorage.getItem('idtorre'+x),
                        'idproyecto': localStorage.getItem('idp_actual'),
                        'desde': $('#frm_pisos'+x+' #del'+c).val(),
                        'hasta': $('#frm_pisos'+x+' #alpiso'+c).val(),
                        'naptos': $('#frm_pisos'+x+' #pisos'+c).val(),
                        'nm1': $('#frm_pisos'+x+' #nomenc'+c+'_1').val(),
                        'nm2': $('#frm_pisos'+x+' #nomenc'+c+'_2').val()
                    };
                    $.post(server+'api/product/create.php', formData, function(data){
                        //reistro exitoso
                        //alert(data);
                        if(data.res==1){
                            nomenc_ok = 1;
                            
                        }else alert('Ha ocurido un error'+data)
                    });
                }
            }
        }
		
		if(nomenc_ok==1){
			alert('Configuración guardada con éxito');
			$('#savepisosxapto').fadeOut(60);
			$('#paso3').fadeIn(60);
		}//else alert(nomenc_ok);
		
    }else{
        alert('Una o más torres no están configuradas correctamente, revisa la configuración para continuar');
        $('.btn_pisos').attr('disabled', false);
        contadory = 0;
        for(b=0;b<localStorage.getItem('p_torres');b++){
            document.getElementById('frm_pisos'+b).reset();
            $('#frm_pisos0 #del0').focus();
        }
        //return false;
    } 
	
	$('.mk-spinner-ring.loader').css('display','block');
	step3(); //función que muestra la tabla
	
	
  }
  
  function step3(){
	$('.steps').fadeOut(60,function(){
		$('#step3').fadeIn(60);
	});
	$('.modal-footer').fadeOut(60);
	$('#current_tipo').fadeIn(60).text('proyecto: '+$('#nombrep').val());
	//id de proyecto
	idp = localStorage.getItem('idp_actual');
	
	var formData = {
		'accion': 'dotabla',
		'idp': idp
	};
	$.post(server+'api/product/create.php', formData, function(data){
		//console.log(data);
		//res = JSON.parse(data);
		res = data;
		$('.mk-spinner-ring.loader').css('display','none');
		if(res.res==1){
			//alert(res.tabla);
			$('#step3').prepend(res.tabla);
			$('.file_reforma').change(function(e){
				idap=$(this).attr('data-id');
				var fileName = e.target.files[0].name;
				$('#namer'+idap).text(fileName);
			});
			//Llamada ajax que trae los tipos de apto
			var formData = {
				'accion': 'tipos',
				'idp': idp
			};
			
			$.post(server+'api/product/create.php', formData, function(data){
				//console.log(data);
				//res = JSON.parse(data);
				res = data;
				if(res.res==1){
					idtipos=res.idtipo.split(';');
					ntipos=res.ntipo.split(';');
					html_select='<option value="">...</option>';
					
					for(m=0;m<idtipos.length;m++){
						//alert(ntipos[m]);
						if(!(idtipos[m]=='')){
							html_select+='<option value="'+idtipos[m]+'">'+ntipos[m]+'</option>';
						}
					}
					
					//alert('clase tipoapto->'+$('.tipoapto').length);
					$('select.tipoapto').each(function(){
						$(this).html(html_select);
					});
					/*for(j=0;j<$('.tipoapto').length;j++){
						$('#tipoapto'+j).html(html_select);
					}*/
					
				}else alert('Ha ocurido un error'+res);
			});
			
		}else alert('Ha ocurido un error'+res)
	});
  }
  
  function newSection(){
	$('#savetipo, #step1 h4, #step1 #ntipo, #cancel_tipo, #btnntipo').css('display','none');
	$('#preview_secs').fadeOut(120);
	$('.cuadro img').css('opacity', 1);
	$('#secciones-apto, #frm_seccion').fadeIn(60);
	$('#btn_actions, #frm_seccion2').fadeOut(60);
	$('#crearseccion').css('display', 'inline-block');
	$('#editseccion').css('display', 'none');
	$('.nuevasec').css('display','block');
	if(sec_atras==false) document.getElementById('frm_seccion').reset();
	document.getElementById('frm_seccion2').reset();
	$('#nseccion').focus();
  }
  
  var sec_atras=false;
  function addSection(n){
	if(!(n=='')){
		localStorage.setItem('nseccion_actual', n);
		if($('#idproyecto1').length>0) $('#idproyecto1').val(localStorage.getItem('idp_actual'));
		else $('#idapto').val(localStorage.getItem('apto_actual'));
		
		if($('#d_accion').length>0) $('#idapto').val(localStorage.getItem('apto_actual'));
		$('#idtipo1').val(localStorage.getItem('tipoactual'));
		$('#accion1').val('nseccion');
		updsec=false;
		if(sec_atras) $('#sactual').val(localStorage.getItem('idseccion'));
		
		
		var formData = new FormData($("#frm_seccion")[0]);
		//if($('#d_accion').length>0) formData.tipodesistido=1;
		//alert($('#d_accion').length);
		$('.mk-spinner-ring.loader').css('display','block');
		$('.mk-spinner-ring.loader').css('top','39%');
		$.ajax({
			url:server+'api/product/create.php',
			type:"POST",
			data:formData,
			processData: false,
			contentType: false,
			success:function(res){
				//res = JSON.parse(res);
				$('.mk-spinner-ring.loader').css('display','none');
				if(res.res==1){
					//alert('Sección creada con éxito');
					//Obtener el id del proyecto actual
					if(res.idseccion==0) alert('Sección duplicada para este tipo de apartamento');
					else{
						localStorage.setItem('idseccion', res.idseccion);
						$('#frm_seccion').fadeOut(60);
						$('#frm_seccion2').fadeIn(60);
						//mostrar flecha de "atrás"
						$('#back1').css('display','none');
						$('#back2').css('display','block');
					}
					
				}else alert('Ha ocurido un error'+res);
			}

		});
	}
  }

  var arr_torres_aptos ='0';
  var objTorres = {};
  //GUARDAR PROYECTO
  var acep_nproy=0;
  function saveProyecto(){
  
    $('#iduser').val(localStorage.getItem('iduser'));
	if($('.pisosxtorre').length>0){
        valor=0;
        
        if($('.pisosxtorre').length){
             for(x=0;x<$('.pisosxtorre').length;x++){
                if($('#pisosxtorre'+x).val()==''){
                    alert('Todos los campos son requeridos');
                    return false;
                }
                valor += parseInt($('#pisosxtorre'+x).val());

               // alert('valor--'+valor);
            }

            if(valor!=$('#naptos').val()){
                alert('El número de apartamentos por torre no coincide con el número de apartamentos totales ingresados');
                return false;
            }
        }
       
    }
	
	$('#nxtorre').val($('.pisosxtorre').length);


    var formData = new FormData($("#frm_nproyecto")[0]);
    //var formData = JSON.stringify($("#frm_nproyecto").serializeObject());
    //formData.append('');
	$("#frm_nproyecto").css('opacity', 0.4);
	$('.mk-spinner-ring.loader').css('display','block');
	$('.pisosxtorre').attr('disabled', true);
	$('#nombrep').attr('disabled', true);
	$('#naptos').attr('disabled', true);
	$('#ntorres').attr('disabled', true);
	
	$.ajax({
		url:server+'api/product/create.php',
		type:"POST",
		data:formData,
		processData: false,
		contentType: false,
		success:function(res){
			p_notcomplete=true; //indica si el proyecto no está configurado al 100%
			//res = JSON.stringify(res);
			//alert('res->'+res);
			$('.mk-spinner-ring.loader').css('display','none');
			$("#frm_nproyecto").css('opacity', 1);
			if(res.res==1){
				alert('Proyecto creado con éxito');
				//alert(res.idtorres);
				//alert(typeof res.idtorres);
				//almacenar id de torres creadas
				for(a=0;a<res.idtorres.length;a++){
					localStorage.setItem('idtorre'+a, res.idtorres[a]);
				}
				
				//Obtener el id del proyecto actual
				localStorage.setItem('idp_actual', res.idp);
				localStorage.setItem('p_aptos', $('#naptos').val());
				localStorage.setItem('p_torres', $('#ntorres').val());

                for(j=0;j<$('#ntorres').val();j++){
                    localStorage.setItem('xtorre'+j, $('#pisosxtorre'+j).val());
                }
				
				$('#save_proyecto').css('display','none');
				$('#conf_proyecto').css('display','block');
				
			}else alert('Ha ocurido un error'+res);
		}

	});
  }
  //GUARDAR TIPO DE APTO
  var tipo_atras=false; //me indica si el usuario se devuelve cuando ya ha creado un tipo
  var conta_prev=0;
  var cont_tipos=0; //CUENTA CUANTOS TIPOS VOY CREANDO
  var arr_tipos=0; //ALMACENA LOS TIPOS Q VOY CREANDO
  function saveType(){
	//alert('entraa');
	if(!($('#ntipo').val()=='')){
		arr_tipos[cont_tipos]=$('#ntipo').val();
		cont_tipos++;
		localStorage.setItem('ntipo_actual', $('#ntipo').val());
		$('.mk-spinner-ring.loader').css('display','block');
		$('.mk-spinner-ring.loader').css('top','23%');
		var datatipo = {
			'accion': 'ntipo',
			'ntipo': $('#ntipo').val(),
			'proyecto': localStorage.getItem('idp_actual')
		};
		if(tipo_atras) datatipo.tactual=localStorage.getItem('tipoactual');
		
		console.log(datatipo);
		
		if( $('#select_tipo').css('display')=='block' ){ 
			datatipo.desistir='1'; //cuando se selecciona de una lista
			datatipo.idtipo=$('#selecttipo').val();
		}
		if( $('#select_tipo').length>0 && idp_d>0 ){
			datatipo.idapto=idp_d;
		}
		
		if($('#d_accion').length>0) datatipo.tipodesistido=1;
		
		$.post(server+'api/product/create.php', datatipo, function(res){
			//reistro exitoso
			//res = JSON.parse(res);
			$('.mk-spinner-ring.loader').css('display','none');
			if($('#d_accion').length>0) $('#preview_secs').remove();
			contprev=0;
			if(res.res==1){
				//alert('Tipo de apartamento creado con éxito');
				if($('#d_accion').length==0){
					if( $('#select_tipo').css('display')=='none' || $('#select_tipo').length==0 ){
						$('#current_tipo').fadeIn(60);
						editipo=' <i onclick="editTipo('+res.idtipo+')" class="edit material-icons">edit</i>';
						$('#current_tipo').html($('#ntipo').val()+editipo);
						//Obtener el id del TIPO actual
						localStorage.setItem('tipoactual', res.idtipo);
						//mostrar flecha de "atrás"
						$('#back1').css('display','block');
						$('#back2').css('display','none');
						
						if($('#select_tipo').length>0) $('#btnstep2').css('display', 'none'); //OCULTAR BTN DE SIGUIENTE
						$('#savetipo, #step1 h4, #step1 #ntipo, #cancel_tipo, #btnntipo').css('display','none');
						newSection();
					}else location.reload();
				}else{
					localStorage.setItem('tipoactual', res.idtipo);
					$('#btnseccion').css('display','inline-block');
					$('#savetipo, #step1 h4, #step1 #ntipo, #cancel_tipo, #btnntipo').css('display','none');
					if($('#selecttipo').length==0) newSection();
					else $('#modald').modal('close');
					//$('.modal').modal('close');
					//location.reload();
				}
				
			}else alert('Ha ocurido un error'+res)
		});
	}
  }
  
  function editTipo(id){
	newType(id);
  }
  
  function editType(){
	var formData = {
		'accion': 'updtype',
		'id': localStorage.getItem('tipoactual'),
		'tipo': $('#ntipo').val()
	};
	$.post(server+'api/product/create.php', formData, function(data){
		if(data.res==1){
			editipo=' <i onclick="editTipo('+localStorage.getItem('tipoactual')+')" class="edit material-icons">edit</i>';
			$('#current_tipo').html($('#ntipo').val()+editipo);
			$('#savetipo, #editipo').css('display','none');
			$('#step1 h4, #step1 #ntipo').css('display','none');
			$('#btnseccion').css('display','inline-block');
			$('#btn_actions').css('display','block');
			
		}else alert('Ha ocurido un error'+data)
	});
  }
  
   //CREAR NUEVO TIPO DE APTO
   function newType(id){
		//if($('#savetipo').css('display')=='none'){
			$('#preview_secs').fadeOut(120);
			$('#btn_actions').fadeOut(40);
			$('#savetipo, #cancel_tipo').css('display','block');
			$('#btnseccion, .nuevasec, #secciones-apto, #d_accion').css('display','none');
			if(!id) $('#ntipo').val('');
			$('#step1 h4, #step1 #ntipo, #tipos-apto').css('display','block');
			$('#ntipo').focus();
			$('#caracs_checks').html('');
			
			if(id && id != 'back'){
				$('#editipo').css('display', 'block');
				$('#savetipo').css('display', 'none');
			}
		//}
   }
  
  //GUARDAR CARACTERÍSTICAS
  function saveCaracs(){
	$('.mk-spinner-ring.loader').css('display','block');
	$('#idproyecto2').val(localStorage.getItem('idp_actual'));
	$('#idtipo2').val(localStorage.getItem('tipoactual'));
	$('#idseccion').val(localStorage.getItem('idseccion'));
	$('#aptoactual').val(localStorage.getItem('apto_actual'));
	$('#accion2').val('ncarac');
	//saber el número de características
	var ncaracs = $('.caracsdn').length; //CUENTA CUANTAS CARACS HAY
	$('#ncaracs').val(ncaracs);
	//crear preview de la sección
	if(updsec==false){
		prevsec='<div class="col m3 s6">';
		prevsec+='<h3>'+localStorage.getItem('ntipo_actual')+'</h3>';
		prevsec+='<div class="prev-cont" data-id="'+localStorage.getItem('idseccion')+'" id="prev-cont'+conta_prev+'">';
		prevsec+='<h3 style="margin-bottom:0">';
		prevsec+='<a class="tipolist prevs" id="psec'+conta_prev+'"';
		prevsec+=' href="javascript:void(0);">'+$('#nseccion').val();
		prevsec+='<i onclick="deleteSec('+localStorage.getItem('idseccion'); 
		prevsec+=','+conta_prev+')" class="delete material-icons">delete</i>';
		prevsec+='<i onclick="editSec('+localStorage.getItem('idseccion')+', '+conta_prev+', \''+localStorage.getItem('nseccion_actual')+'\')"'; 
		prevsec+=' class="edit material-icons prev">edit</i></a></h3>';
		prevsec+='<ul class="collapsible"></ul>';
		prevsec+='</div></div>';
		$('#preview_secs .row').append(prevsec);
		conta_prev++;
	}else{
		$('.prev-cont').each(function(x){
			if($(this).attr('data-id')==updsec_id){
				padre=$(this);
				padre.find('.cprev').each(function(i){
					$(this).text($('.caracsdn').eq(i).val());
				});
			}
		});
	}
	
	var formData = new FormData($("#frm_seccion2")[0]);

	$.ajax({
		url:server+'api/product/create.php',
		type:"POST",
		data:formData,
		processData: false,
		contentType: false,
		success:function(res){
			//res = JSON.parse(res);
			$('.mk-spinner-ring.loader').css('display','none');
			if(res.res==1){
				//alert('Características guardadas con éxito');
				//localStorage.setItem('idcaracs', res.idcaracs);
				$('#secciones-apto').fadeOut(60);
				$('#btn_actions,#btnseccion').fadeIn(40);
				
				//si se está creando el tipo para un apto desistido
				if($('#d_accion').length>0){
					$('#preview_secs').css('display','none');
					$('#btnntipo,#btnstep2').remove();
				}
				$('#preview_secs').fadeIn(120);
				if(updsec==false){
					for(x=0;x<ncaracs;x++){
						//alert('indice->'+(conta_prev-1));
						//SI LA CARAC NO ESTÁ VACÍA
						if(!($('.caracsdn').eq(x).val()=='')) $('#prev-cont'+(conta_prev-1)+' .collapsible').append('<li>'+$('.caracsdn').eq(x).val()+'</li>');
					}
				}
				if($('.modalcustom').length>0){ 
					localStorage.setItem('saved_custom', '1');
					localStorage.setItem('noexit', '0');
					document.location.reload();
				}
				alert('Características guardadas con éxito');
				
			}else alert('Ha ocurido un error'+res);
		}

	});
  }

    $(function () {
       /* var fileupload = $("#FileUpload1");
        var filePath = $("#spnFilePath");
        var image = $("#imgUpload");
        image.click(function () {
            fileupload.click();
        });
        fileupload.change(function () {
            var fileName = $(this).val().split('\\')[$(this).val().split('\\').length - 1];
            filePath.html("<b>Selected File: </b>" + fileName);
        });*/
		
		var image = $("#imgUpload");
		image.click(function () {
			$("#FileUpload1").click();
		});
		
		var image = $("#imgUpload2");
		image.click(function () {
			$("#FileUpload2").click();
		});
		
		var image = $("#imgUpload3");
		image.click(function () {
			$("#FileUpload3").click();
		});
		
    });
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgUpload')
                    .attr('src', e.target.result)
                    .width(128);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	
	function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgUpload2')
                    .attr('src', e.target.result)
                    .width(120);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	
	function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgUpload3')
                    .attr('src', e.target.result)
                    .width(54);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	
	function secsDefault(){
		$('#sblank').fadeIn();
		$('#sdefault').fadeOut();
		axios.get(server+'api/product/read.php?accion=getsecsdefault')
		.then(function (response) {
			//console.log(response.data.html);
			$('.mk-spinner-ring.loader').css('display','none');
			if(response.data.res==1){
				$('#sdefault_cont').fadeIn(60);
				$('#nseccion').fadeOut(40);
				ids=response.data.ids.split(';');
				secs=response.data.secs.split(';');
				for(x=0;x<ids.length;x++){
					if(!(ids[x]=='')) $('#espaciod').append('<option value="'+ids[x]+'">'+secs[x]+'</option>');
				}
				$('#espaciod').change(function(){
					if(!( $('#espaciod option:selected').text()=='' )) $('#nseccion').val($('#espaciod option:selected').text());
				});
			}else alert('No se obtuvieron espacios predefinidos');
			//if(localStorage.getItem('nivelu')==1) $('.delproyecto').css('display', 'block');
		})
		.catch(function (error) {
			console.log(error);
		});
	}
/*********************************** PROYECTOS ******************************/
idp_d=0;
function desistir(id){
	idp_d=id;
	if(confirm('¿Realmente desea desistir este apartamento?')){
		var formData = {
        'accion': 'desistir',
        'apto': id
		}
		//poner el apto en estado desistido=1
		$.post(server+'api/product/create.php', formData, function(data){
			if(data.res==1){
				$('#modald').modal('open');
				$('#d_accion').css('display', 'block');
				$('#d_accion #btnstep2').remove();
				$('#step1 h4, #tipos-apto').css('display', 'none');
			}
		});
	}
}

function selectType(){
	var formData = {
		'accion': 'tipos',
		'idp': localStorage.getItem('idp_actual')
	};
	//Consultar los tipos de apartamento
	$.post(server+'api/product/create.php', formData, function(data){
		//console.log(data);
		//res = JSON.parse(data);
		res = data;
		if(res.res==1){
			idtipos=res.idtipo.split(';');
			ntipos=res.ntipo.split(';');
			html_select='<option value="">Seleccionar tipo...</option>';
			
			for(m=0;m<idtipos.length;m++){
				//alert(ntipos[m]);
				if(!(idtipos[m]=='')){
					html_select+='<option value="'+idtipos[m]+'">'+ntipos[m]+'</option>';
				}
			}
			
			//alert('clase tipoapto->'+$('.tipoapto').length);
			$('#select_tipo, #cancel_tipo').css('display', 'block');
			$('#select_tipo').html('<select name="" id="selecttipo">'+html_select+"</select>");
			
			$('#selecttipo').change(function(){
				$('#ntipo').val($('#selecttipo option:selected').text());
			});
			/*for(j=0;j<$('.tipoapto').length;j++){
				$('#tipoapto'+j).html(html_select);
			}*/
			
			$('#tipos-apto').css('display', 'block');
			$('#ntipo, #d_accion a').css('display', 'none');
			
		}
	});
}

function openModal(idapto){
	$('#modald').modal('open');
	$('#step1 h4, #tipos-apto, #select_tipo').css('display', 'none');
	$('#d_accion').css('display', 'block');
	$('#d_accion a').css('display', 'inline-block');
	idp_d=idapto;
}

//***************** FIRMA *************


//***************** FIRMA *************