-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql.industrialfumigaciones.com
-- Tiempo de generación: 06-12-2019 a las 09:51:41
-- Versión del servidor: 5.7.25-log
-- Versión de PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `capital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptos_torre`
--

CREATE TABLE `aptos_torre` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `torre` varchar(100) NOT NULL,
  `naptos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptos_torre`
--

INSERT INTO `aptos_torre` (`id`, `id_proyecto`, `torre`, `naptos`) VALUES
(1, 1, 'Torre1', 25),
(2, 1, 'Torre2', 25),
(3, 2, 'Torre1', 10),
(4, 2, 'Torre2', 10),
(5, 3, 'Torre1', 10),
(6, 3, 'Torre2', 10),
(7, 3, 'Torre3', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campos_entrega`
--

CREATE TABLE `campos_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `apto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `propietario` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `cedula` int(100) NOT NULL,
  `torre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `parqueadero` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `c_util` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `proyecto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `municipio` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `energia` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `acueducto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `gas` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `municipio2` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `ingeniero` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `firma1` text COLLATE latin1_spanish_ci NOT NULL,
  `firma2` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `archivo` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `campos_entrega`
--

INSERT INTO `campos_entrega` (`id`, `idapto`, `apto`, `propietario`, `cedula`, `torre`, `parqueadero`, `c_util`, `proyecto`, `direccion`, `municipio`, `energia`, `acueducto`, `gas`, `municipio2`, `ingeniero`, `firma1`, `firma2`, `fecha`, `archivo`) VALUES
(45, 3, '102', 'PEPE', 6868, 'Torre1', '67', '67', 'PROY1', '567', 'MEDELLIN', '67', '67', '67', 'MEDELLIN', 'LEYLA', 'uzNiSI6nyz.png', 'kNb3VKxuMS.png', '27 de Noviembre del 2019', 'ENTREGA_INMUEBLE_1522134836.pdf'),
(48, 11, '102', 'alejo', 1234, 'Torre1', '1', '1', 'orquideas', 'medellin', 'antioquia', '12', '13', '14', 'antioquia', 'Inj.Alejo', 'k2g6SI2bvc.png', 'L8AlSBi2j6.png', 'clientes de Noviembre del 2019', 'ENTREGA_INMUEBLE_164647241.pdf'),
(50, 1, '101', 'PEDRO PEREZ', 79879798, 'Torre1', '678', '68', 'PROY1', 'CR 55', 'MEDELLIN', '67', '676', '76', 'MEDELLIN', 'LEYLA TORRES', '7IOYxjdW94.png', 'eCLEFob4Eh.png', '3 de Diciembre del 2019', 'ENTREGA_INMUEBLE_199923268.pdf'),
(52, 70, '504', 'carlos a', 1234566, 'Torre2', '1', '1', 'a1', 'calle2', 'Medellín', '2', '56', '1', 'mede', '', 'Lf93RExRP4.png', 'o9vgN8QeHG.png', '2 de Diciembre del 2019', 'ENTREGA_INMUEBLE_554422923.pdf'),
(53, 71, '101', 'qwe', 123, 'Torre1', '1', '1', 'b1', '1', '1', '1', '1', '1', '1', 'sdasdasdasdas', 'vJCQeiWf9o.png', 'T1ZmVAKN1v.png', '23 de Diciembre del 2019', 'ENTREGA_INMUEBLE_712579416.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracs_custom`
--

CREATE TABLE `caracs_custom` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `caracs_custom`
--

INSERT INTO `caracs_custom` (`id`, `idapto`, `seccion`, `caracs`, `fecha`) VALUES
(1, 24, 1, 'Muebles', '2019-12-05 15:48:06'),
(2, 24, 1, 'Isla', '2019-12-05 15:48:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracs_default`
--

CREATE TABLE `caracs_default` (
  `id` int(10) UNSIGNED NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura`
--

CREATE TABLE `nomenclatura` (
  `id` int(10) UNSIGNED NOT NULL,
  `idtorre` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `desde_piso` int(11) NOT NULL,
  `hasta_piso` int(11) NOT NULL,
  `naptos` int(11) NOT NULL,
  `nomenc1` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nomenc2` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nomenclatura` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nomenclatura`
--

INSERT INTO `nomenclatura` (`id`, `idtorre`, `idproyecto`, `desde_piso`, `hasta_piso`, `naptos`, `nomenc1`, `nomenc2`, `nomenclatura`) VALUES
(1, 1, 1, 1, 10, 2, '1', '2', ''),
(2, 2, 1, 99, 99, 5, '6', '10', ''),
(3, 2, 1, 1, 10, 2, '3', '4', ''),
(4, 1, 1, 99, 99, 5, '1', '5', ''),
(5, 4, 2, 1, 5, 2, '3', '4', ''),
(6, 3, 2, 1, 5, 2, '1', '2', ''),
(7, 5, 3, 1, 5, 2, '1', '2', ''),
(8, 7, 3, 1, 5, 2, '5', '6', ''),
(9, 6, 3, 6, 10, 1, '2', '2', ''),
(10, 6, 3, 1, 5, 1, '1', '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura_history`
--

CREATE TABLE `nomenclatura_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `clase` int(11) NOT NULL,
  `nomenclatura` int(11) NOT NULL,
  `reforma` varchar(100) NOT NULL,
  `obs_generales` text NOT NULL,
  `imgs_generales` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `entrega` int(11) NOT NULL,
  `entregado` int(11) NOT NULL,
  `desistido` int(11) NOT NULL,
  `informe` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nomenclatura_history`
--

INSERT INTO `nomenclatura_history` (`id`, `idapto`, `clase`, `nomenclatura`, `reforma`, `obs_generales`, `imgs_generales`, `aprobado`, `entrega`, `entregado`, `desistido`, `informe`, `fecha`) VALUES
(1, 6, 1, 0, '', '', '', 0, 0, 0, 0, 'historico_935780633.pdf', '2019-12-05 15:46:26'),
(2, 23, 1, 0, '', '', '', 0, 0, 0, 0, 'historico_702489904.pdf', '2019-12-05 15:46:45'),
(3, 24, 1, 0, '', '', '', 0, 0, 0, 0, 'historico_878112339.pdf', '2019-12-05 15:48:20'),
(4, 31, 3, 0, '', '', '', 0, 0, 0, 0, 'historico_1920579484.pdf', '2019-12-05 15:55:30'),
(5, 60, 4, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:40:08'),
(6, 70, 4, 0, '', '', '', 1, 1, 0, 0, '', '2019-12-06 06:41:50'),
(7, 71, 7, 0, '', '', '', 1, 1, 0, 0, '', '2019-12-06 06:52:17'),
(8, 72, 7, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:52:48'),
(9, 73, 7, 0, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:53:23'),
(10, 73, 7, 0, '', '', '', 1, 1, 0, 0, '', '2019-12-06 06:54:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura_oficial`
--

CREATE TABLE `nomenclatura_oficial` (
  `id` int(10) UNSIGNED NOT NULL,
  `idtorre` int(11) NOT NULL,
  `nomenclatura` int(11) NOT NULL,
  `clase` int(11) NOT NULL,
  `reforma` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `obs_generales` text COLLATE latin1_spanish_ci NOT NULL,
  `imgs_generales` text COLLATE latin1_spanish_ci NOT NULL,
  `aprobado` int(11) NOT NULL,
  `entrega` int(11) NOT NULL,
  `entregado` int(11) NOT NULL COMMENT '0 cuando el propietario no lo ha aprobado, 1 cuando todo está 100% ok',
  `desistido` int(11) NOT NULL,
  `informe` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nomenclatura_oficial`
--

INSERT INTO `nomenclatura_oficial` (`id`, `idtorre`, `nomenclatura`, `clase`, `reforma`, `obs_generales`, `imgs_generales`, `aprobado`, `entrega`, `entregado`, `desistido`, `informe`, `fecha`) VALUES
(1, 1, 9901, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:09'),
(2, 1, 9903, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:09'),
(3, 1, 9904, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:09'),
(4, 1, 9905, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:09'),
(5, 1, 9902, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:10'),
(6, 1, 101, 1, '', '', '', 0, 0, 0, 0, 'historico_935780633.pdf', '2019-12-05 15:46:26'),
(7, 1, 202, 3, '', '', '', 0, 0, 0, 2, '', '2019-12-05 15:45:16'),
(8, 1, 302, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(9, 1, 401, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(10, 1, 402, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(11, 1, 501, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(12, 1, 502, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(13, 1, 601, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(14, 1, 602, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(15, 1, 701, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(16, 1, 702, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(17, 1, 801, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(18, 1, 802, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(19, 1, 901, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(20, 1, 902, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(21, 1, 1001, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(22, 1, 1002, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:16'),
(23, 1, 102, 1, '', '', '', 0, 0, 0, 0, 'historico_1460719881.pdf', '2019-12-05 15:46:45'),
(24, 1, 201, 2, '', '', '', 0, 0, 0, 2, 'historico_811873682.pdf', '2019-12-05 15:48:20'),
(25, 1, 301, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:17'),
(26, 2, 9906, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:23'),
(27, 2, 9907, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:23'),
(28, 2, 9908, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:23'),
(29, 2, 9909, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:23'),
(30, 2, 9910, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:23'),
(31, 2, 103, 3, '', '', '', 0, 0, 0, 2, 'historico_1920579484.pdf', '2019-12-05 15:55:30'),
(32, 2, 104, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(33, 2, 203, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(34, 2, 204, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(35, 2, 303, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(36, 2, 304, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(37, 2, 403, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(38, 2, 404, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(39, 2, 503, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(40, 2, 504, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(41, 2, 603, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(42, 2, 604, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(43, 2, 703, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(44, 2, 704, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(45, 2, 803, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(46, 2, 804, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(47, 2, 903, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(48, 2, 904, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:31'),
(49, 2, 1003, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:32'),
(50, 2, 1004, 1, '', '', '', 0, 0, 0, 0, '', '2019-12-05 15:45:32'),
(51, 3, 102, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(52, 3, 201, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(53, 3, 202, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(54, 3, 301, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(55, 3, 302, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(56, 3, 401, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(57, 3, 402, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(58, 3, 501, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(59, 3, 502, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:34'),
(60, 3, 101, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:40:08'),
(61, 4, 103, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(62, 4, 104, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(63, 4, 203, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(64, 4, 204, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(65, 4, 303, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(66, 4, 304, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(67, 4, 403, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(68, 4, 404, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:57'),
(69, 4, 503, 4, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:35:58'),
(70, 4, 504, 4, '', 'ok', '', 1, 1, 1, 0, 'historico_702489904.pdf', '2019-12-06 06:43:05'),
(71, 5, 101, 7, '', '', '', 1, 1, 1, 0, 'historico_878112339.pdf', '2019-12-06 06:56:00'),
(72, 5, 102, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:52:48'),
(73, 5, 201, 7, '', '', '', 1, 1, 0, 0, '', '2019-12-06 06:58:37'),
(74, 5, 202, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:45'),
(75, 5, 301, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:45'),
(76, 5, 302, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:45'),
(77, 5, 401, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:45'),
(78, 5, 402, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:45'),
(79, 5, 501, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:46'),
(80, 5, 502, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:46'),
(81, 6, 101, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:57'),
(82, 6, 201, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:57'),
(83, 6, 301, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:57'),
(84, 6, 401, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:57'),
(85, 6, 501, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:49:57'),
(86, 6, 602, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:01'),
(87, 6, 702, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:01'),
(88, 6, 802, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:01'),
(89, 6, 902, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:01'),
(90, 6, 1002, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:01'),
(91, 7, 106, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(92, 7, 105, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(93, 7, 305, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(94, 7, 306, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(95, 7, 405, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(96, 7, 406, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(97, 7, 505, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(98, 7, 506, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:16'),
(99, 7, 206, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:17'),
(100, 7, 205, 7, '', '', '', 0, 0, 0, 0, '', '2019-12-06 06:51:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_entrega`
--

CREATE TABLE `obs_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `caracs` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `obs` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `obs_entrega`
--

INSERT INTO `obs_entrega` (`id`, `idapto`, `caracs`, `custom`, `obs`, `aprobado`, `fecha`) VALUES
(1, 70, 7, 0, '', 1, '2019-12-06 06:43:05'),
(2, 70, 8, 0, '', 1, '2019-12-06 06:43:05'),
(3, 70, 9, 0, '', 1, '2019-12-06 06:43:06'),
(4, 70, 10, 0, '', 1, '2019-12-06 06:43:05'),
(5, 71, 19, 0, '', 1, '2019-12-06 06:56:00'),
(6, 71, 20, 0, '', 1, '2019-12-06 06:56:00'),
(7, 71, 21, 0, '', 1, '2019-12-06 06:56:00'),
(8, 71, 22, 0, '', 1, '2019-12-06 06:56:00'),
(9, 71, 19, 0, '', 1, '2019-12-06 06:56:00'),
(10, 71, 20, 0, '', 1, '2019-12-06 06:56:00'),
(11, 71, 21, 0, '', 1, '2019-12-06 06:56:00'),
(12, 71, 22, 0, '', 1, '2019-12-06 06:56:00'),
(13, 73, 19, 0, 'malop', 0, '2019-12-06 06:58:36'),
(14, 73, 20, 0, '', 1, '2019-12-06 06:58:36'),
(15, 73, 21, 0, '', 1, '2019-12-06 06:58:36'),
(16, 73, 22, 0, '', 1, '2019-12-06 06:58:37'),
(17, 73, 19, 0, 'malop', 0, '2019-12-06 06:58:36'),
(18, 73, 20, 0, '', 1, '2019-12-06 06:58:36'),
(19, 73, 21, 0, '', 1, '2019-12-06 06:58:36'),
(20, 73, 22, 0, '', 1, '2019-12-06 06:58:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_generales`
--

CREATE TABLE `obs_generales` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_generales_entrega`
--

CREATE TABLE `obs_generales_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text COLLATE latin1_spanish_ci NOT NULL,
  `imgs` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `obs_generales_entrega`
--

INSERT INTO `obs_generales_entrega` (`id`, `idapto`, `obs`, `imgs`, `fecha`) VALUES
(1, 70, 'ok', '', '2019-12-06 06:43:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `n_aptos` int(11) NOT NULL,
  `n_torres` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `nombre`, `n_aptos`, `n_torres`, `imagen`, `aprobado`, `fecha_creacion`) VALUES
(1, 'Prueba 6 (5/12/19)', 50, 2, 'iproyecto.png', 0, '2019-12-05 15:44:32'),
(2, 'a1', 20, 2, '4j4EJmhowd.jpg', 0, '2019-12-06 06:33:17'),
(3, 'b1', 30, 3, 'iproyecto.png', 0, '2019-12-06 06:47:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto_complete`
--

CREATE TABLE `proyecto_complete` (
  `id` int(10) UNSIGNED NOT NULL,
  `iduser` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fechap` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas`
--

CREATE TABLE `reformas` (
  `id` int(10) UNSIGNED NOT NULL,
  `indice` int(11) NOT NULL COMMENT 'indice de la reforma',
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas_entrega`
--

CREATE TABLE `reformas_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `indice` int(11) NOT NULL COMMENT 'indice de la reforma',
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas_h`
--

CREATE TABLE `reformas_h` (
  `id` int(10) UNSIGNED NOT NULL,
  `indice` int(11) NOT NULL,
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revision_apto`
--

CREATE TABLE `revision_apto` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `carac` int(11) NOT NULL,
  `custom` int(11) NOT NULL COMMENT 'me dice si es carac custom',
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `revision_apto`
--

INSERT INTO `revision_apto` (`id`, `idapto`, `obs`, `imgs`, `carac`, `custom`, `aprobado`, `fecha`) VALUES
(1, 0, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(2, 0, '', '', 2, 0, 0, '0000-00-00 00:00:00'),
(3, 6, 'feo', '', 1, 0, 0, '2019-12-05 15:46:26'),
(4, 6, '', '', 2, 0, 1, '2019-12-05 15:46:26'),
(5, 23, '', '', 1, 0, 1, '2019-12-05 15:46:45'),
(6, 23, 'horrible', '', 2, 0, 0, '2019-12-05 15:46:45'),
(7, 24, '', '', 1, 0, 1, '2019-12-05 15:48:20'),
(8, 24, '', '', 2, 0, 1, '2019-12-05 15:48:20'),
(9, 24, 'feo', '', 1, 1, 0, '2019-12-05 15:48:20'),
(10, 24, '', '', 2, 1, 1, '2019-12-05 15:48:20'),
(11, 0, '', '', 3, 0, 0, '0000-00-00 00:00:00'),
(12, 0, '', '', 4, 0, 0, '0000-00-00 00:00:00'),
(13, 24, '', '', 3, 0, 0, '0000-00-00 00:00:00'),
(14, 24, '', '', 4, 0, 0, '0000-00-00 00:00:00'),
(15, 7, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(16, 7, '', '', 2, 0, 0, '0000-00-00 00:00:00'),
(17, 0, '', '', 5, 0, 0, '0000-00-00 00:00:00'),
(18, 0, '', '', 6, 0, 0, '0000-00-00 00:00:00'),
(19, 31, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(20, 31, '', '', 2, 0, 0, '0000-00-00 00:00:00'),
(21, 32, '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(22, 32, '', '', 2, 0, 0, '0000-00-00 00:00:00'),
(23, 31, 'abc', '', 5, 0, 0, '2019-12-05 15:55:30'),
(24, 31, '', '', 6, 0, 1, '2019-12-05 15:55:30'),
(25, 0, '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(26, 0, '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(27, 0, '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(28, 0, '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(29, 0, '', '', 11, 0, 0, '0000-00-00 00:00:00'),
(30, 0, '', '', 12, 0, 0, '0000-00-00 00:00:00'),
(31, 0, '', '', 13, 0, 0, '0000-00-00 00:00:00'),
(32, 0, '', '', 14, 0, 0, '0000-00-00 00:00:00'),
(33, 60, '', '', 7, 0, 1, '2019-12-06 06:40:08'),
(34, 60, '', '', 8, 0, 1, '2019-12-06 06:40:08'),
(35, 60, '', '', 9, 0, 1, '2019-12-06 06:40:08'),
(36, 60, 'malo', '', 10, 0, 0, '2019-12-06 06:40:08'),
(37, 61, '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(38, 61, '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(39, 61, '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(40, 61, '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(41, 62, '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(42, 62, '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(43, 62, '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(44, 62, '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(45, 63, '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(46, 63, '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(47, 63, '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(48, 63, '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(49, 51, '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(50, 51, '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(51, 51, '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(52, 51, '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(53, 70, '', '', 7, 0, 1, '2019-12-06 06:41:50'),
(54, 70, '', '', 8, 0, 1, '2019-12-06 06:41:50'),
(55, 70, '', '', 9, 0, 1, '2019-12-06 06:41:50'),
(56, 70, '', '', 10, 0, 1, '2019-12-06 06:41:50'),
(57, 0, '', '', 15, 0, 0, '0000-00-00 00:00:00'),
(58, 0, '', '', 16, 0, 0, '0000-00-00 00:00:00'),
(59, 0, '', '', 17, 0, 0, '0000-00-00 00:00:00'),
(60, 0, '', '', 18, 0, 0, '0000-00-00 00:00:00'),
(61, 0, '', '', 19, 0, 0, '0000-00-00 00:00:00'),
(62, 0, '', '', 20, 0, 0, '0000-00-00 00:00:00'),
(63, 0, '', '', 21, 0, 0, '0000-00-00 00:00:00'),
(64, 0, '', '', 22, 0, 0, '0000-00-00 00:00:00'),
(65, 71, '', '', 19, 0, 1, '2019-12-06 06:52:16'),
(66, 71, '', '', 20, 0, 1, '2019-12-06 06:52:17'),
(67, 71, '', '', 21, 0, 1, '2019-12-06 06:52:17'),
(68, 71, '', '', 22, 0, 1, '2019-12-06 06:52:17'),
(69, 72, 'Mueble bajo malo', '', 19, 0, 0, '2019-12-06 06:52:48'),
(70, 72, '', '', 20, 0, 1, '2019-12-06 06:52:48'),
(71, 72, '', '', 21, 0, 1, '2019-12-06 06:52:48'),
(72, 72, '', '', 22, 0, 1, '2019-12-06 06:52:48'),
(73, 73, '', '', 19, 0, 1, '2019-12-06 06:54:38'),
(74, 73, '', '', 20, 0, 1, '2019-12-06 06:54:38'),
(75, 73, 'mala pintura', '', 21, 0, 1, '2019-12-06 06:54:38'),
(76, 73, 'cielo sucio', '', 22, 0, 1, '2019-12-06 06:54:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE `secciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `proyecto` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `seccion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`id`, `imagen`, `proyecto`, `tipo`, `seccion`, `fecha_creacion`) VALUES
(1, 'king_bed', 1, 1, 'H.Principal', '2019-12-05 15:44:47'),
(2, 'restaurant', 1, 2, 'Cocina', '2019-12-05 15:49:31'),
(3, 'deck', 1, 3, 'patio', '2019-12-05 15:53:51'),
(4, 'kitchen', 2, 4, 'Cocina', '2019-12-06 06:33:34'),
(5, 'kitchen', 2, 5, 'Cocina', '2019-12-06 06:33:53'),
(6, 'kitchen', 3, 6, 'Cocina', '2019-12-06 06:47:38'),
(7, 'king_bed', 3, 6, 'alcoba', '2019-12-06 06:47:57'),
(8, 'kitchen', 3, 7, 'Cocina', '2019-12-06 06:48:57'),
(9, 'restaurant', 3, 7, 'comedor', '2019-12-06 06:49:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_apto`
--

CREATE TABLE `secciones_apto` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `seccion` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_caracs`
--

CREATE TABLE `secciones_caracs` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `secciones_caracs`
--

INSERT INTO `secciones_caracs` (`id`, `imagen`, `seccion`, `caracs`, `fecha_creacion`) VALUES
(1, '', 1, 'Mesas de madera', '2019-12-05 15:44:59'),
(2, '', 1, 'Paredes blancas', '2019-12-05 15:44:59'),
(3, '', 2, 'Piso de Piedra', '2019-12-05 15:49:38'),
(4, '', 2, 'b', '2019-12-05 15:49:39'),
(5, '', 3, 'c', '2019-12-05 15:53:56'),
(6, '', 3, 'd', '2019-12-05 15:53:56'),
(7, '', 4, '1', '2019-12-06 06:33:40'),
(8, '', 4, '2', '2019-12-06 06:33:40'),
(9, '', 4, '3', '2019-12-06 06:33:40'),
(10, '', 4, '4', '2019-12-06 06:33:40'),
(11, '', 5, '1', '2019-12-06 06:33:59'),
(12, '', 5, '2', '2019-12-06 06:33:59'),
(13, '', 5, '3', '2019-12-06 06:33:59'),
(14, '', 5, '4', '2019-12-06 06:33:59'),
(15, '', 6, '1', '2019-12-06 06:47:42'),
(16, '', 6, '2', '2019-12-06 06:47:42'),
(17, '', 7, '1', '2019-12-06 06:48:02'),
(18, '', 7, '2', '2019-12-06 06:48:02'),
(19, '', 8, '1', '2019-12-06 06:49:01'),
(20, '', 8, '2', '2019-12-06 06:49:01'),
(21, '', 9, '1', '2019-12-06 06:49:13'),
(22, '', 9, '2', '2019-12-06 06:49:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_caracs_history`
--

CREATE TABLE `secciones_caracs_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `carac` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_caracs_history`
--

INSERT INTO `secciones_caracs_history` (`id`, `idapto`, `carac`, `custom`, `obs`, `imgs`, `aprobado`, `fecha`) VALUES
(1, 6, 2, 0, '', '', 1, '2019-12-05 15:46:26'),
(2, 6, 1, 0, 'feo', '', 0, '2019-12-05 15:46:26'),
(3, 23, 1, 0, '', '', 1, '2019-12-05 15:46:45'),
(4, 23, 2, 0, 'horrible', '', 0, '2019-12-05 15:46:45'),
(5, 24, 2, 1, '', '', 1, '2019-12-05 15:48:20'),
(6, 24, 1, 1, 'feo', '', 0, '2019-12-05 15:48:20'),
(7, 24, 1, 0, '', '', 1, '2019-12-05 15:48:20'),
(8, 24, 2, 0, '', '', 1, '2019-12-05 15:48:20'),
(9, 31, 5, 0, 'abc', '', 0, '2019-12-05 15:55:30'),
(10, 31, 6, 0, '', '', 1, '2019-12-05 15:55:30'),
(11, 60, 7, 0, '', '', 1, '2019-12-06 06:40:08'),
(12, 60, 8, 0, '', '', 1, '2019-12-06 06:40:08'),
(13, 60, 9, 0, '', '', 1, '2019-12-06 06:40:08'),
(14, 60, 10, 0, 'malo', '', 0, '2019-12-06 06:40:08'),
(15, 70, 7, 0, '', '', 1, '2019-12-06 06:41:50'),
(16, 70, 8, 0, '', '', 1, '2019-12-06 06:41:50'),
(17, 70, 9, 0, '', '', 1, '2019-12-06 06:41:50'),
(18, 70, 10, 0, '', '', 1, '2019-12-06 06:41:50'),
(19, 71, 19, 0, '', '', 1, '2019-12-06 06:52:16'),
(20, 71, 20, 0, '', '', 1, '2019-12-06 06:52:16'),
(21, 71, 21, 0, '', '', 1, '2019-12-06 06:52:17'),
(22, 71, 22, 0, '', '', 1, '2019-12-06 06:52:17'),
(23, 72, 19, 0, 'Mueble bajo malo', '', 0, '2019-12-06 06:52:48'),
(24, 72, 20, 0, '', '', 1, '2019-12-06 06:52:48'),
(25, 72, 21, 0, '', '', 1, '2019-12-06 06:52:48'),
(26, 72, 22, 0, '', '', 1, '2019-12-06 06:52:48'),
(27, 73, 19, 0, '', '', 1, '2019-12-06 06:53:23'),
(28, 73, 20, 0, '', '', 1, '2019-12-06 06:53:23'),
(29, 73, 21, 0, 'mala pintura', '', 0, '2019-12-06 06:53:23'),
(30, 73, 22, 0, 'cielo sucio', '', 0, '2019-12-06 06:53:23'),
(31, 73, 19, 0, '', '', 1, '2019-12-06 06:54:38'),
(32, 73, 20, 0, '', '', 1, '2019-12-06 06:54:38'),
(33, 73, 21, 0, 'mala pintura', '', 1, '2019-12-06 06:54:38'),
(34, 73, 22, 0, 'cielo sucio', '', 1, '2019-12-06 06:54:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_custom`
--

CREATE TABLE `secciones_custom` (
  `id` int(11) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `apto` int(11) NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_custom`
--

INSERT INTO `secciones_custom` (`id`, `imagen`, `apto`, `seccion`, `fecha`) VALUES
(1, 'tv', 24, 'Sala', '2019-12-05 15:47:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_default`
--

CREATE TABLE `secciones_default` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_default`
--

INSERT INTO `secciones_default` (`id`, `imagen`, `seccion`, `fecha_creacion`) VALUES
(1, 'king_bed', 'H.Principal', '2019-12-05 15:44:47'),
(2, 'tv', 'Sala', '2019-12-05 15:47:59'),
(3, 'restaurant', 'Cocina', '2019-12-05 15:49:31'),
(4, 'deck', 'patio', '2019-12-05 15:53:51'),
(5, 'king_bed', 'alcoba', '2019-12-06 06:47:57'),
(6, 'restaurant', 'comedor', '2019-12-06 06:49:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_aptos`
--

CREATE TABLE `tipos_aptos` (
  `id` int(10) UNSIGNED NOT NULL,
  `proyecto` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tipos_aptos`
--

INSERT INTO `tipos_aptos` (`id`, `proyecto`, `nombre`, `fecha_creacion`) VALUES
(1, 1, 'A', '2019-12-05 15:44:38'),
(2, 1, 'B', '2019-12-05 15:49:26'),
(3, 1, 'C', '2019-12-05 15:53:43'),
(4, 2, '1', '2019-12-06 06:33:26'),
(5, 2, '2', '2019-12-06 06:33:45'),
(6, 3, '1', '2019-12-06 06:47:34'),
(7, 3, '2', '2019-12-06 06:48:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torres_proyecto`
--

CREATE TABLE `torres_proyecto` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `torres_proyecto`
--

INSERT INTO `torres_proyecto` (`id`, `id_proyecto`, `nombre`) VALUES
(1, 1, 'Torre1'),
(2, 1, 'Torre2'),
(3, 2, 'Torre1'),
(4, 2, 'Torre2'),
(5, 3, 'Torre1'),
(6, 3, 'Torre2'),
(7, 3, 'Torre3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `pass` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `nivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `pass`, `nivel`) VALUES
(1, 'Capital', 'admin@capital.com', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 'Carlos', 'carlos@capital.com', 'dc599a9972fde3045dab59dbd1ae170b', 1),
(3, 'Cesar', 'cesar@capital.com', '6f597c1ddab467f7bf5498aad1b41899', 1),
(4, 'Pedro', 'pedro@capital.com', 'c6cc8094c2dc07b700ffcc36d64e2138', 2),
(5, 'Pepe', 'pepe@capital.com', '926e27eecdbc7a18858b3798ba99bddd', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idp` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `idapto` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `visitas`
--

INSERT INTO `visitas` (`id`, `idp`, `user`, `idapto`, `fecha`) VALUES
(1, 1, 2, 6, '2019-12-05 15:46:19'),
(2, 1, 2, 23, '2019-12-05 15:46:35'),
(3, 1, 2, 24, '2019-12-05 15:47:49'),
(4, 1, 2, 24, '2019-12-05 15:49:57'),
(5, 1, 2, 24, '2019-12-05 15:50:26'),
(6, 1, 2, 7, '2019-12-05 15:51:09'),
(7, 1, 2, 24, '2019-12-05 15:51:22'),
(8, 1, 2, 31, '2019-12-05 15:54:10'),
(9, 1, 2, 32, '2019-12-05 15:54:26'),
(10, 1, 2, 31, '2019-12-05 15:55:12'),
(11, 2, 2, 60, '2019-12-06 06:39:47'),
(12, 2, 2, 70, '2019-12-06 06:40:23'),
(13, 3, 2, 71, '2019-12-06 06:51:38'),
(14, 3, 2, 72, '2019-12-06 06:52:31'),
(15, 3, 2, 73, '2019-12-06 06:53:02'),
(16, 3, 2, 73, '2019-12-06 06:53:36');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aptos_torre`
--
ALTER TABLE `aptos_torre`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `campos_entrega`
--
ALTER TABLE `campos_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracs_custom`
--
ALTER TABLE `caracs_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracs_default`
--
ALTER TABLE `caracs_default`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura`
--
ALTER TABLE `nomenclatura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura_history`
--
ALTER TABLE `nomenclatura_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura_oficial`
--
ALTER TABLE `nomenclatura_oficial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_entrega`
--
ALTER TABLE `obs_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_generales`
--
ALTER TABLE `obs_generales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_generales_entrega`
--
ALTER TABLE `obs_generales_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proyecto_complete`
--
ALTER TABLE `proyecto_complete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas`
--
ALTER TABLE `reformas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas_entrega`
--
ALTER TABLE `reformas_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas_h`
--
ALTER TABLE `reformas_h`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `revision_apto`
--
ALTER TABLE `revision_apto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_apto`
--
ALTER TABLE `secciones_apto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_caracs`
--
ALTER TABLE `secciones_caracs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `secciones_caracs_history`
--
ALTER TABLE `secciones_caracs_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_custom`
--
ALTER TABLE `secciones_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_default`
--
ALTER TABLE `secciones_default`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_aptos`
--
ALTER TABLE `tipos_aptos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `torres_proyecto`
--
ALTER TABLE `torres_proyecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aptos_torre`
--
ALTER TABLE `aptos_torre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `campos_entrega`
--
ALTER TABLE `campos_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `caracs_custom`
--
ALTER TABLE `caracs_custom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `caracs_default`
--
ALTER TABLE `caracs_default`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `nomenclatura`
--
ALTER TABLE `nomenclatura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `nomenclatura_history`
--
ALTER TABLE `nomenclatura_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `nomenclatura_oficial`
--
ALTER TABLE `nomenclatura_oficial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `obs_entrega`
--
ALTER TABLE `obs_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `obs_generales`
--
ALTER TABLE `obs_generales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `obs_generales_entrega`
--
ALTER TABLE `obs_generales_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proyecto_complete`
--
ALTER TABLE `proyecto_complete`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `reformas`
--
ALTER TABLE `reformas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reformas_entrega`
--
ALTER TABLE `reformas_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reformas_h`
--
ALTER TABLE `reformas_h`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `revision_apto`
--
ALTER TABLE `revision_apto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `secciones_apto`
--
ALTER TABLE `secciones_apto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secciones_caracs`
--
ALTER TABLE `secciones_caracs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `secciones_caracs_history`
--
ALTER TABLE `secciones_caracs_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `secciones_custom`
--
ALTER TABLE `secciones_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `secciones_default`
--
ALTER TABLE `secciones_default`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipos_aptos`
--
ALTER TABLE `tipos_aptos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `torres_proyecto`
--
ALTER TABLE `torres_proyecto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
