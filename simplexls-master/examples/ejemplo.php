<?php /** @noinspection ForgottenDebugOutputInspection */
require "../src/SimpleXLS.php";
echo '<h1>Parse books.xsl</h1><pre>';
echo '<table cellspacing="0" style="border:1px solid #333">';
echo '<tr><th colspan="7">REFORMAS</th></tr>';
if ( $xls = SimpleXLS::parse('reformas.xls') ) {
	//print_r( $xls->rows() );
	for($x=1;$x<count($xls->rows());$x++){
		echo '<tr>';
		$arr_rows=$xls->rows();
		for($i=0;$i<count($arr_rows[$x]);$i++){
			$clase=($x==1)?'class="tdhead"':'class="tdata"';
			echo '<td '.$clase.' style="border:1px solid #333">'.$arr_rows[$x][$i].'</td>';
			//if($x==1) array_push($arr_rows[$x], '<textarea placeholder="Observaciones"></textarea>');
			//if($x==1) array_push($arr_rows[$x], 'foto');
		}
		echo '</tr>';
	}
} else {
	echo SimpleXLS::parseError();
}
echo '<pre>';
