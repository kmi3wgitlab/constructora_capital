-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-01-2020 a las 00:24:55
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `capital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptos_torre`
--

CREATE TABLE `aptos_torre` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `torre` varchar(100) NOT NULL,
  `naptos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptos_torre`
--

INSERT INTO `aptos_torre` (`id`, `id_proyecto`, `torre`, `naptos`) VALUES
(1, 1, 'Torre1', 10),
(2, 2, 'Torre1', 10),
(3, 3, 'Torre1', 10),
(4, 4, 'Torre1', 10),
(5, 5, 'Torre1', 10),
(6, 6, 'Torre1', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campos_entrega`
--

CREATE TABLE `campos_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `apto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `propietario` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `cedula` int(100) NOT NULL,
  `torre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `parqueadero` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `c_util` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `proyecto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `municipio` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `energia` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `acueducto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `gas` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `municipio2` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `ingeniero` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `firma1` text COLLATE latin1_spanish_ci NOT NULL,
  `firma2` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `archivo` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `campos_entrega`
--

INSERT INTO `campos_entrega` (`id`, `idapto`, `apto`, `propietario`, `cedula`, `torre`, `parqueadero`, `c_util`, `proyecto`, `direccion`, `municipio`, `energia`, `acueducto`, `gas`, `municipio2`, `ingeniero`, `firma1`, `firma2`, `fecha`, `archivo`) VALUES
(48, 11, '102', 'alejo', 1234, 'Torre1', '1', '1', 'orquideas', 'medellin', 'antioquia', '12', '13', '14', 'antioquia', 'Inj.Alejo', 'k2g6SI2bvc.png', 'L8AlSBi2j6.png', 'clientes de Noviembre del 2019', 'ENTREGA_INMUEBLE_164647241.pdf'),
(52, 70, '504', 'carlos a', 1234566, 'Torre2', '1', '1', 'a1', 'calle2', 'Medellín', '2', '56', '1', 'mede', '', 'Lf93RExRP4.png', 'o9vgN8QeHG.png', '2 de Diciembre del 2019', 'ENTREGA_INMUEBLE_554422923.pdf'),
(53, 71, '101', 'qwe', 123, 'Torre1', '1', '1', 'b1', '1', '1', '1', '1', '1', '1', 'sdasdasdasdas', 'vJCQeiWf9o.png', 'T1ZmVAKN1v.png', '23 de Diciembre del 2019', 'ENTREGA_INMUEBLE_712579416.pdf'),
(57, 1, '101', 'PEDRO PEREZ', 798797889, 'Torre1', '67', '67', 'PROY1', 'CR 35', 'MEDELLIN', '78', '78', '78', 'MEDELLIN', 'LEYLA', 'tk2rtwBWz9.png', 'bfJc21rETQ.png', '12 de Diciembre del 2019', 'ENTREGA_INMUEBLE_721161838.pdf'),
(60, 23, '201', 'q', 1, 'Torre1', '1', '1', 'Bitall', 'medellin', 'anqui', '1', '1', '1', 'ASD1', 'PEPITO', 'cOZ4L5clwE.png', '2uamKuv1H5.png', '1 de Diciembre del 2019', 'ENTREGA_INMUEBLE_1958238921.pdf'),
(62, 22, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', 'FKgw7jTaQ6.png', 'Vya0eY57aa.png', '', ''),
(63, 3, '101', 'PEDRO', 53453, 'Torre1', '67', '67', 'PROY1', 'CR 43333', 'MEDELLIN', '78', '78', '78', 'MEDELLIN', 'ALEX', 'TBLV7fKYwX.png', '1CSEvyLgQT.png', '2 de Enero del 2020', 'ENTREGA_INMUEBLE_1924717772.pdf'),
(65, 36, '101', 'hjhj', 7, 'Torre1', '7', '7', 'PROY XX', 'ghg', 'ghg', '7', '7', '7', 'huihu', 'jfj', 'KasunqhNj5.png', 'Ig33Re1ZNS.png', '13 de Enero del 2020', 'ENTREGA_INMUEBLE_1375434062.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracs_custom`
--

CREATE TABLE `caracs_custom` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracs_default`
--

CREATE TABLE `caracs_default` (
  `id` int(10) UNSIGNED NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desist_history`
--

CREATE TABLE `desist_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura`
--

CREATE TABLE `nomenclatura` (
  `id` int(10) UNSIGNED NOT NULL,
  `idtorre` int(11) NOT NULL,
  `idproyecto` int(11) NOT NULL,
  `desde_piso` int(11) NOT NULL,
  `hasta_piso` int(11) NOT NULL,
  `naptos` int(11) NOT NULL,
  `nomenc1` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nomenc2` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nomenclatura` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nomenclatura`
--

INSERT INTO `nomenclatura` (`id`, `idtorre`, `idproyecto`, `desde_piso`, `hasta_piso`, `naptos`, `nomenc1`, `nomenc2`, `nomenclatura`) VALUES
(1, 2, 2, 1, 2, 5, '1', '5', ''),
(2, 4, 4, 1, 2, 5, '1', '5', ''),
(3, 6, 6, 1, 2, 5, '1', '5', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura_history`
--

CREATE TABLE `nomenclatura_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `clase` int(11) NOT NULL,
  `nomenclatura` int(11) NOT NULL,
  `reforma` varchar(100) NOT NULL,
  `obs_generales` text NOT NULL,
  `imgs_generales` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `entrega` int(11) NOT NULL,
  `entregado` int(11) NOT NULL,
  `desistido` int(11) NOT NULL,
  `informe` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nomenclatura_history`
--

INSERT INTO `nomenclatura_history` (`id`, `idapto`, `clase`, `nomenclatura`, `reforma`, `obs_generales`, `imgs_generales`, `aprobado`, `entrega`, `entregado`, `desistido`, `informe`, `fecha`) VALUES
(1, 24, 11, 0, '', 'pend general', '', 0, 0, 0, 0, '', '2020-01-24 15:20:01'),
(2, 24, 11, 0, '', 'pend general', '', 0, 0, 0, 0, '', '2020-01-24 15:22:24'),
(3, 24, 11, 0, '', 'pend general', '', 0, 0, 0, 0, 'historico_159779307.pdf', '2020-01-24 15:28:02'),
(4, 24, 11, 0, '', 'pend general', '', 0, 0, 0, 0, 'historico_1803255364.pdf', '2020-01-24 15:30:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomenclatura_oficial`
--

CREATE TABLE `nomenclatura_oficial` (
  `id` int(10) UNSIGNED NOT NULL,
  `idtorre` int(11) NOT NULL,
  `nomenclatura` int(11) NOT NULL,
  `clase` int(11) NOT NULL,
  `reforma` varchar(150) COLLATE latin1_spanish_ci NOT NULL,
  `obs_generales` text COLLATE latin1_spanish_ci NOT NULL,
  `imgs_generales` text COLLATE latin1_spanish_ci NOT NULL,
  `aprobado` int(11) NOT NULL,
  `entrega` int(11) NOT NULL,
  `entregado` int(11) NOT NULL COMMENT '0 cuando el propietario no lo ha aprobado, 1 cuando todo está 100% ok',
  `desistido` int(11) NOT NULL,
  `informe` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `nomenclatura_oficial`
--

INSERT INTO `nomenclatura_oficial` (`id`, `idtorre`, `nomenclatura`, `clase`, `reforma`, `obs_generales`, `imgs_generales`, `aprobado`, `entrega`, `entregado`, `desistido`, `informe`, `fecha`) VALUES
(1, 2, 201, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(2, 2, 102, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(3, 2, 105, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(4, 2, 101, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(5, 2, 104, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(6, 2, 103, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(7, 2, 202, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(8, 2, 203, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(9, 2, 204, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:22'),
(10, 2, 205, 3, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:09:23'),
(11, 4, 102, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(12, 4, 103, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(13, 4, 101, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(14, 4, 104, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(15, 4, 105, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(16, 4, 201, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(17, 4, 202, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:53'),
(18, 4, 203, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:54'),
(19, 4, 204, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:54'),
(20, 4, 205, 7, '', '', '', 0, 0, 0, 0, '', '2020-01-19 20:58:54'),
(21, 6, 103, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(22, 6, 102, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(23, 6, 105, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(24, 6, 101, 11, '', 'pend general', '', 0, 0, 0, 0, 'historico_159779307.pdf', '2020-01-24 15:30:44'),
(25, 6, 201, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(26, 6, 104, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(27, 6, 202, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(28, 6, 203, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(29, 6, 204, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11'),
(30, 6, 205, 11, '', '', '', 0, 0, 0, 0, '', '2020-01-24 11:18:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_entrega`
--

CREATE TABLE `obs_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `caracs` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `obs` text NOT NULL,
  `obs2` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_generales`
--

CREATE TABLE `obs_generales` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text NOT NULL,
  `imgs` text NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `obs_generales`
--

INSERT INTO `obs_generales` (`id`, `idapto`, `obs`, `imgs`, `fecha`) VALUES
(1, 24, 'pend general', '', '2020-01-24 15:15:49'),
(2, 24, 'pend general', '', '2020-01-24 15:20:00'),
(3, 24, 'pend general', '', '2020-01-24 15:22:24'),
(4, 24, 'pend general', '', '2020-01-24 15:28:02'),
(5, 24, 'pend general', '', '2020-01-24 15:30:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obs_generales_entrega`
--

CREATE TABLE `obs_generales_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text COLLATE latin1_spanish_ci NOT NULL,
  `imgs` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `n_aptos` int(11) NOT NULL,
  `n_torres` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `desactivado` int(11) NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `nombre`, `n_aptos`, `n_torres`, `imagen`, `desactivado`, `aprobado`, `fecha_creacion`) VALUES
(1, 'PROY1', 10, 1, 'iproyecto.png', 0, 0, '2020-01-19 19:49:00'),
(2, 'PROY2', 10, 1, 'iproyecto.png', 0, 0, '2020-01-19 20:03:41'),
(3, 'PROY XX', 10, 1, 'iproyecto.png', 0, 0, '2020-01-19 20:23:01'),
(4, 'PROY CC', 10, 1, 'iproyecto.png', 0, 0, '2020-01-19 20:37:47'),
(5, 'PROY PP', 10, 1, 'iproyecto.png', 0, 0, '2020-01-19 21:01:57'),
(6, 'PROY LAP', 10, 1, 'iproyecto.png', 0, 0, '2020-01-24 11:17:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto_complete`
--

CREATE TABLE `proyecto_complete` (
  `id` int(10) UNSIGNED NOT NULL,
  `iduser` int(11) NOT NULL,
  `idp` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fechap` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proyecto_complete`
--

INSERT INTO `proyecto_complete` (`id`, `iduser`, `idp`, `estado`, `fechap`) VALUES
(1, 3, 1, 0, '2020-01-19 19:49:00'),
(3, 3, 3, 0, '2020-01-19 20:23:02'),
(5, 3, 5, 0, '2020-01-19 21:01:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas`
--

CREATE TABLE `reformas` (
  `id` int(10) UNSIGNED NOT NULL,
  `indice` int(11) NOT NULL COMMENT 'indice de la reforma',
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas_entrega`
--

CREATE TABLE `reformas_entrega` (
  `id` int(10) UNSIGNED NOT NULL,
  `indice` int(11) NOT NULL COMMENT 'indice de la reforma',
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reformas_h`
--

CREATE TABLE `reformas_h` (
  `id` int(10) UNSIGNED NOT NULL,
  `indice` int(11) NOT NULL,
  `idapto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `valorx` varchar(50) NOT NULL,
  `valory` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `fotos` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `activa` int(11) NOT NULL,
  `nuevo` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revision_apto`
--

CREATE TABLE `revision_apto` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `obs` text NOT NULL,
  `obs2` text NOT NULL,
  `imgs` text NOT NULL,
  `carac` int(11) NOT NULL,
  `custom` int(11) NOT NULL COMMENT 'me dice si es carac custom',
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `revision_apto`
--

INSERT INTO `revision_apto` (`id`, `idapto`, `obs`, `obs2`, `imgs`, `carac`, `custom`, `aprobado`, `fecha`) VALUES
(1, 0, '', '', '', 1, 0, 0, '0000-00-00 00:00:00'),
(2, 0, '', '', '', 2, 0, 0, '0000-00-00 00:00:00'),
(3, 0, '', '', '', 3, 0, 0, '0000-00-00 00:00:00'),
(4, 0, '', '', '', 4, 0, 0, '0000-00-00 00:00:00'),
(5, 0, '', '', '', 5, 0, 0, '0000-00-00 00:00:00'),
(6, 0, '', '', '', 6, 0, 0, '0000-00-00 00:00:00'),
(7, 0, '', '', '', 7, 0, 0, '0000-00-00 00:00:00'),
(8, 0, '', '', '', 8, 0, 0, '0000-00-00 00:00:00'),
(9, 0, '', '', '', 9, 0, 0, '0000-00-00 00:00:00'),
(10, 0, '', '', '', 10, 0, 0, '0000-00-00 00:00:00'),
(11, 0, '', '', '', 11, 0, 0, '0000-00-00 00:00:00'),
(12, 0, '', '', '', 12, 0, 0, '0000-00-00 00:00:00'),
(13, 0, '', '', '', 13, 0, 0, '0000-00-00 00:00:00'),
(14, 0, '', '', '', 14, 0, 0, '0000-00-00 00:00:00'),
(15, 0, '', '', '', 15, 0, 0, '0000-00-00 00:00:00'),
(16, 0, '', '', '', 16, 0, 0, '0000-00-00 00:00:00'),
(17, 0, '', '', '', 17, 0, 0, '0000-00-00 00:00:00'),
(18, 0, '', '', '', 18, 0, 0, '0000-00-00 00:00:00'),
(19, 0, '', '', '', 19, 0, 0, '0000-00-00 00:00:00'),
(20, 0, '', '', '', 20, 0, 0, '0000-00-00 00:00:00'),
(21, 0, '', '', '', 21, 0, 0, '0000-00-00 00:00:00'),
(22, 0, '', '', '', 22, 0, 0, '0000-00-00 00:00:00'),
(23, 0, '', '', '', 23, 0, 0, '0000-00-00 00:00:00'),
(24, 0, '', '', '', 24, 0, 0, '0000-00-00 00:00:00'),
(25, 0, '', '', '', 25, 0, 0, '0000-00-00 00:00:00'),
(26, 0, '', '', '', 26, 0, 0, '0000-00-00 00:00:00'),
(27, 0, '', '', '', 27, 0, 0, '0000-00-00 00:00:00'),
(28, 0, '', '', '', 28, 0, 0, '0000-00-00 00:00:00'),
(29, 24, 'obs visita1', 'pendiente1', '', 27, 0, 0, '2020-01-24 15:30:46'),
(30, 24, 'obs visita2', 'pendiente2', '', 28, 0, 0, '2020-01-24 15:30:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE `secciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `proyecto` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `seccion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`id`, `imagen`, `proyecto`, `tipo`, `seccion`, `fecha_creacion`) VALUES
(1, 'kitchen', 1, 1, 'COCINA', '2020-01-19 19:50:49'),
(2, 'bathtub', 1, 2, 'JACUZI', '2020-01-19 19:53:27'),
(3, 'wc', 2, 3, 'BAÑO', '2020-01-19 20:04:01'),
(4, 'kitchen', 2, 4, 'COCINA', '2020-01-19 20:08:28'),
(5, 'kitchen', 3, 5, 'COCINA', '2020-01-19 20:23:14'),
(6, 'wc', 3, 5, 'BAÑO', '2020-01-19 20:24:31'),
(7, 'deck', 3, 6, 'JARDIN', '2020-01-19 20:25:13'),
(8, 'kitchen', 4, 7, 'cocinas', '2020-01-19 20:39:00'),
(9, 'wc', 4, 7, 'baño', '2020-01-19 20:39:29'),
(10, 'king_bed', 4, 8, 'alcoba', '2020-01-19 20:39:56'),
(11, 'wc', 5, 9, 'BAÑO', '2020-01-19 21:02:12'),
(12, 'bathtub', 5, 10, 'JACUZI', '2020-01-19 21:02:40'),
(13, 'wc', 6, 11, 'baño', '2020-01-24 11:17:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_apto`
--

CREATE TABLE `secciones_apto` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `seccion` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_caracs`
--

CREATE TABLE `secciones_caracs` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `seccion` int(11) NOT NULL,
  `caracs` text COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `secciones_caracs`
--

INSERT INTO `secciones_caracs` (`id`, `imagen`, `seccion`, `caracs`, `fecha_creacion`) VALUES
(1, '', 1, 'C1', '2020-01-19 19:50:53'),
(2, '', 1, 'C2', '2020-01-19 19:50:53'),
(3, '', 2, 'JA1', '2020-01-19 19:53:34'),
(4, '', 2, 'JA2', '2020-01-19 19:53:34'),
(5, '', 3, 'B1', '2020-01-19 20:07:45'),
(6, '', 3, 'B2', '2020-01-19 20:07:45'),
(7, '', 4, 'C1', '2020-01-19 20:08:32'),
(8, '', 4, 'C2', '2020-01-19 20:08:33'),
(9, '', 5, 'C1', '2020-01-19 20:23:21'),
(10, '', 5, 'C2', '2020-01-19 20:23:21'),
(11, '', 6, 'BI', '2020-01-19 20:24:37'),
(12, '', 6, 'B2', '2020-01-19 20:24:38'),
(13, '', 7, 'JR1', '2020-01-19 20:25:24'),
(14, '', 7, 'JR2', '2020-01-19 20:25:24'),
(17, '', 9, 'b1', '2020-01-19 20:39:35'),
(18, '', 9, 'b2', '2020-01-19 20:39:36'),
(19, '', 10, 'al1', '2020-01-19 20:40:03'),
(20, '', 10, 'al2', '2020-01-19 20:40:04'),
(21, '', 8, 'rr', '2020-01-19 20:44:02'),
(22, '', 8, 'er', '2020-01-19 20:44:03'),
(23, '', 11, 'B1', '2020-01-19 21:02:18'),
(24, '', 11, 'B2', '2020-01-19 21:02:18'),
(25, '', 12, 'JA1', '2020-01-19 21:02:50'),
(26, '', 12, 'JA2', '2020-01-19 21:02:50'),
(27, '', 13, 'b1', '2020-01-24 11:17:46'),
(28, '', 13, 'b2', '2020-01-24 11:17:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_caracs_history`
--

CREATE TABLE `secciones_caracs_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `idapto` int(11) NOT NULL,
  `carac` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `obs` text NOT NULL,
  `obs2` text NOT NULL,
  `imgs` text NOT NULL,
  `aprobado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_caracs_history`
--

INSERT INTO `secciones_caracs_history` (`id`, `idapto`, `carac`, `custom`, `obs`, `obs2`, `imgs`, `aprobado`, `fecha`) VALUES
(1, 24, 28, 0, 'pendiente1', 'pendiente2', '', 0, '2020-01-24 15:20:00'),
(2, 24, 27, 0, 'obs visita1', 'pendiente1', '', 0, '2020-01-24 15:20:00'),
(3, 24, 28, 0, 'pendiente1', 'pendiente2', '', 0, '2020-01-24 15:22:24'),
(4, 24, 27, 0, 'obs visita1', 'pendiente1', '', 0, '2020-01-24 15:22:24'),
(5, 24, 27, 0, 'obs visita1', 'pendiente1', '', 0, '2020-01-24 15:27:52'),
(6, 24, 28, 0, 'pendiente1', 'pendiente2', '', 0, '2020-01-24 15:28:02'),
(7, 24, 28, 0, 'obs visita2', 'pendiente2', '', 0, '2020-01-24 15:30:44'),
(8, 24, 27, 0, 'obs visita1', 'pendiente1', '', 0, '2020-01-24 15:30:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_custom`
--

CREATE TABLE `secciones_custom` (
  `id` int(11) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `apto` int(11) NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones_default`
--

CREATE TABLE `secciones_default` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `seccion` varchar(100) NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `secciones_default`
--

INSERT INTO `secciones_default` (`id`, `imagen`, `seccion`, `fecha_creacion`) VALUES
(1, 'kitchen', 'COCINA', '2020-01-19 19:50:49'),
(2, 'bathtub', 'JACUZI', '2020-01-19 19:53:27'),
(3, 'wc', 'BAÑO', '2020-01-19 20:04:03'),
(4, 'deck', 'JARDIN', '2020-01-19 20:25:13'),
(5, 'king_bed', 'alcoba', '2020-01-19 20:39:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_aptos`
--

CREATE TABLE `tipos_aptos` (
  `id` int(10) UNSIGNED NOT NULL,
  `proyecto` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `tipos_aptos`
--

INSERT INTO `tipos_aptos` (`id`, `proyecto`, `nombre`, `fecha_creacion`) VALUES
(1, 1, 'TIPO X', '2020-01-19 19:50:44'),
(2, 1, 'TIPO Z', '2020-01-19 19:53:14'),
(3, 2, 'TIPO A', '2020-01-19 20:03:51'),
(4, 2, 'TIPO B', '2020-01-19 20:08:22'),
(5, 3, 'TIPO 1', '2020-01-19 20:23:08'),
(6, 3, 'TIPO B', '2020-01-19 20:25:07'),
(7, 4, 'TIPO A', '2020-01-19 20:38:11'),
(8, 4, 'tipo b', '2020-01-19 20:39:50'),
(9, 5, 'TIPO 1', '2020-01-19 21:02:06'),
(10, 5, 'TIPO 2', '2020-01-19 21:02:33'),
(11, 6, 'tipo x', '2020-01-24 11:17:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `torres_proyecto`
--

CREATE TABLE `torres_proyecto` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `torres_proyecto`
--

INSERT INTO `torres_proyecto` (`id`, `id_proyecto`, `nombre`) VALUES
(1, 1, 'Torre1'),
(2, 2, 'Torre1'),
(3, 3, 'Torre1'),
(4, 4, 'Torre1'),
(5, 5, 'Torre1'),
(6, 6, 'Torre1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `pass` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `nivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `email`, `pass`, `nivel`) VALUES
(1, 'Capital', 'admin@capital.com', '21232f297a57a5a743894a0e4a801fc3', 1),
(2, 'Carlos', 'carlos@capital.com', 'dc599a9972fde3045dab59dbd1ae170b', 1),
(3, 'Cesar', 'cesar@capital.com', '6f597c1ddab467f7bf5498aad1b41899', 1),
(4, 'Pedro', 'pedro@capital.com', 'c6cc8094c2dc07b700ffcc36d64e2138', 2),
(5, 'Pepe', 'pepe@capital.com', '926e27eecdbc7a18858b3798ba99bddd', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idp` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `idapto` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `visitas`
--

INSERT INTO `visitas` (`id`, `idp`, `user`, `idapto`, `fecha`) VALUES
(1, 6, 3, 24, '2020-01-24 15:18:06'),
(2, 6, 3, 24, '2020-01-24 15:20:08'),
(3, 6, 3, 24, '2020-01-24 15:21:11'),
(4, 6, 3, 24, '2020-01-24 15:27:11'),
(5, 6, 3, 24, '2020-01-24 15:30:30'),
(6, 6, 3, 24, '2020-01-24 15:30:57');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aptos_torre`
--
ALTER TABLE `aptos_torre`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `campos_entrega`
--
ALTER TABLE `campos_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracs_custom`
--
ALTER TABLE `caracs_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `caracs_default`
--
ALTER TABLE `caracs_default`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `desist_history`
--
ALTER TABLE `desist_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura`
--
ALTER TABLE `nomenclatura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura_history`
--
ALTER TABLE `nomenclatura_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nomenclatura_oficial`
--
ALTER TABLE `nomenclatura_oficial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_entrega`
--
ALTER TABLE `obs_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_generales`
--
ALTER TABLE `obs_generales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `obs_generales_entrega`
--
ALTER TABLE `obs_generales_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proyecto_complete`
--
ALTER TABLE `proyecto_complete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas`
--
ALTER TABLE `reformas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas_entrega`
--
ALTER TABLE `reformas_entrega`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reformas_h`
--
ALTER TABLE `reformas_h`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `revision_apto`
--
ALTER TABLE `revision_apto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_apto`
--
ALTER TABLE `secciones_apto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_caracs`
--
ALTER TABLE `secciones_caracs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `secciones_caracs_history`
--
ALTER TABLE `secciones_caracs_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_custom`
--
ALTER TABLE `secciones_custom`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `secciones_default`
--
ALTER TABLE `secciones_default`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_aptos`
--
ALTER TABLE `tipos_aptos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `torres_proyecto`
--
ALTER TABLE `torres_proyecto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aptos_torre`
--
ALTER TABLE `aptos_torre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `campos_entrega`
--
ALTER TABLE `campos_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `caracs_custom`
--
ALTER TABLE `caracs_custom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `caracs_default`
--
ALTER TABLE `caracs_default`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `desist_history`
--
ALTER TABLE `desist_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `nomenclatura`
--
ALTER TABLE `nomenclatura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nomenclatura_history`
--
ALTER TABLE `nomenclatura_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `nomenclatura_oficial`
--
ALTER TABLE `nomenclatura_oficial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `obs_entrega`
--
ALTER TABLE `obs_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `obs_generales`
--
ALTER TABLE `obs_generales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `obs_generales_entrega`
--
ALTER TABLE `obs_generales_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `proyecto_complete`
--
ALTER TABLE `proyecto_complete`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `reformas`
--
ALTER TABLE `reformas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reformas_entrega`
--
ALTER TABLE `reformas_entrega`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reformas_h`
--
ALTER TABLE `reformas_h`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `revision_apto`
--
ALTER TABLE `revision_apto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `secciones_apto`
--
ALTER TABLE `secciones_apto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secciones_caracs`
--
ALTER TABLE `secciones_caracs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `secciones_caracs_history`
--
ALTER TABLE `secciones_caracs_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `secciones_custom`
--
ALTER TABLE `secciones_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secciones_default`
--
ALTER TABLE `secciones_default`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipos_aptos`
--
ALTER TABLE `tipos_aptos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `torres_proyecto`
--
ALTER TABLE `torres_proyecto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
