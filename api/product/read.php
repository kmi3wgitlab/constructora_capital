<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$eldir_foto = "http://200.122.212.98/"; //directorio para subir imagenes tomadas con la cámara en revisiones
$eldir = "http://200.122.212.98/";
$eldir = "https://industrailfumigaciones.com/capital/";
$eldir = "http://200.122.212.101/";
//$eldir = "http://localhost/constructora_capital/";

//phpinfo();

if(isset($_GET['accion']) and $_GET['accion']=='camera'){
    // Set new file name
    //$new_image_name = "newimage_".mt_rand().".jpg";
    $new_image_name = $_FILES["file"]["name"];

    // upload file
    $sube=move_uploaded_file($_FILES["file"]["tmp_name"], $eldir_foto.'uploads/revisiones/'.$new_image_name);
    //echo array('res' => 1, 'image' => $new_image_name) ;
    echo $new_image_name;
    exit;
}
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/product.php';
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$product = new Product($db);
 
// query products
$stmt='';
$num='';
if(!($_GET['accion']=='readxls')){
    $stmt = $product->read($_GET['accion'], $_GET);
    if($stmt != 'online') $num = $stmt->rowCount();
}

function pdf()
{ 
    $CI = & get_instance(); 
    log_message('Debug', 'mPDF class is loaded.'); 
} 
function load($param=[])
{ 
    return new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L', 'tempDir' => __DIR__ . '/mpdf_tempdir']); 
}
 
// check if more than 0 record found
switch($_GET['accion']){
    case 'isonline':
		/*ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);*/
		http_response_code(200);
		$arr_res=array('res' => 1);
		echo json_encode($arr_res);
	break;
	case 'login':
        if($num>0){
    
            $arr_res=array();
        
            // fetch() is faster than fetchAll()
            // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                // extract row
                // this will make $row['name'] to
                // just $name only
                extract($row);
        
                $arr_res = array( 'res' => 1, 'iduser' => $id, 'emailu' => $email, 'nivel' => $nivel, 'nombreu' => $nombre );
        
                //array_push($products_arr["records"], $product_item);
            }
        
            // set response code - 200 OK
            http_response_code(200);
        
            // show products data in json format
            echo json_encode($arr_res);
        }else{
        
            // set response code - 404 Not found
            //http_response_code(404);
        
            $arr_res = array( 'res' => 0 );
            echo json_encode($arr_res);
        }
    break;
    case 'getproyectos':
        $arr_pnames=array();
        $arr_pids=array();
        if($num>0){
            
            $html_proyectos="";
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $clase="";
                $estado="";
                // extract row
                // this will make $row['name'] to
                // just $name only
                extract($row);
                array_push($arr_pnames, strtoupper($nombre));
                array_push($arr_pids, strtoupper($id));

                if($aprobado==1){ 
                    $clase='aprobado';
                    $estado='APROBADO';
                }else{
                    $clase='pendiente';
                    $estado='PENDIENTE';
                }

                $entrega=(isset($_GET['entregas']))?'e':'';
                $html_proyectos.='<div class="col s12 m4 itemproyecto" data-id="'.$id.'" id="proyecto'.$id.'">
                    <div class="card">
                        <div class="card-image" onclick="detalleProyecto('.$id.', \''.$nombre.'\', \''.$entrega.'\');">
                        <img class="img-proyectos responsive-img" src="uploads/proyectos/'.$imagen.'">
                        </div>
                        <div class="card-content">
                            <h3>'.strtoupper($nombre).'</h3>
                            <span>PROYECTO</span>
                            <span class="'.$clase.'">
                            '.$estado.'</span>
							</br>
                            <a style="color:#6cd26f;" class="waves-effect waves-light modal-trigger" href="#modal1">Ver proyecto</a>
							</br>
							<a href="javascript:void(0);" class="delproyecto" onclick="delProyecto('.$id.');">Eliminar proyecto</a>

                        </div>
						<div class="clear"></div>
                        <!--<div class="card-action">
                            <div class="detalles" onclick="detalleProyecto('.$id.', \''.$nombre.'\', \''.$entrega.'\');">DETALLES
                            </div>
                        </div>--><div class="clear"></div>
						</div></div>';
            }
        
            http_response_code(200);
            $arr_res = array( 'res' => 1, 'html' => $html_proyectos, 'ids' => $arr_pids, 'names' => $arr_pnames );
            echo json_encode($arr_res);
        }
    break;
    case 'getaptos':
        if($num>0){
        
            $html_aptos="";
			$fecharev="";
            $count_torres=0;
	        $arr_tid=array();
            $arr_tname=array();
            $entrega=isset($_GET['entregas'])?1:0;
            //para saber si debo listar los aptos para entrega en el menu item 'entregas'
			$list_entrega=(isset($_GET['entrega']) and $_GET['entrega']==1)?1:0;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $arr_tid[$count_torres]=$id;
                $arr_tname[$count_torres]=$nombre;
		        $count_torres++;
            }

            $cont_torres=1;
            for($x=0;$x<$count_torres;$x++){
				$enhist=(isset($_GET['hist']))?1:0; //saber si estoy en el historial
                $stmt1 = $product->read('aptos2', array($entrega, $arr_tid[$x], $enhist, $list_entrega));
                $num1 = $stmt1->rowCount();
               // echo $arr_tid[$x].'--';
                $html_aptos.='<li>
                        <div class="collapsible-header"><h3>'.$arr_tname[$x].'</h3></div>
                        <div class="collapsible-body">';
                
                if($num1>0){
                    while ($row2 = $stmt1->fetch(PDO::FETCH_ASSOC)){
                        extract($row2);
                        ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
						//consultar si el apto tiene revisiones (para darle el estado reprogramado o pendiente)
						$revisiones=$product->read('checkRevisiones', array($id));
						//consultar si se ha desistido alguna vez
						$desistidas=$product->read('checkDesist', array($id));
						
						//html de fechas de desistimiento
						$html_d='';
						while ($row_d = $desistidas[1]->fetch(PDO::FETCH_ASSOC)){
							extract($row_d);
							$html_d.='<span>Desistido en: '.$fecha.'</span>--';
						}
						
						$ver_desist='<a id="ldesis" href="javascript:void(0);" onclick="showDesis(\''.$html_d.'\');"></a>';
						$link_desist=($desistidas[0]==1 and isset($_GET['hist']))?$ver_desist:'';
						
						
						
						$fecharev=(isset($_GET['hist']))?strtotime($fecha):'';
                        $desistido_e=($desistido==1)?'desistido':'';
                        //$fecharev=(isset($_GET['hist']))?date( 'Y-m-d H:i:s', $fecharev ):'';
                        $lahora=(isset($_GET['hist']))?$lahora:'';
						$fecharev=(isset($_GET['hist']))?date( 'Y-m-d', $fecharev ).' // '.$lahora:'';
                        $onclick=(isset($_GET['hist']))?'onclick="allRevs('.$id.', '.$nomenclatura.',\'\');"':'onclick="goSecciones('.$id.', '.$nomenclatura.', \''.$arr_tname[$x].'\');';
                        if($desistido==1) $onclick='';
                        $rev=(isset($_GET['hist']))?'Última revisión: '.$fecharev:'';

                        $desistir=(isset($_GET['hist']))?'':'<a class="waves-effect waves-light btn desistir" href="javascript:void(0);" 
                        style="" onclick="desistir('.$id.');">Desistir</a>';
                        //pendiente o aprobado
						$estadotext=($aprobado==0)?'Pendiente':'Aprobado';
						$estadotext=($revisiones>0 and $aprobado==0 and $desistido==0)?'Reprogramado':$estadotext;
                        //desistido
						$estadotext=($desistido==1)?'Desistido':$estadotext;
                        //entregado o pendiente de entrega
						if(( isset($_GET['entrega']) and $_GET['entrega'] != 0 and $entrega==1) or (isset($_GET['entregas']) and $entrega==1)){
							$estadotext=($entregado==1)?'Entregado':'Pendiente de entrega';
						}
                       
					   $linkaccion=($desistido==1 and !isset($_GET['hist']))?'<a href="javascript:void(0);" 
                            onclick="openModal('.$id.');">Escoger una acción</a>':'';
                        $desistir=($desistido==1 or isset($_GET['hist']))?'':'<a class="waves-effect waves-light btn desistir" href="javascript:void(0);" 
                        style="" onclick="desistir('.$id.');">Desistir</a>';
						$desistir=($entregado==1)?'':$desistir;
                        
						$laclase=($aprobado==0)?'naprobados':'aprobados';
						$laclase=($aprobado==0 and $revisiones>0 and $desistido==0)?'pendiente_e':$laclase;
                        if(isset($_GET['entrega']) and $_GET['entrega'] != 0 and $entrega==1 or (isset($_GET['entregas']) and $entrega==1)){
							$laclase=($entregado==1)?'aprobados':'pendiente_e';
						}
                        
						$html_aptos.='<span class="'.$laclase.'_span">'.$desistir.$link_desist.'
						<h4 class="'.$laclase.' '.$desistido_e.'" 
                         '.$onclick.' ">APTO '.$nomenclatura.' 
                        <span style="color:#ccc;font-size:1rem">('.$estadotext.')</span> <span>'.$linkaccion.'</span> <span class="lastrev">'.$rev.'</span>
                        </h4></span>';
                        $cont_torres++;
                    }
                    $html_aptos.='</div></li>';
                }else $html_aptos.='<h4>Esta torre no contiene apatamentos para entrega.</h4>';
            }

            http_response_code(200);
            $arr_res = array( 'res' => 1, 'fecharev' => $fecharev, 'html' => $html_aptos, 'html_desis' => $html_d );
            echo json_encode($arr_res);
        }
    break;
    case 'getsecsdefault':
        if($num>0){
          $secs='';
          $ids='';
          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $secs.=$seccion.';';
            $ids.=$id.';';
          }
          echo json_encode( array('res' =>1, 'ids' => $ids, 'secs' => $secs) ); 
        }else json_encode( array('res' =>0) ); 
    break;
    case 'getsecs':
        if($num>0){
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

            //require "../mpdf/src/Mpdf.php";
           /* $mpdf=new mPDF();
            // La variable $html es vuestro código que queréis pasar a PDF
            */

            require "../mpdf/vendor/autoload.php";
            $cargar=load();
           // $cargar->library('M_pdf');
            $mpdf = load([
            'mode' => 'utf-8',
            'format' => 'A4'
            ]);

            
            //$mpdf->Output('nombre.pdf','D'); // Genera el fichero y fuerza la descarga 
            //$mpdf->Output('../../uploads/historial/nombre.pdf','F');

            $tablar='';
            if(isset($_GET['fecha'])){
                //obtener el tipo de apto para el apto actual
                $tipoapto = $product->read('eltipo', $_GET['idapto']);

                //obtener reformas del apto actual
                $refapto = $product->read('reformasbd', array($_GET['idapto'], $_GET['fecha']));
                $num1 = $refapto->rowCount();
                
                if($num1>0){
                    //require "../simplexls-master/SimpleXLS.php";
                    $tablar.='<h3 style="text-align:center">Reformas</h3><table><tr>
                            <td><h3>Descripción</h3></td>
                            <td><h3>Cantidad solicitada</h3></td>
                            <td><h3>Observaciones</h3></td>
                            <td><h3>Estado</h3></td></tr>';
                    while ($row = $refapto->fetch(PDO::FETCH_ASSOC)){
                        extract($row);

                        $estador=($aprobado==0)?'Pendiente':'Aprobado';
                        $tablar.='<tr>
                            <td style="padding:10px">'.$descripcion.'</td>
                            <td style="padding:10px">'.$cantidad.'</td>
                            <td style="padding:10px">'.$observaciones.'</td>
                            <td style="padding:10px">'.$estador.'</td>
                        </tr>';
                    }
                    $tablar.= '</table>';
                }
            }
            
            

            $html_sec="";
            $html_pdf="<style>h4,h3,span{text-align:center;font-family:Arial}
                        h3{text-align:left}
                       table{width:100%}
                       table td{border:1px solid #c5cbd2;font-family:Arial;text-align:center}
                       </style>";
            if(isset($_GET['fecha'])){
                $fecharevi=($_GET['fecha']=='last')?'':"<span>Fecha de revisión: ".$_GET['fecha']."</span>";
				$html_pdf.="<div style='text-align:center'>
                        <img src='https://industrailfumigaciones.com/capital/img/logo-trans.png' 
                        style='margin-bottom:35px;width:150px' align='center' />
                        </div>
                        <h4>Detalles de la revisión</h4>
                        <h4>
                        Proyecto: ".ucfirst($_GET['pactual'])."<br>
                        Tipo de apartamento: ".ucfirst($tipoapto)."<br>
                        <span>APTO ".$_GET['elapto']."</span><br>
                        ".$fecharevi."<br>
                        <span>Estado: ".$_GET['elestado']."</span>
                        </h4><table cellpadding='0' cellspacing='0'>";
            }
	        $contsecs=0;
            $nombretipo="";

            //***********Secciones custom
            //if(!(isset($_GET['centrega']))){
            $stmt_c = $product->read('getsecsc', $_GET['idapto']);
            $num_c = $stmt_c->rowCount();
                if($num_c>0){
                    while ($row_c = $stmt_c->fetch(PDO::FETCH_ASSOC)){
                        extract($row_c);
                        $clasecolap=(isset($_GET['fecha']))?'':'collapsible-body';
                        //$nombretipo=$nombre;
                        $html_sec.='<li>
						<div class="checkgen">
							<img width="20" height="20" src="img/ok.png" onclick="secState(1, 0, '.$contasec.');">
							<img class="activo" width="20" height="20" src="img/xx.png" onclick="secState(0, 0, '.$contasec.');">
						</div>
                        <div class="collapsible-header"><span class="icon" data-carac="'.$id.'">'.$imagen.'</span><h3>'.$seccion.'</h3>
                        <div class="'.$clasecolap.'"><div class="clist">';
                        $html_pdf.='<tr>
                        <td colspan="4"><h3>'.ucfirst($seccion).'</h3></td>
                        </tr>';

                        // - obtener características de la sección
                        $fecha=(isset($_GET['fecha']))?$_GET['fecha']:'';
                        $centrega=(isset($_GET['centrega']))?1:'';
                        //SELECCIONAR CARACS **********
                        $stmtc = $product->read('seccaracsc', array($id, $fecha, $_GET['idapto'], $centrega));
                        $num1 = $stmtc->rowCount();
                        if($num1>0){
                            while ($row2 = $stmtc->fetch(PDO::FETCH_ASSOC)){
                                //extract($row2);
                                $imgs_caracs='';
                                $arr_imgsc='';
                                $count_imgs=0;
                                if(!(empty($row2['imgs']))){
                                    $arr_imgsc=explode(';', $row2['imgs']);
                                    for($x=0;$x<count($arr_imgsc);$x++){
                                        if(!(empty($arr_imgsc[$x]))){
                                            /*$imgs_caracs.='<img data-carac="'.$row2['id'].'" width="100%" id="fotosec'.$row2['id'].'" 
                                                        src="'.$eldir.'uploads/revisiones/'.$arr_imgsc[$x].'" /><br>';*/
                                            $imgs_caracs.=$eldir.'uploads/revisiones/'.$arr_imgsc[$x].';';
                                            $count_imgs++;
                                        }
                                    }
                                }
								
								$obsc=(isset($row2['obs']))?$row2['obs']:'';
								$obsp=(isset($row2['obs2']))?$row2['obs2']:'';
								$aprobadoc=(isset($row2['aprobado']))?$row2['aprobado']:'';

                                //$count_imgs=($count_imgs==0)?'':$count_imgs;
                                $img_ok=($aprobadoc==0)?'<img class="check" id="ok'.$row2['id'].'_custom" width="20" src="img/ok.png" 
                                onclick="secState(1, '.$row2['id'].', 1);" />':'<img class="activo check" id="ok'.$row2['id'].'_custom" width="20" src="img/ok.png" 
                                onclick="secState(1, '.$row2['id'].', 1);" />';
								
								//if(isset($_GET['centrega'])) $img_ok='<img class="check" id="ok'.$row2['id'].'_custom" width="20" src="img/ok.png" 
                                //onclick="secState(1, '.$row2['id'].', 1);" />';
                                
                                $img_nok=($aprobadoc==0)?'<img class="activo uncheck" id="nok'.$row2['id'].'_custom" width="20" src="img/xx.png" 
                                onclick="secState(0, '.$row2['id'].', 1);" />':'<img class="uncheck" id="nok'.$row2['id'].'_custom" width="20" src="img/xx.png" 
                                onclick="secState(0, '.$row2['id'].', 1);" />';
								
								//if(isset($_GET['centrega'])) $img_nok='<img class="uncheck" id="nok'.$row2['id'].'_custom" width="20" src="img/xx.png" 
                                //onclick="secState(0, '.$row2['id'].', 1);" />'; 

                                $aprobado_sec=($aprobadoc==0)?'<input type="hidden" class="aprobado_sec custom" 
                                name="aprobado_sec" id="aprobado_sec'.$row2['id'].'_custom" value="0" />':'<input type="hidden" class="aprobado_sec custom" 
                                name="aprobado_sec" id="aprobado_sec'.$row2['id'].'_custom" value="1" />';
								
								//if(isset($_GET['centrega'])) $aprobado_sec='<input type="hidden" class="aprobado_sec" 
                                //name="aprobado_sec" id="aprobado_sec'.$row2['id'].'_custom" value="0" />';


                                $aprobado=($aprobadoc==0)?'<span>Reprogramado</span>':'<span>Aprobado</span>';
                                $secstate=(isset($_GET['fecha']))?$aprobado:$img_ok.$img_nok.$aprobado_sec;

                                $obs=$obsc;
                                $html_sec.='<span class="spancarac" data-carac="'.$row2['id'].'">
                                    <div class="cright">
                                        <h4>'.strtoupper($row2['caracs']).'</h4>
                                        <div class="input-field">
                                            <textarea placeholder="Observaciones de la visita..." class="materialize-textarea obs_sec obs1" 
											id="obs'.$row2['id'].'_custom" 
                                            name="obs'.$row2['id'].'" >'.$obs.'</textarea>
											<textarea placeholder="Observaciones pendientes..." class="materialize-textarea obs_sec obs_secp" 
											id="obsp'.$row2['id'].'_custom" 
                                            name="obsp'.$row2['id'].'" >'.$obsp.'</textarea>
                                            <!--<label for="obs'.$row2['id'].'">Observaciones pendientes...</label>-->
                                        </div>
                                        <div class="sec_state">
                                            '.$secstate.'
                                            <div class="clear"></div>
                                        </div>
                                        <div class="camerac" id="camerac'.$row2['id'].'">
                                            <div class="cont_imgs" datac="'.$row2['id'].'" 
                                            id="cont_imgs'.$row2['id'].'" value="" onclick="openImgs('.$row2['id'].');">+'.$count_imgs.'</div>
                                            <input type="hidden" name="imgs_old" id="imgs_old'.$row2['id'].'" value="'.$imgs_caracs.'" />
                                            <input type="hidden" name="imgs" id="imgs'.$row2['id'].'" value="" />
                                            <input type="hidden" name="imgs_uris" id="imgs_uris'.$row2['id'].'" value="" />
                                            <input type="hidden" name="countimg" id="countimg'.$row2['id'].'" value="'.$count_imgs.'" />
                                            <img class="camera" onclick="tomarFoto('.$row2['id'].', 0);" id ="foto'.$row2['id'].'" src="img/camera.png" 
                                            width="30%" /><br>
                                            <div class="thumb_imgs"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    </span>';

                                $html_pdf.='<tr>
                                    <td>
                                        <table><tr>
                                        <td style="width:10%"><strong>'.strtoupper($row2['caracs']).'</strong></td>
                                        <td style="width:50%">
                                            <strong>Observaciones: </strong><br> '.$obs.'
                                        </td>
                                        <td style="width:15%">
                                            '.ucfirst($secstate).'
                                            <div class="clear"></div>
                                        </td>
										<td style="width:25%">
                                            '.$row2['fecha'].'
                                            <div class="clear"></div>
                                        </td>
                                        <!--<div class="camerac" id="camerac'.$row2['id'].'">
                                            <div class="cont_imgs" datac="'.$row2['id'].'" 
                                            id="cont_imgs'.$row2['id'].'" value="" onclick="openImgs('.$row2['id'].');">+'.$count_imgs.'</div>
                                            <input type="hidden" name="imgs_old" id="imgs_old'.$row2['id'].'" value="'.$imgs_caracs.'" />
                                            <input type="hidden" name="imgs" id="imgs'.$row2['id'].'" value="" />
                                            <input type="hidden" name="imgs_uris" id="imgs_uris'.$row2['id'].'" value="" />
                                            <input type="hidden" name="countimg" id="countimg'.$row2['id'].'" value="'.$count_imgs.'" />
                                            <img class="camera" onclick="tomarFoto('.$row2['id'].', 0);" id ="foto'.$row2['id'].'" src="img/camera.png" 
                                            width="30%" /><br>
                                            <div class="thumb_imgs"></div>
                                        </div>-->
                                    </tr></table></td></tr>';
                            }                    
                        }
                    }
                }
            //}

            $contasec=0;
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				extract($row);
                $clasecolap=(isset($_GET['fecha']))?'':'collapsible-body';
                $nombretipo=$nombre;
                $html_sec.='<li>
				  <div class="checkgen">
					<img width="20" height="20" src="img/ok.png" onclick="secState(1, 0, '.$contasec.');">
				    <img class="activo" width="20" height="20" src="img/xx.png" onclick="secState(0, 0, '.$contasec.');">
				  </div>
				  <div class="collapsible-header">
				  <span class="icon" data-carac="'.$id.'">'.$imagen.'</span>
				  <h3>'.$seccion.'</h3>
				  </div>
				  <div class="'.$clasecolap.'"><div class="clist">';
                $html_pdf.='<tr>
				  <td><h3 colspan="4">'.ucfirst($seccion).'</h3></td>
                  </tr>';
				  
				  $contasec++;

                // - obtener características de la sección
                $fecha=(isset($_GET['fecha']))?$_GET['fecha']:'';
                $tabla=(isset($_GET['centrega']))?'obs_entrega':'';
                //SELECCIONAR CARACS **********
                $stmt1 = $product->read('seccaracs', array($id, $fecha, $_GET['idapto'], $tabla));
                $num1 = $stmt1->rowCount();
                if($num1>0){
                    while ($row2 = $stmt1->fetch(PDO::FETCH_ASSOC)){
                        //echo print_r($row2,1);
						//extract($row2);
                        //echo print_r($row2,1);
						$imgs_caracs='';
                        $arr_imgsc='';
                        $count_imgs=0;
                        if(isset($row2['imgs']) and !(empty($row2['imgs']))){
                            $arr_imgsc=explode(';', $row2['imgs']);
                            for($x=0;$x<count($arr_imgsc);$x++){
                                if(!(empty($arr_imgsc[$x]))){
                                    /*$imgs_caracs.='<img data-carac="'.$row2['id'].'" width="100%" id="fotosec'.$row2['id'].'" 
                                                src="'.$eldir.'uploads/revisiones/'.$arr_imgsc[$x].'" /><br>';*/
                                    $imgs_caracs.=$eldir.'uploads/revisiones/'.$arr_imgsc[$x].';';
                                    $count_imgs++;
                                }
                            }
                        }

                        //$count_imgs=($count_imgs==0)?'':$count_imgs;
                        //$aprob=($tabla==''):;
                        $aprobado=(isset($row2['aprobado']) and $row2['aprobado']==0)?'<span>Pendiente</span>':'<span>Aprobado</span>';
                        
                        $obsc=(isset($row2['obs']))?$row2['obs']:'';
                        $obsp=(isset($row2['obs2']))?$row2['obs2']:'';
						$aprobadoc=(isset($row2['aprobado']))?$row2['aprobado']:'';
						
						$dis_input=($aprobadoc==0)?'':'disabled';
                        
                        $img_ok=($aprobadoc==0)?'<img class="check" id="ok'.$row2['id'].'" width="20" src="img/ok.png" 
						onclick="secState(1, '.$row2['id'].');" />':'<img class="activo check" id="ok'.$row2['id'].'" width="20" src="img/ok.png" 
						onclick="secState(1, '.$row2['id'].');" />';
						
						//if(isset($_GET['centrega'])) $img_ok='<img class="check" id="ok'.$row2['id'].'" width="20" src="img/ok.png" 
						//onclick="secState(1, '.$row2['id'].');" />';
						
						$img_nok=($aprobadoc==0)?'<img class="activo uncheck" id="nok'.$row2['id'].'" width="20" src="img/xx.png" 
						onclick="secState(0, '.$row2['id'].');" />':'<img class="uncheck" id="nok'.$row2['id'].'" width="20" src="img/xx.png" 
						onclick="secState(0, '.$row2['id'].');" />';
						
						//if(isset($_GET['centrega'])) $img_nok='<img class="uncheck" id="nok'.$row2['id'].'" width="20" src="img/xx.png" 
						//onclick="secState(0, '.$row2['id'].');" />'; 

						$aprobado_sec=($aprobadoc==0)?'<input type="hidden" class="aprobado_sec" 
						name="aprobado_sec" id="aprobado_sec'.$row2['id'].'" value="0" />':'<input type="hidden" class="aprobado_sec" 
						name="aprobado_sec" id="aprobado_sec'.$row2['id'].'" value="1" />';
						
						//if(isset($_GET['centrega'])) $aprobado_sec='<input type="hidden" class="aprobado_sec" 
						//name="aprobado_sec" id="aprobado_sec'.$row2['id'].'" value="0" />';

                        $secstate=(isset($_GET['fecha']))?$aprobado:$img_ok.$img_nok.$aprobado_sec;

                        $obs=$obsc; //observaciones
                        $html_sec.='<span class="spancarac" data-carac="'.$row2['id'].'">
                            <div class="cright">
                                <h4>'.strtoupper($row2['caracs']).'</h4>
                                <div class="input-field">
                                    <textarea '.$dis_input.' placeholder="Observaciones de la visita..." class="materialize-textarea obs_sec obs1" 
									id="obs'.$row2['id'].'" 
                                    name="obs'.$row2['id'].'" >'.$obs.'</textarea>
									<textarea '.$dis_input.' placeholder="Observaciones pendientes..." class="materialize-textarea obs_sec obs_secp" 
									id="obsp'.$row2['id'].'" 
                                    name="obsp'.$row2['id'].'" >'.$obsp.'</textarea>
                                    <!--<label for="obs'.$row2['id'].'">Observaciones...</label>-->
                                </div>
                                <div class="sec_state">
                                    '.$secstate.'
                                    <div class="clear"></div>
                                </div>
                                <div class="camerac" id="camerac'.$row2['id'].'">
                                    <div class="cont_imgs" datac="'.$row2['id'].'" 
                                    id="cont_imgs'.$row2['id'].'" value="" onclick="openImgs('.$row2['id'].');">+'.$count_imgs.'</div>
                                    <input type="hidden" name="imgs_old" id="imgs_old'.$row2['id'].'" value="'.$imgs_caracs.'" />
                                    <input type="hidden" name="imgs" id="imgs'.$row2['id'].'" value="" />
                                    <input type="hidden" name="imgs_uris" id="imgs_uris'.$row2['id'].'" value="" />
                                    <input type="hidden" name="countimg" id="countimg'.$row2['id'].'" value="'.$count_imgs.'" />
                                    <img class="camera" onclick="tomarFoto('.$row2['id'].', 0);" id ="foto'.$row2['id'].'" src="img/camera.png" 
                                    width="30%" /><br>
                                    <div class="thumb_imgs"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            </span>';

                        $html_pdf.='<tr>
                            <td>
                                <table><tr>
                                <td style="width:10%"><strong>'.strtoupper($row2['caracs']).'</strong></td>
                                <td style="width:50%">
                                    <strong>Observaciones: </strong><br> '.$obs.'
                                </td>
                                <td style="width:15%">
                                    '.ucfirst($secstate).'
                                    <div class="clear"></div>
                                </td>
								<td style="width:25%">
                                            '.$row2['fecha'].'
                                            <div class="clear"></div>
                                        </td>
                                <!--<div class="camerac" id="camerac'.$row2['id'].'">
                                    <div class="cont_imgs" datac="'.$row2['id'].'" 
                                    id="cont_imgs'.$row2['id'].'" value="" onclick="openImgs('.$row2['id'].');">+'.$count_imgs.'</div>
                                    <input type="hidden" name="imgs_old" id="imgs_old'.$row2['id'].'" value="'.$imgs_caracs.'" />
                                    <input type="hidden" name="imgs" id="imgs'.$row2['id'].'" value="" />
                                    <input type="hidden" name="imgs_uris" id="imgs_uris'.$row2['id'].'" value="" />
                                    <input type="hidden" name="countimg" id="countimg'.$row2['id'].'" value="'.$count_imgs.'" />
                                    <img class="camera" onclick="tomarFoto('.$row2['id'].', 0);" id ="foto'.$row2['id'].'" src="img/camera.png" 
                                    width="30%" /><br>
                                    <div class="thumb_imgs"></div>
                                </div>-->
                            </tr></table></td></tr>';
                    }                    
                }

                $html_sec.='</div></div></li>';
                
                $contsecs++;
            }
            $html_pdf.='</table><br><br>'.$tablar;
            //$html_pdf='cosaa';
            
            //consultar si el apto actual está aprobado
            $fecha=(isset($_GET['fecha']))?$_GET['fecha']:'';
            $estadoapto="";
            $obsg="";
            $imgsg="";
            $imgs_caracsg="";
			//obtener estado del apto, así como las obsg e imgsg
            $stmt1 = $product->read('getaprobado', array($_GET['idapto'], $fecha));
            $num1 = $stmt1->rowCount();
            if($num1>0){
                while ($row2 = $stmt1->fetch(PDO::FETCH_ASSOC)){
                     extract($row2);
                     if(!(empty($imgs_generales))){
                        $arr_imgsc=explode(';', $imgs_generales);
                        for($x=0;$x<count($arr_imgsc);$x++){
                            if(!(empty($arr_imgsc[$x]))){
                                /*$imgs_caracs.='<img data-carac="'.$row2['id'].'" width="100%" id="fotosec'.$row2['id'].'" 
                                            src="'.$eldir.'uploads/revisiones/'.$arr_imgsc[$x].'" /><br>';*/
                                $imgs_caracsg.=$eldir.'uploads/revisiones/'.$arr_imgsc[$x].';';
                                $count_imgs++;
                            }
                        }
                    }
					
					$estadoapto=$aprobado;
                     //$obsg=isset($obs)?$obs:$obs_generales;
                     $obsg=$obs_generales;
                     $imgsg=$imgs_caracsg;
					 
                     //si se está en entregas, consulto las obs generales de entregas
					 $stmt_obse="";
					 if(isset($_GET['centrega'])){
						 $stmt_obse = $product->read('obsentrega', array($_GET['idapto']));
						 $count_obs = $stmt_obse->rowCount();
						 if($count_obs>0){
							while ($row_o = $stmt_obse->fetch(PDO::FETCH_ASSOC)){
								extract($row_o);
								$obsg=$obs;
							}
						 }else $obsg='';
					 }
                }
            }
            
            //guardar pdf
            $npdf="";
            if(!(empty($fecha))){
                $html_pdf.='<br><br><h3>Observaciones generales:</h3><span>'.$obsg.'</span>';
                $mpdf->WriteHTML($html_pdf);
                $npdf='historico_'.mt_rand().'.pdf';
                $mpdf->Output('../../uploads/historial/'.$npdf,'F');
                //guardar pdf en bd
                $product->read('savepdf', array($_GET['idapto'], $npdf, $_GET['hist']));
            }
            http_response_code(200);
			$entregado=(isset($entregado))?$entregado:'';
            $arr_res = array( 
                              'entregado' => $entregado, 'res' => 1, 'pdf' => $npdf, 
							  'secs' => $html_sec, 'estadoapto' => $estadoapto, 
                              'obsg' => $obsg, 'imgsg' => $imgsg, 'ntipo' => $nombretipo, 
							  'tipoid' => $tipoid
                            );
            echo json_encode($arr_res);
        }
    break;
    case 'gettipos':
    ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        if($num>0){
        
            $html_tipos="";
	        $contsecs=0;
            $tiposs="";
            $seccont="";
			
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                //extract($row);
                $tipos = $product->read('tiposp', $row['id']);
                //si no hay datos en la bd
                while ($larow = $tipos->fetch(PDO::FETCH_ASSOC)){
                    $tiposs='<div class="col m3 s6"><h3 style="margin-bottom:0"><a class="tipolist" 
                    href="javascript:void(0);"><!--Tipo de apartamento: -->'.strtoupper($larow['nombre']).'</a> 
					</h3>';
                    $secss="";
                    $seccont.=$tiposs.'<ul class="collapsible secs">';
                    $secstipo= $product->read('secstipo', $larow['id']);
					
                     while ($larow2 = $secstipo->fetch(PDO::FETCH_ASSOC)){
                        $seccont.='<li><div class="collapsible-header">
                        <h3 class="noblue" id="psec'.$larow2['id'].'"><span class="icon">'.$larow2['imagen'].'</span> 
                        <span class="txt">'.$larow2['seccion'].'</span>
                        <!--<i class="remove" onclick="deleteSec('.$larow2['id'].')">delete</i>
                                <i class="edit" onclick="editSec('.$larow2['id'].');">edit</i>-->
                        </h3>
                        
                        </div>
                        <div class="collapsible-body">';
                        $caracstipo = $product->read('seccaracs1', $larow2['id']);
						//echo print_r($caracstipo->fetch(PDO::FETCH_ASSOC),1);
                        while ($larow3 = $caracstipo->fetch(PDO::FETCH_ASSOC)){
                            $seccont.='<span><h4>'.$larow3['caracs'].'</h4>
                                <!--<i class="remove">delete</i>
                                <i class="edit">edit</i>-->
                                <div class="clear"></div>
                                </span>';
                        }
                        $seccont.='</div></li>';
                    }
                    $seccont.='</ul></div>';
                }
                $html_tipos.=$seccont;
            }
        }
        http_response_code(200);
        $arr_res = array( 'res' => 1, 'tipos' => $html_tipos );
        echo json_encode($arr_res);
    break;
    case 'getvisita':
        if($num==0){
            http_response_code(200);
            echo json_encode(array( 'res' => 1 ));
        }else echo json_encode(array( 'res' => 0 ));
    break;
    case 'readxls':
        $stmt = $product->read($_GET['accion'], $_GET);
        //si no hay datos de reformas en la bd
        if( is_array($stmt[1]) and count($stmt[1])==0 ){
            $num = $stmt[0]->rowCount();
			//echo 'op1';
			//echo '<br>num->'.$num;
            if($num>0){
                require "../simplexls-master/SimpleXLS.php";
                $tablar=array();
                while ($row = $stmt[0]->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
					//echo 'ref->'.$reforma;
                    $arr_reformas=explode(',', $reforma);
					//echo print_r($arr_reformas,1);
					for($r=0;$r<count($arr_reformas);$r++){
						if(!(empty($arr_reformas[$r]))){
							if( $xls = SimpleXLS::parse('../../uploads/proyectos/reformas/'.$arr_reformas[$r]) ) {
								$tablar[$r]= '<table id="treforma'.$r.'" cellspacing="0" style="border:1px solid #c1b8b8">';
								$tablar[$r].= '<tr><th colspan="7">REFORMAS '.($r+1).'</th></tr>';
								//print_r( $xls->rows() );
								for($x=1;$x<count($xls->rows());$x++){
									$clasetr=($x==1)?'class="trtop"':'class="trdata"';
									$tablar[$r].= '<tr '.$clasetr.' data-id="" data-tr="'.$x.'">';
									$arr_rows=$xls->rows();
									for($i=0;$i<count($arr_rows[$x]);$i++){
										$clase=($x==1)?'class="tdhead"':'class="tdata"';
										if($x==1) $tablar[$r].= '<td '.$clase.' style="border:1px solid #c1b8b8">'.$arr_rows[$x][$i].'</td>';
										else{
											$campo='';
											if($i==0) $campo='<input type="hidden" class="tdesc" name="tdesc" id="tdesc'.$x.'" value="'.$arr_rows[$x][$i].'" />';
											if($i==1) $campo='<input type="hidden" class="tcant" name="tcant" id="tcant'.$x.'" value="'.$arr_rows[$x][$i].'" />';
											if($i==2) $campo='<input type="hidden" class="tvx" name="tvx" id="tvx'.$x.'" value="'.$arr_rows[$x][$i].'" />';
											if($i==3) $campo='<input type="hidden" class="tvy" name="tvy" id="tvy'.$x.'" value="'.$arr_rows[$x][$i].'" />';
											$tablar[$r].= '<td '.$clase.' style="border:1px solid #c1b8b8">'.$arr_rows[$x][$i].$campo.'</td>';
										} 
										//if($x==1) array_push($arr_rows[$x], '<textarea placeholder="Observaciones"></textarea>');
										//if($x==1) array_push($arr_rows[$x], 'foto');
									}
									$tablar[$r].= '</tr>';
								}
								$tablar[$r].= '</table>';
								//echo 'tablar->';
								//echo print_r($tablar,1);
							}else echo 'no read';
						}						
					}
					echo json_encode( array('res' => 1, 'tabla' => $tablar, 'bd' => 0) );
					/*else {
                        //echo SimpleXLS::parseError();
                        echo json_encode( array('res' => 0, 'msg' => 'noread', 'bd' => 0) );
                    }*/
                }
            }else echo json_encode( array('res' => 0) );
        }else{
			//echo 'op2';
			echo json_encode( array('res' => 1, 'tabla' => $stmt[1], 'bd' => 1) );
		}
    break;
    case 'allrevs':
       if($num>0){
           $html_revs="<table>";
           while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                extract($row);
                $estado=($aprobado==1)?'Aprobado':'Reprogramado';
                $fecharev=strtotime($fecha);
				$horarev=date( 'h:i: A', $fecharev );
                $fecharev=date( 'Y-m-d', $fecharev );
                $html_revs.='<tr>
                <td><h4><a href="javascript:void(0);" onclick="detailRev('.$id.', '.$idapto.', \''.$fecha.'\', '.$_GET['nomenc'].', \''.$estado.'\')">
                APTO '.$_GET['nomenc'].' </h4></td>
                <td><span>Fecha de revisión: '.$fecharev.' // '.$horarev.'</span></td>
                <td><span>'.$estado.'</span></td>
                </tr>';
           }
           $html_revs.="</table>";
           echo json_encode( array('res' => 1, 'revs' => $html_revs) );
       }else echo json_encode( array('res' => 0) ); 
    break;
}
?>