<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "proyectos";
 
    // object properties
    public $nombre;
    public $naptos;
    public $ntorres;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	public function pdf()
	{ 
		$CI = & get_instance(); 
		log_message('Debug', 'mPDF class is loaded.'); 
	} 
	public function load($param=[])
	{ 
		return new \Mpdf\Mpdf($param); 
	} 
	
	public function multi_attach_mail($to, $subject, $message, $senderMail, $senderName, $files){

		$from = $senderName." <".$senderMail.">"; 
		$headers = "From: $from";

		// boundary 
		$semi_rand = md5(time()); 
		$mime_boundary = "==Multipart_Boundary_x".$semi_rand."x"; 

		// headers for attachment 
		$headers .= "nMIME-Version: 1.0n" . "Content-Type: multipart/mixed;n" . ' boundary="'.$mime_boundary.'"'; 

		// multipart boundary 
		$message = "--".$mime_boundary."n" . "Content-Type: text/html; charset=\"UTF-8\"n" .
		"Content-Transfer-Encoding: 7bitnn" . $message . "nn"; 

		// preparing attachments
		if(count($files) > 0){
			for($i=0;$i<count($files);$i++){
				if(is_file($files[$i])){
					$message .= "--".$mime_boundary."n";
					$fp =    @fopen($files[$i],"rb");
					$data =  @fread($fp,filesize($files[$i]));
					@fclose($fp);
					$data = chunk_split(base64_encode($data));
					$message .= "Content-Type: application/octet-stream; name='".basename($files[$i])."'n" . 
					"Content-Description: ".basename($files[$i])."n" .
					"Content-Disposition: attachment;n" . " filename='".basename($files[$i])."'; 
					size='".filesize($files[$i])."';n" . 
					"Content-Transfer-Encoding: base64nn" . $data . "nn";
				}
			}
		}

		$message .= "--".$mime_boundary."--";
		$returnpath = "-f" . $senderMail;

		//send email
		$mail = mail($to, $subject, $message, $headers, $returnpath); 

		//function return true, if email sent, otherwise return fasle
		if($mail){ return true; } else { return false; }

	}

    // read products
    function read($accion, $arr_get){
    
        switch($accion){
            case 'isonline':
			 return 'online';
			break;
			case 'login':

                $sql = "SELECT * FROM usuarios 
                        where email='".$arr_get['email']."' 
                        and pass='".md5($arr_get['pass'])."'";
                // prepare query statement
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();
            // if($stmt->rowCount()>0) return $stmt;
                //else return false;

                return $stmt;

            break;
            case 'getproyectos':

                $filtro='';
				if(isset($_GET['key'])){
					$filtro=(!(empty($_GET['key'])))?"and nombre like '%".$_GET['key']."%'":"and nombre like '%1xZ8@%'";
				}
				
				$sql = "SELECT *    
						FROM proyectos where desactivado=0 ".$filtro." order by fecha_creacion desc";
                // prepare query statement
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();
            // if($stmt->rowCount()>0) return $stmt;
                //else return false;

                return $stmt;
            break;
            case 'getaptos':
                $sql = "SELECT torres_proyecto.id, torres_proyecto.nombre, 
                        proyectos.nombre as elpro  
                        FROM torres_proyecto, proyectos 
                        where torres_proyecto.id_proyecto=proyectos.id and 
                        torres_proyecto.id_proyecto=".$arr_get['idp'];
                // prepare query statement
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
            // if($stmt->rowCount()>0) return $stmt;
                //else return false;

                return $stmt;

            break;
            case 'aptos2':
			   
			   $entrega=($arr_get[0]==1)?"":"";
			   $entrega=($arr_get[3]==1)?"and entrega=1":"";
			   //para el historial solo se seleccionan los aptos q tengan datos (visitas)
			   $enhist_add=(isset($_GET['entregas']))?' and entrega=1':'';
			   //$enhist=($arr_get[2]==1)?"and obs_generales != ''".$enhist_add:""; //saber si estoy en historial
			   $enhist=($arr_get[2]==1)?$enhist_add:""; //saber si estoy en historial
			   
                /*$sql = "SELECT * FROM nomenclatura_oficial 
                            where idtorre=".$arr_get[1]." and ".$entrega."  
                            order by nomenclatura asc";*/
                $sql = "SELECT id, idtorre, nomenclatura, clase, reforma, obs_generales, 
							imgs_generales, aprobado, entrega, entregado, desistido, informe, fecha, 
							DATE_FORMAT(fecha,'%r') AS lahora 
							FROM nomenclatura_oficial  
                            where idtorre=".$arr_get[1]." ".$entrega." ".$enhist."    
                            order by nomenclatura asc";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return $stmt;

            break;
			case 'checkRevisiones':
				$sql="SELECT idapto FROM revision_apto WHERE idapto=".$arr_get[0];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				$revisiones = $stmt->rowCount();
				return $revisiones;
			break;
			case 'checkDesist':
				$sql="SELECT * FROM desist_history WHERE idapto=".$arr_get[0];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				$desis = $stmt->rowCount();
				return array($desis, $stmt);
			break;
            case 'reformasbd':
                $fecha=$arr_get[1];
				$sql="";
				if($fecha=='last'){
					$sql = "SELECT * FROM reformas_h  
							where idapto=".$arr_get[0]." 
							order by fecha desc";
				}else{
					$fecha=explode(' ', $arr_get[1]);
					$arrhora=explode(':', $fecha[1]);
					$arrfecha=explode('-', $fecha[0]);
					$sql = "SELECT * FROM reformas_h  
							where idapto=".$arr_get[0]." and 
							year(fecha)='".$arrfecha[0]."' and  
							month(fecha)='".$arrfecha[1]."' and 
							day(fecha)='".$arrfecha[2]."' and
							hour(fecha)='".$arrhora[0]."' and 
							minute(fecha)='".$arrhora[1]."'";
				}
				
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return $stmt;
            break;
            case 'savepdf':
                $sql="UPDATE nomenclatura_oficial SET informe='".$arr_get[1]."' WHERE id=".$arr_get[0];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                $sql="UPDATE nomenclatura_history SET informe='".$arr_get[1]."' WHERE id=".$arr_get[2];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
            break;
            case 'eltipo':
                $sql="SELECT tipos_aptos.nombre 
                      FROM tipos_aptos, nomenclatura_history 
                      WHERE tipos_aptos.id=nomenclatura_history.clase and 
                      nomenclatura_history.idapto=".$arr_get;
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $ntipo="";
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['name'] to
                    // just $name only
                    extract($row);
                    return $nombre;
                }
            break;
            case 'getsecsdefault':
                $sql = "SELECT * FROM secciones_default group by seccion";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                return $stmt;
            break;
            case 'getsecs':
                $tabla=(isset($_GET['fecha']))?'nomenclatura_history':'nomenclatura_oficial';
                $campoid=(isset($_GET['fecha']))?'idapto':'id';
				$sql = "SELECT clase FROM ".$tabla." where ".$campoid."=".$arr_get['idapto'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $claseapto="";
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    $claseapto=$clase;
                }
                
                $sql = "SELECT secciones.*, tipos_aptos.nombre, tipos_aptos.id as tipoid  
						FROM secciones, tipos_aptos 
                        where secciones.tipo=tipos_aptos.id 
                        and secciones.proyecto=".$arr_get['idp'].' and secciones.tipo='.$claseapto;
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();

                return $stmt;

            break;
            case 'getsecsc':
                $sql_custom="SELECT * FROM secciones_custom WHERE apto=".$arr_get;
                $stmt = $this->conn->prepare($sql_custom);
                $stmt->execute();
                return $stmt;
            break;
            case 'secstipo':
                $sql_custom="SELECT id, seccion, imagen FROM secciones WHERE tipo=".$arr_get;
                $stmt = $this->conn->prepare($sql_custom);
                $stmt->execute();
                return $stmt;
            break;
            case 'gettipos':
                $sql = "SELECT id, nombre FROM proyectos where id=".$arr_get['idp'];
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();

                return $stmt;

            break;
             case 'tiposp':
                $sql = "SELECT id, nombre FROM tipos_aptos where proyecto=".$arr_get;
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();

                return $stmt;

            break;
			case 'obsentrega':
				$sql="SELECT obs FROM obs_generales_entrega WHERE idapto=".$arr_get[0];
				$stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return $stmt;
			break;
            case 'getaprobado':
                //si viene fecha (historial)
				if(!(empty($arr_get[1]))){ 
					if($arr_get[1]=='last'){
						//última revisión
						$sql = "SELECT nomenclatura_history.aprobado, 
						    nomenclatura_history.obs_generales, 
							nomenclatura_history.imgs_generales,
							obs_generales_entrega.obs 
							FROM nomenclatura_history, obs_generales_entrega  
                            WHERE nomenclatura_history.idapto=obs_generales_entrega.idapto and 
							nomenclatura_history.idapto=".$arr_get[0]." 
							group by obs_generales_entrega.obs order by obs_generales_entrega.fecha 
							desc limit 0,1";
					}else{
						$sql = "SELECT aprobado, obs_generales, imgs_generales FROM nomenclatura_history 
                            WHERE idapto=".$arr_get[0]." and fecha='".$arr_get[1]."'";
					}
		}		else{ 
					
					$sql = "SELECT aprobado, entregado, obs_generales, imgs_generales 
							FROM nomenclatura_oficial WHERE id=".$arr_get[0];
				}
            
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return $stmt;

            break;
            case 'seccaracs':
                
                $sec=$arr_get[0];
                $fecha=$arr_get[1];
                $idapto=$arr_get[2];
                $tabla=$arr_get[3];
                $sql="";
				$stmt="";
                //echo 'tabla->'.$tabla;
                if($tabla=='obs_entrega'){
                    $sql="SELECT * FROM obs_entrega WHERE idapto=".$idapto;
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    if($stmt->rowCount()>0){
                        
						$sql="SELECT secciones_caracs.*, obs_entrega.obs,obs_entrega.obs2,  
                                          obs_entrega.aprobado, obs_entrega.fecha  
                                          FROM secciones_caracs, obs_entrega 
                                          WHERE obs_entrega.caracs=secciones_caracs.id 
                                          and obs_entrega.idapto=".$idapto." 
										  and obs_entrega.custom=0 
										  and secciones_caracs.seccion=".$sec." 
										  group by secciones_caracs.id";
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
                    }else{
                        $sql="select * from secciones_caracs where seccion=".$sec;
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
                    }
                }else{
                    $sql="SELECT secciones_caracs.*, revision_apto.obs,revision_apto.obs2, revision_apto.imgs, 
                                          revision_apto.aprobado, revision_apto.fecha  
                                          FROM secciones_caracs, revision_apto 
                                          WHERE revision_apto.carac=secciones_caracs.id 
                                          and revision_apto.idapto=".$idapto." 
										  and revision_apto.custom=0 
										  and secciones_caracs.seccion=".$sec." 
										  group by secciones_caracs.id";
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
					$rows=$stmt->rowCount();
					
					if($rows==0){
						//SELECCIONAR TIPO DESDE SECCIONES
						/*$sql="SELECT tipo FROM secciones WHERE id=".$sec;
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
						$idtipo="";
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
							$idtipo=$row['id'];
						}*/
						//SELECCIONAR
						
						$sql="SELECT id, caracs FROM secciones_caracs 
							  where seccion=".$sec;
						$rows0 = $this->conn->prepare($sql);
						$rows0->execute();
						$rows=$rows0->rowCount();
						$arr_idc=array();
						//echo 'rowss->'.$rows;
						//echo print_r($rows0->fetch(PDO::FETCH_ASSOC),1);
						while ($row = $rows0->fetch(PDO::FETCH_ASSOC)){
							array_push($arr_idc, $row['id']);
						}
						for($x=0;$x<count($arr_idc);$x++){
							$sql="INSERT INTO revision_apto(idapto,carac) 
								  VALUES(".$idapto.", ".$arr_idc[$x].")";
							/*$sql="UPDATE revision_apto SET idapto=".$idapto." 
								  WHERE carac=".$arr_idc[$x];*/
							$stmt = $this->conn->prepare($sql);
							$stmt->execute();
						}
						
						//VOLVEMOS A CONSULTAR
						$sql="SELECT secciones_caracs.*, revision_apto.obs,revision_apto.obs2, revision_apto.imgs, 
                                          revision_apto.aprobado, revision_apto.fecha  
                                          FROM secciones_caracs, revision_apto 
                                          WHERE revision_apto.carac=secciones_caracs.id 
                                          and revision_apto.idapto=".$idapto." 
										  and revision_apto.custom=0 
										  and secciones_caracs.seccion=".$sec." 
										  group by secciones_caracs.id";
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}
                }
                if(empty($fecha)) $sql = $sql;
                
                else{ 
                    $sql="";
					if($fecha=='last'){ //última revisión
						$sql = "SELECT secciones_caracs.caracs, obs_entrega.* 
							   FROM secciones_caracs, obs_entrega 
							   WHERE secciones_caracs.id=obs_entrega.caracs 
							   AND secciones_caracs.seccion=".$sec." 
							   AND obs_entrega.idapto=".$idapto;
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}else{
						$arrfechatime=explode(' ', $fecha);
						$arrfecha=explode('-', $arrfechatime[0]);
						$arrhora=explode(':', $arrfechatime[1]);
						$sql="SELECT secciones_caracs.caracs, secciones_caracs_history.* 
							   FROM secciones_caracs, secciones_caracs_history 
							   WHERE secciones_caracs.id=secciones_caracs_history.carac AND 
							   year(secciones_caracs_history.fecha)='".$arrfecha[0]."' and 
							   month(secciones_caracs_history.fecha)='".$arrfecha[1]."' and 
							   day(secciones_caracs_history.fecha)='".$arrfecha[2]."' and 
							   hour(secciones_caracs_history.fecha)='".$arrhora[0]."' and 
							   minute(secciones_caracs_history.fecha)='".$arrhora[1]."'   
							   AND secciones_caracs.seccion=".$sec." 
							   AND secciones_caracs_history.custom=0 
							   AND secciones_caracs_history.idapto=".$idapto;
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}
                } 
                

                return $stmt;

            break;
            case 'seccaracs1':
                
               $sql = "SELECT * FROM secciones_caracs WHERE seccion=".$arr_get; 
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();

                return $stmt;

            break;
            case 'seccaracsc':
                
                $sec=$arr_get[0];
                $fecha=$arr_get[1];
                $idapto=$arr_get[2];
                $centrega=$arr_get[3];
				$tabla=($centrega==1)?'obs_entrega':'revision_apto';
				$obs_entrega=''; //me dice si hay obs de entrega o no
                //echo "tabla->".$tabla;
				if(empty($fecha)){ 
					$imgs=($tabla=='obs_entrega')?'':$tabla.".imgs,"; //las obs de entrega no tienen imgs
					//Si estamos en entregas, verifico si hay observ. en entregas
					if($tabla=='obs_entrega'){
						$sql="SELECT caracs_custom.*, obs_entrega.obs, obs_entrega.obs2, obs_entrega.fecha  
							  where obs_entrega.carac=caracs_custom.id 
							  and obs_entrega.idapto=".$idapto;
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
						$obs = $stmt->rowCount();
                        $obs_entrega = ($obs>0)?1:0;
						//si no hay obs en entregas
						if($obs_entrega==0) $sql="SELECT * FROM caracs_custom WHERE idapto=".$idapto;
					}

					else{
						$sql = "SELECT caracs_custom.*, ".$tabla.".obs, ".$imgs."  
                                ".$tabla.".aprobado 
                                FROM caracs_custom, ".$tabla." 
                                WHERE ".$tabla.".carac=caracs_custom.id 
                                and ".$tabla.".idapto=".$idapto." 
								and ".$tabla.".custom=1 
								and caracs_custom.seccion=".$sec;
							
					}
                
				}else{ 
                    $sql="";
					if($fecha=='last'){ //última revisión
						$sql = "SELECT secciones_caracs.caracs, obs_entrega.* 
							   FROM secciones_caracs, obs_entrega 
							   WHERE secciones_caracs.id=obs_entrega.carac 
							   AND secciones_caracs.seccion=".$sec." 
							   AND obs_entrega.idapto=".$idapto."  
							   order by obs_entrega.fecha desc limit 0,1";
					}else{
						$arrfechatime=explode(' ', $fecha);
						$arrfecha=explode('-', $arrfechatime[0]);
						$arrhora=explode(':', $arrfechatime[1]);
						$sql="SELECT caracs_custom.caracs, secciones_caracs_history.* 
							   FROM caracs_custom, secciones_caracs_history 
							   WHERE caracs_custom.id=secciones_caracs_history.carac AND 
							   year(secciones_caracs_history.fecha)='".$arrfecha[0]."' and 
							   month(secciones_caracs_history.fecha)='".$arrfecha[1]."' and 
							   day(secciones_caracs_history.fecha)='".$arrfecha[2]."' and 
							   hour(secciones_caracs_history.fecha)='".$arrhora[0]."' and 
							   minute(secciones_caracs_history.fecha)='".$arrhora[1]."'   
							   AND caracs_custom.seccion=".$sec." 
							   AND secciones_caracs_history.custom=1 
							   AND secciones_caracs_history.idapto=".$idapto;
					}
                } 
                
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return $stmt;

            break;
            case 'getvisita':
                /*$sql = "SELECT * FROM visitas WHERE idp=".$arr_get['idp']." and 
			            user=".$arr_get['iduser']." and idapto=".$arr_get['idapto'];*/
				$sql="SELECT aprobado FROM nomenclatura_oficial 
					  WHERE id=".$arr_get['idapto']." and aprobado=1";
                $stmt = $this->conn->prepare($sql);
            
                // execute query
                $stmt->execute();

                return $stmt;

            break;
            case 'readxls':
                $sql="";
                $tablar="";
                $arr_tablar=array();

                //consultar si el apto ya tiene reformas en bd (si tiene aunq sea una revisión)
                $tablare="";
                if(isset($arr_get['entrega'])) $tablare='reformas_entrega';
                if(isset($arr_get['fecha'])) $tablare='reformas_h';
				if(!(isset($arr_get['entrega'])) and !(isset($arr_get['fecha']))) $tablare='reformas';
				
				$contador=1;
				//saber cuantos indices (reformas) hay
				$sql="SELECT indice FROM ".$tablare." WHERE idapto=".$arr_get['idapto']." group by indice";
				$stmti = $this->conn->prepare($sql);
                $stmti->execute();
                $indices=$stmti->rowCount();
				$conta=1;
				$conta_t=0;

				if($indices>0){
					//echo 'siiii';
					while ($row_i = $stmti->fetch(PDO::FETCH_ASSOC)){
                        extract($row_i);
						$sql=(isset($arr_get['fecha']))?"SELECT * FROM reformas_h WHERE 
								idapto=".$arr_get['idapto']." and 
								fecha='".$arr_get['fecha']."' 
								and indice=".$indice:"SELECT * FROM ".$tablare." WHERE idapto=".$arr_get['idapto']." and indice=".$indice;
						
						//echo $sql;
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
						$refs=$stmt->rowCount();
						$tablar="";
						
						if($refs>0){
							$theader='<table id="treforma'.$conta_t.'">
								<tr style="background: #eaeaea">
									<td colspan="5" style="text-align:center"><h3>Reformas '.$conta.'</h3></td>
								</tr>
								<tr class="trtop" data-tr="'.$contador.'" style="background: #eaeaea">
								<td class="tdhead"><h3>Aprobar</h3></td>
								<td class="tdhead"><h3>Descripción</h3></td>
								<td class="tdhead"><h3>Cantidad solicitada</h3></td>
								<td class="tdhead"><h3>Observaciones</h3></td>
								<td class="tdhead"><h3>Fotos <span class="delreforma" onclick="delReforma('.$arr_get['idapto'].');">delete</span></h3></td>
								<!--<td><h3>Estado</h3></td>-->
								</tr>';
							while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
								extract($row);
								$imgs_caracs='';
								$arr_imgsc='';
								$count_imgs=0;

								if(!(empty($fotos))){
									$arr_imgsc=explode(';', $fotos);
									for($x=0;$x<count($arr_imgsc);$x++){
										if(!(empty($arr_imgsc[$x]))){
											/*$imgs_caracs.='<img data-carac="'.$row2['id'].'" width="100%" id="fotosec'.$row2['id'].'" 
														src="'.$eldir.'uploads/revisiones/'.$arr_imgsc[$x].'" /><br>';*/
											$imgs_caracs.=$eldir.'uploads/revisiones/'.$arr_imgsc[$x].';';
											$count_imgs++;
										}
									}
								}
								$contador++;
								$estador=($aprobado==0)?'Pendiente':'Aprobado';
								
								$img_ok=($aprobado==0)?'<img id="ok1_1" width="20" src="img/ok.png" />':'<img class="activo" id="ok1_1" width="20" src="img/ok.png" />';
								
								//if(isset($arr_get['entrega'])) $img_ok='<img id="ok1_1" width="20" src="img/ok.png" />';
                                
                                $img_nok=($aprobado==0)?'<img class="activo" id="nok1_1" width="20" src="img/xx.png" />':'<img id="nok1_1" width="20" src="img/xx.png" />';
								
								//if(isset($_GET['centrega'])) $img_nok='<img id="nok1_1" width="20" src="img/xx.png" />';
								
								$aprobador=($aprobado==0)?0:1;
								$disabled=($aprobado==0 or isset($arr_get['entrega']))?'':'disabled="disabled"';
								
								//echo print_r($_GET,1);
								
								
								$observaciones_partes=explode("---", $observaciones);
								$tablar.='<tr class="trdata" data-id="'.$id.'"  data-tr="'.$contador.'">
									<td class="tddata" style="padding:10px"><div class="sec_state">'.$img_ok.$img_nok.'
									<div class="clear"></div></div></td>
									<td class="tddata" style="padding:10px">'.$descripcion.'
									<input type="hidden" class="tdesc" name="tdesc" id="tdesc'.$contador.'" value="'.$descripcion.'"></td>
									<td class="tddata" style="padding:10px">'.$cantidad.'
									<input type="hidden" class="tcant" name="tcant" id="tcant'.$contador.'" value="'.$cantidad.'"></td>
									<td class="tddata" style="padding:10px"><div class="input-field">
									<textarea placeholder="Observaciones de la visita..." 
									 '.$disabled.' 
									class="obsr materialize-textarea" 
									name="obsr" >'.$observaciones_partes[0].'</textarea>
									<textarea placeholder="Observaciones pendientes..." 
									 '.$disabled.' 
									class="obsrp materialize-textarea" 
									name="obsrp" >'.$observaciones_partes[1].'</textarea><label></label></div>
									<input type="hidden" class="descr" name="descr" />
									<input type="hidden" name="cantr" class="cantr" />
									<input type="hidden" name="fotosr" class="fotosr" />
									<input type="hidden" name="aprobado" class="aprobado" value="'.$aprobador.'" />
									</td>
									<td class="tddata">
									<!--<div class="cont_imgs" datac="'.$id.'" 
										id="cont_imgs'.$id.'" value="" onclick="openImgs('.$id.');">+'.$count_imgs.'</div>-->
										<div class="cont_imgs" datac="'.$id.'" 
										id="cont_imgs'.$id.'" value="">+'.$count_imgs.'</div>
										<input type="hidden" name="imgs_old" id="imgs_old'.$id.'" value="'.$imgs_caracs.'" />
										<input type="hidden" name="imgs" id="imgs'.$id.'" value="" />
										<input type="hidden" name="imgs_uris" id="imgs_uris'.$id.'" value="" />
										<input type="hidden" name="countimg" id="countimg'.$id.'" value="'.$count_imgs.'" />
									<!--<img class="lacamera" onclick="tomarFoto('.$id.', 1);" id ="foto'.$id.'" src="img/camera.png" width="20%">-->
									<img class="lacamera" id ="foto'.$id.'" src="img/camera.png" width="20%"></td>
									<!--<td style="padding:10px">'.$estador.'</td>-->
								</tr>';
							}
							$tablar=$theader.$tablar.'</table>';
							$conta_t++;
							array_push($arr_tablar, $tablar);
							}
						$conta++;
					}
                
                }else $sql="SELECT reforma FROM nomenclatura_oficial WHERE id=".$arr_get['idapto'];
				//echo 'sql->'.$sql;
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                return array($stmt, $arr_tablar);
            break;
            case 'allrevs':
                $filtro="";
                
                if(isset($arr_get['from'])){
                    $from=str_replace('/', '-', $arr_get['from']);
                    $to=str_replace('/', '-', $arr_get['to']);

                    $from = strtotime($from);
                    $from = date('Y-m-d',$from);
                    $to = strtotime($to);
                    $to = date('Y-m-d',$to);
                    $to.=' 23:0:0';

                    //$arr_from=explode('/', $from);
                    //$arr_to=explode('/', $to);
                    /*$diafrom=$arr_from[0];
                    $mesfrom=$arr_from[1];
                    $aniofrom=$arr_from[2];
                    $diato=$arr_to[0];
                    $mesto=$arr_to[1];
                    $anioto=$arr_to[2];*/
                    $filtro=" and fecha between '".$from."' and '".$to."'";
                }
                $sql="SELECT id, idapto, aprobado, fecha FROM nomenclatura_history 
                      WHERE idapto=".$arr_get['idapto'].' '.$filtro." group by fecha";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                return $stmt;
            break;
        }
    }

    function cropAlign($image, $cropWidth, $cropHeight, $horizontalAlign = 'center', $verticalAlign = 'middle') {
        $width = imagesx($image);
        $height = imagesy($image);
        $horizontalAlignPixels = $this->calculatePixelsForAlign($width, $cropWidth, $horizontalAlign);
        $verticalAlignPixels = $this->calculatePixelsForAlign($height, $cropHeight, $verticalAlign);
        return imageCrop($image, [
            'x' => $horizontalAlignPixels[0],
            'y' => $verticalAlignPixels[0],
            'width' => $horizontalAlignPixels[1],
            'height' => $verticalAlignPixels[1]
        ]);
    }

    function calculatePixelsForAlign($imageSize, $cropSize, $align) {
        switch ($align) {
            case 'left':
            case 'top':
                return [0, min($cropSize, $imageSize)];
            case 'right':
            case 'bottom':
                return [max(0, $imageSize - $cropSize), min($cropSize, $imageSize)];
            case 'center':
            case 'middle':
                return [
                    max(0, floor(($imageSize / 2) - ($cropSize / 2))),
                    min($cropSize, $imageSize),
                ];
            default: return [0, $imageSize];
        }
    }

    function resize_image($file, $w, $h, $crop=FALSE) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width-($width*abs($r-$w/$h)));
            } else {
                $height = ceil($height-($height*abs($r-$w/$h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }

    function getWidth($img) {

      return imagesx($img);
   }
   
   function getHeight($img) {

      return imagesy($img);
   }

    // create product
    function create($accion, $arr_post, $files=''){
        $uploads='../../';
        switch($accion){
            case 'subirfirmas':
				$base1=$_POST['imgBase64'][0];
				$base2=$_POST['imgBase64'][1];
				$firma1="";
				$firma2="";
				
				if(!(empty($base1)) and !(empty($base2))){
					$img = str_replace('data:image/png;base64,', '', $base1);
					$img = str_replace(' ', '+', $img);
					$fileData = base64_decode($img);
					//saving
					$firma1 = $this->generateRandomString().'.png';
					file_put_contents("../../uploads/firmas/".$firma1, $fileData);
					
					$img = str_replace('data:image/png;base64,', '', $base2);
					$img = str_replace(' ', '+', $img);
					$fileData = base64_decode($img);
					//saving
					$firma2 = $this->generateRandomString().'.png';
					file_put_contents("../../uploads/firmas/".$firma2, $fileData);
					
					$sql="DELETE FROM campos_entrega WHERE idapto=".$arr_post['idapto'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
					
					$sql="INSERT INTO campos_entrega(idapto, firma1, firma2) 
						  VALUES(".$arr_post['idapto'].", '".$firma1."', '".$firma2."')";
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}
				return array('res' => 1);
				//echo print_r($_POST,1);
			break;
			case 'fentrega':
				//echo print_r($_POST,1);
				$sql="SELECT firma1, firma2 FROM campos_entrega 
					  WHERE idapto=".$arr_post['idapto'];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				$f1="";
				$f2="";
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
					$f1=$firma1;
					$f2=$firma2;
				}
				
				//GENERAR PDF
				require "../mpdf/vendor/autoload.php";
				//$cargar=load();
			    // $cargar->library('M_pdf');
				$mpdf = $this->load([
					'mode' => 'utf-8',
					'format' => 'A4'
				]);
				
				$pag1 = file_get_contents("../../formato1.html");
				$pag1=str_replace("#nproyecto", $arr_post['proyecto'], $pag1);
				$pag1=str_replace("#fecha", $arr_post['dias']."/".$arr_post['mes']."/".$arr_post['anio'], $pag1);
				$pag1=str_replace("#propi", $arr_post['propi'], $pag1);
				$pag1=str_replace("#ced", $arr_post['ced'], $pag1);
				$pag1=str_replace("#apto", $arr_post['apto'], $pag1);
				$pag1=str_replace("#torre", $arr_post['torre'], $pag1);
				$pag1=str_replace("#parqueadero", $arr_post['parq'], $pag1);
				$pag1=str_replace("#util", $arr_post['util'], $pag1);
				$pag1=str_replace("#dir", $arr_post['dir'], $pag1);
				$pag1=str_replace("#mun", $arr_post['mun'], $pag1);
				$pag1=str_replace("#energia", $arr_post['energia'], $pag1);
				$pag1=str_replace("#acueducto", $arr_post['acue'], $pag1);
				$pag1=str_replace("#gas", $arr_post['gas'], $pag1);
				
				$pag2 = file_get_contents("../../formato2.html");
				$pag2=str_replace("#ing", $arr_post['ing'], $pag2);
				$pag2=str_replace("#ced", $arr_post['ced'], $pag2);
				$pag2=str_replace("#firma1", $f1, $pag2);
				$pag2=str_replace("#firma2", $f2, $pag2);
				$pag2=str_replace("#nproyecto", $arr_post['proyecto'], $pag2);
				$pag2=str_replace("#mun2", $arr_post['mun2'], $pag2);
				$pag2=str_replace("#dias", $arr_post['dias'], $pag2);
				$pag2=str_replace("#mes", $arr_post['mes'], $pag2);
				$pag2=str_replace("#anio", $arr_post['anio'], $pag2);
				$pag2=str_replace("#fecha", $arr_post['dias']."/".$arr_post['mes']."/".$arr_post['anio'], $pag2);
				
				$mpdf->WriteHTML($pag1);
				$mpdf->AddPage();
				$mpdf->WriteHTML($pag2);
                $npdf='ENTREGA_INMUEBLE_'.mt_rand().'.pdf';
                $mpdf->Output('../../uploads/formatos_entrega/'.$npdf,'F');
				
				$sql="UPDATE campos_entrega SET 
						apto='".$arr_post['apto']."',
						propietario='".$arr_post['propi']."',
						cedula=".$arr_post['ced'].",
						torre='".$arr_post['torre']."',
						parqueadero='".$arr_post['parq']."',
						c_util='".$arr_post['util']."',
						proyecto='".$arr_post['proyecto']."',
						direccion='".$arr_post['dir']."',
						municipio='".$arr_post['mun']."',
						energia='".$arr_post['energia']."',
						acueducto='".$arr_post['acue']."',
						gas='".$arr_post['gas']."',
						municipio2='".$arr_post['mun2']."',
						ingeniero='".$arr_post['ing']."',
						archivo='".$npdf."',
						fecha='".$arr_post['dias']." de ".$arr_post['mes']." del ".$arr_post['anio']."'
				WHERE idapto=".$arr_post['idapto'];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
					
				return array('res' => 1);
			break;
			case 'mailentrega':
				
				if(!(empty($arr_post['email']))){
					//seleccionar archivo de entrega
					$sql="SELECT archivo, apto, proyecto 
							FROM campos_entrega 
							WHERE idapto=".$arr_post['idapto'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
					$mapto="";
					$mproy="";
					$acta="";
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row);
						$mapto=$apto;
						$mproy=$proyecto;
						$acta=$archivo;
					}
					
					//Para y asunto del mensaje a enviar
					$actasemail='actasentrega@constructoracapital.com';
					//$actasemail='actasentrega@constructoracapital.com';
					$email_to = $arr_post['email'].','.$actasemail; //$arr_post['email']
					$email_subject = "ACTA DE ENTREGA PARA APTO ".$mapto." PROYECTO ".$mproy;
						
					// print_r("var1: ".$nombreArchivo." var2:".$tamanioArchivo."  var3:".$tipoArchivo."  var4:".$tempArchivo);exit;
					 //print_r($_FILES);exit;
					$error_message = "";
					$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
					$string_exp = "/^[A-Za-z .'-]+$/";

					//variables para los datos del archivo 
					$nombrearchivo = $acta;
					$archivo = '../../uploads/formatos_entrega/'.$acta;
					
					$nombrearchivo2 = $arr_post['pdfruta'];
					$archivo2 = '../../'.$arr_post['pdfruta'];
					
					$links_pdf='<a target="_blank" href="http://200.122.212.101/uploads/formatos_entrega/'.$acta.'">'.$acta.'</a><br>
								<a target="_blank" href="http://200.122.212.101/'.$arr_post['pdfruta'].'">ARCHIVO DE REVISIÓN</a>';
					
					//enviamos el email
					//if($this->multi_attach_mail($email_to, $email_subject, '', 'info@capital.com', 'Constructora Capital', array($archivo, $archivo2))) return array('res' => 1);
					//else return array('res' => 0);
					$msg='Hola, a continuación te proporcionamos los documentos correspondientes 
						  a la entrega de tu inmueble<br><br>'.$links_pdf;
					$cabeceras = 'MIME-Version: 1.0' . "\r\n";
					$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$cabeceras .= 'From: Constructora Capital<actasentrega@constructoracapital.com>';
					
					if(mail($email_to, $email_subject, $msg, $cabeceras)) return array('res' => 1);
					else return array('res' => 0);
				}else return array('res' => 0);
				
			break;
			case 'delreforma':
				$indice=$arr_post['indice'];
				$sql="DELETE FROM reformas WHERE idapto=".$arr_post['idapto']." and indice=".$indice;
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				
				$sql="SELECT reforma FROM nomenclatura_oficial WHERE id=".$arr_post['idapto'];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				$reformaupd="";
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
					$arr_reformas=explode(',', $reforma);
					for($x=0;$x<count($arr_reformas);$x++){
						if(!(empty($arr_reformas[$x]))){
							//solo se almacenan las refomas restantes
							if($x != $indice) $reformaupd.=$arr_reformas[$x].',';
						}
					}
				}
				
				$sql="UPDATE nomenclatura_oficial set reforma='".$reformaupd."' WHERE id=".$arr_post['idapto'];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				return array('res'=>1);
			break;
			case 'nproyecto':
            $imagenp = '';
			$targetPath='';
			$arr_torres = array();
			if(!isset($arr_post['nuevatorre'])){
            if(isset($files["imagen"]["name"]) and !(empty($files["imagen"]["name"]))){
                switch($files['imagen']['type']){
                    case 'image/jpeg':
                        $files["imagen"]["name"]=str_replace(' ','',strtolower($arr_post['nombrep'])).'.jpg';
                        break;
                    case 'image/jpg':
                        $files["imagen"]["name"]=str_replace(' ','',strtolower($arr_post['nombrep'])).'.jpg';
                        break;
                    case 'image/JPG':
                        $files["imagen"]["name"]=str_replace(' ','',strtolower($arr_post['nombrep'])).'.jpg';
                        break;
                    case 'image/JPEG':
                        $files["imagen"]["name"]=str_replace(' ','',strtolower($arr_post['nombrep'])).'.jpg';
                        break;
                    case 'image/png':
                        $files["imagen"]["name"]=str_replace(' ','',strtolower($arr_post['nombrep'])).'.png';
                        break;
                }
                
                $sourcePath = $files['imagen']['tmp_name']; // Storing source path of the file in a variable
                $temporary = explode(".", $files["imagen"]["name"]);
                $file_extension = end($temporary);
                $files["imagen"]["name"]=$this->generateRandomString().'.'.$file_extension;
                $targetPath = $uploads."uploads/proyectos/".$files["imagen"]["name"]; // Target path where file is to be stored
                
				//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
                $eldir = getcwd();
                //echo $eldir;
                //echo 'laimg->'.$files["imagen"]["tmp_name"];
                move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
                $im="";
                $constante="";
                $tamano=getimagesize($targetPath);

                if ($tamano[0] == $tamano[1]) {
                    $constante = 1;
                }else {
                    if ($tamano[0] > $tamano[1]) {
                        $constante = $tamano[1] / $tamano[0];
                    }else {
                        $constante = $tamano[0] / $tamano[1];
                    }
                }
                $width_d = 250;
                $height_d = $width_d * round($constante); 
                
               $new_image=$this->resize_image($targetPath, $width_d, $height_d);
               imagejpeg($new_image, $targetPath);
               $im="";
                
                

               if($files['imagen']['type']=='image/jpeg') $im = imagecreatefromjpeg($targetPath);
               if($files['imagen']['type']=='image/jpg') $im = imagecreatefromjpeg($targetPath);
               if($files['imagen']['type']=='image/JPG') $im = imagecreatefromjpeg($targetPath);
               if($files['imagen']['type']=='image/JPEG') $im = imagecreatefromjpeg($targetPath);
               if($files['imagen']['type']=='image/png') $im = imagecreatefrompng($targetPath);

               if($files['imagen']['type']=='image/jpeg') imagejpeg($this->cropAlign($im, 200, 200, 'center', 'middle'), $targetPath);
               if($files['imagen']['type']=='image/jpg') imagejpeg($this->cropAlign($im, 200, 200, 'center', 'middle'), $targetPath);
               if($files['imagen']['type']=='image/JPG') imagejpeg($this->cropAlign($im, 200, 200, 'center', 'middle'), $targetPath);
               if($files['imagen']['type']=='image/JPEG') imagejpeg($this->cropAlign($im, 200, 200, 'center', 'middle'), $targetPath);
               if($files['imagen']['type']=='image/png') imagepng($this->cropAlign($im, 200, 200, 'center', 'middle'), $targetPath);
               
                $imagenp = $files['imagen']['name'];
            }else $imagenp = 'iproyecto.png';

            //echo 'imagen->'.$imagenp;
           // return false;
            $aptosxtorre=$arr_post['nxtorre'];
            $axtorre='';


            $sql = "INSERT INTO proyectos(nombre,n_aptos,n_torres,imagen,fecha_creacion)
            VALUES ('".$arr_post['nombrep']."',".$arr_post['naptos'].",".$arr_post['ntorres'].",
            '".$imagenp."',now())";

            // prepare query
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $idproyecto = $this->conn->lastInsertId();

            $sql = "INSERT INTO proyecto_complete(iduser, idp, estado, fechap)
            VALUES ('".$arr_post['iduser']."',".$idproyecto.",0, now())";

            // prepare query
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
		}
			
			//CUANDO SE AÑADE UNA NUEVA TORRE
			$torres_act=0;
			if(isset($arr_post['nuevatorre'])){
				//consultar cuantas torres tiene actualmente
				$sql="SELECT n_torres FROM proyectos WHERE 
					 id=".$arr_post['idp'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
				
				while ($rowt = $stmt->fetch(PDO::FETCH_ASSOC)){
					$torres_act+=$rowt['n_torres'];
				}
				//actualizar # de torres
				$sql="UPDATE proyectos SET n_torres=".$torres_act."  
					  WHERE id=".$arr_post['idp'];
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				
				//$sql="DELETE FROM torres_proyecto WHERE id_proyecto=".$arr_post['idp'];
				$sql="INSERT INTO torres_proyecto(id_proyecto, nombre)
					  values(".$arr_post['idp'].", '".$arr_post['nmtorre']."')";
				$stmt = $this->conn->prepare($sql);
				$stmt->execute();
				$idtorre=$this->conn->lastInsertId();
				array_push($arr_torres, $idtorre);
				
				$idproyecto=$arr_post['idp'];
			}

            //echo 'idproyecto->'.$idproyecto;

            //GUARDAR LAS TORRES
			if(!isset($arr_post['nuevatorre'])){
				 for($r=0;$r<$arr_post['ntorres'];$r++){
					
					$sql = "INSERT INTO torres_proyecto(id_proyecto, nombre)
					VALUES (".$idproyecto.",'".$arr_post['ntorre']."')";
					
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
					$idtorre=$this->conn->lastInsertId();
					array_push($arr_torres, $idtorre);
				}
			}

            //GUARDAR APTOS X TORRE
            /*for($x=0;$x<$aptosxtorre;$x++){
                if(!(empty( $arr_post['pisosxtorre'.$x] ))){
                    $sql = "INSERT INTO aptos_torre(id_proyecto, torre, naptos)
                    VALUES (".$idproyecto.",'Torre".($x+1)."',".$arr_post['pisosxtorre'.$x].")";
                    
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                }
            }*/
            // execute query
            if($idproyecto){
                return array( 'res' => 1, 'idp' => $idproyecto, 'idtorres' => $arr_torres, 'path' => $targetPath );
            }else return false;

            break;
            case 'ntipo':
                $idtipo='';
                if(!(isset($arr_post['idtipo']))){
                    //validar si se va a actualizar un tipo o no
					$sql="";
					if(isset($arr_post['tactual'])) $sql="UPDATE tipos_aptos SET nombre='".$arr_post['ntipo']."' WHERE id=".$arr_post['tactual'];
					else{ $sql = "INSERT INTO tipos_aptos(proyecto,nombre,fecha_creacion)
                            VALUES(".$arr_post['proyecto'].", '".$arr_post['ntipo']."', now())";
					}
                    
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $idtipo=(isset($arr_post['tactual']))?$arr_post['tactual']:$this->conn->lastInsertId();
                }
                if(isset($arr_post['desistir']) and isset($arr_post['idtipo'])){
                    $sql = "update nomenclatura_oficial set clase=".$arr_post['idtipo'].", desistido=2 
                            where id=".$arr_post['idapto'];
                    
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                }else{
                    if(isset($arr_post['idtipo']) or isset($arr_post['tipodesistido'])){
						$sql = "update nomenclatura_oficial set desistido=2  
								where id=".$arr_post['idapto'];
						
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}
                }
                if($idtipo){ 
                    return array( 'res' => 1, 'idtipo' => $idtipo );
                }else{
                    if(isset($arr_post['desistir'])){
                        return array( 'res' => 1, 'idtipo' => $arr_post['idtipo'] );
                    }else return false;
                }
            break;
            case 'nseccion':
			 ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
				$countsec=0;
                $sql="";
                $imagenp = $arr_post['rutaimg'];
                
				if(isset($arr_post['tipodesistido'])){
					$sql = "update nomenclatura_oficial set 
							clase=".$arr_post['idtipo1'].", desistido=2  
                            where id=".$arr_post['idapto'];
                    
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
				}
				
				if(isset($arr_post['custom'])){ 
                   $sql = "INSERT INTO secciones_custom(imagen,apto,seccion,fecha)
	                        VALUES ('".$imagenp."',".$arr_post['idapto'].",'".$arr_post['seccion']."',now())";
                }else{
                     //saber si la seccion ya existe para ese tipo
					 $sql="SELECT seccion FROM secciones WHERE 
					 seccion='".$arr_post['seccion']."' and tipo=".$arr_post['idtipo1'];
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $countsec=$stmt->rowCount();
                    if($countsec==0){
                     //saber si se va a actualizar o no
					 $sql="";
					 if(isset($arr_post['sactual']) and $arr_post['sactual'] != '') $sql="UPDATE secciones SET seccion='".$arr_post['seccion']."' WHERE id=".$arr_post['sactual'];
					 else{ 
						$sql = "INSERT INTO secciones(imagen,proyecto,tipo,seccion,fecha_creacion)
	                        VALUES ('".$imagenp."',".$arr_post['idproyecto1'].",".$arr_post['idtipo1'].",'".$arr_post['seccion']."',now())";
					 }
                    }else $sql="UPDATE secciones SET seccion='".$arr_post['seccion']."' WHERE tipo=".$arr_post['idtipo1'];
                }
				//echo $sql;
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
				$idseccion=0;
				if($countsec>0) $idseccion=0;
				else $idseccion=(isset($arr_post['sactual']) and $arr_post['sactual'] != '')?$arr_post['sactual']:$this->conn->lastInsertId();
                //seccion predefinida
                $sql="SELECT seccion FROM secciones_default WHERE seccion='".$arr_post['seccion']."'";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $countsec=$stmt->rowCount();
                if($countsec==0){
                    $sql = "INSERT INTO secciones_default(imagen,seccion,fecha_creacion)
                                VALUES ('".$imagenp."','".$arr_post['seccion']."',now())";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                }
                
                return array( 'res' => 1, 'idseccion' => $idseccion );
            break;
            case 'ncarac':
                if(!( isset($arr_post['custom']) )){
					$sql="delete from secciones_caracs WHERE seccion=".$arr_post['idseccion'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}else{
					$sql="delete from caracs_custom WHERE seccion=".$arr_post['idseccion'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}
                
                //capturar las características
                $n_caracs=$arr_post['ncaracs'];
                $caracs='';
				$sqlrev1="";
				$sqlrev2="";
                
                for($x=0;$x<$n_caracs;$x++){
                    if(!(empty( $arr_post['carac'.($x+1)] ))){
                        $caracs=$arr_post['carac'.($x+1)];
                        if(isset($arr_post['custom'])){ 
                            
							$sql = "INSERT INTO caracs_custom(seccion,caracs,fecha,idapto)
                                    VALUES (".$arr_post['idseccion'].",'".$caracs."',now(),".$arr_post['aptoactual'].")";
                        }else{
                            $sql = "INSERT INTO secciones_caracs(seccion,caracs,fecha_creacion)
                                    VALUES (".$arr_post['idseccion'].",'".$caracs."',now())";
                        }
                        
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute();
                        $idcarac=$this->conn->lastInsertId();

                        $escustom=(isset($arr_post['custom']))?1:0;
						$sql="";
						if($escustom==1){
							$sql="INSERT INTO revision_apto(carac, custom, idapto) 
								  VALUES(".$idcarac.", ".$escustom.", ".$arr_post['aptoactual'].")";
						}else{
							$sql="INSERT INTO revision_apto(carac, custom) 
								  VALUES(".$idcarac.", ".$escustom.")";
						}
                        $stmt = $this->conn->prepare($sql);
                        $stmt->execute();
                    }
                }

                return array('res' => 1);
            break;
			case 'delnomenc':
				$ids = $arr_post['ids'];
				for($x=0;$x<count($ids);$x++){
					$sql = "DELETE FROM nomenclatura_oficial WHERE id=".$ids[$x];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}
				return array('res' => 1);
			break;
            case 'savenomenc1':
                $sql = "INSERT INTO nomenclatura_oficial(idtorre,nomenclatura, fecha)
	                    VALUES (".$arr_post['idtorre'].",'".$arr_post['nomenc']."', now())";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                $idapto=$this->conn->lastInsertId();

                return array('res' => 1, 'idapto' => $idapto);
            break;
            case 'dotabla':
                $contador=0;
                $conttorres=0;
                $arr_nomenc=array();
                $arr_torres=array();
                $cadena_nom='';
                $nomen1=0;
                $nomen2=0;
                $elcount=0;

                $html_tabla='<table id="tablag"><tr>
                <td><label><input style="display:none" type="checkbox" id="checkall" value="1" onclick="checkAll(this.value);" /> 
                <span>Seleccionar / Deseleccionar Items</span></label></td>
                <td>Clase: <select id="tipoaptog" onchange="setClaseapto(this, 0);" name="tipoapto" 
                class="tipoapto"> </select></td>
                    <td><a href="javascript:void(0);" onclick="setClaseg($(\'#tipoaptog\').val());" class="btn">APLICAR</a></td>
                   </tr></table><br>
                <div style="height: 16rem;overflow:auto;"><table><tr><th>TORRE</th><th>APARTAMENTO</th>
                    <!--<th>CLASE DEL APARTAMENTO</th>-->
                    <th>AÑADIR REFORMAS</th>			
                    <!--<th>GUARDAR</th>--></tr>';			
                
                $maximo=0;
				if(isset($arr_post['ntorre'])){
					$sql="SELECT MAX(id) AS maximo FROM torres_proyecto WHERE id_proyecto=".$arr_post['idp'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();

					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						$maximo=$row['maximo'];
					}
				}
				
				$add=(isset($arr_post['ntorre']))?' and id='.$maximo:'';
				$sql = "SELECT * FROM torres_proyecto WHERE id_proyecto=".$arr_post['idp'].$add;
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
				$arr_nametorres=array();

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['name'] to
                    // just $name only
                    extract($row);
                    //almacenar ids de torres
		            array_push($arr_torres, $id);
		            array_push($arr_nametorres, $nombre);
                }

                for($x=0;$x<count($arr_torres);$x++){
		            $sql="SELECT COUNT(nomenclatura) as elcount FROM nomenclatura_oficial WHERE idtorre=".$arr_torres[$x];
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();

                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        // extract row
                        // this will make $row['name'] to
                        // just $name only
                        extract($row);
                        $elcount=$elcount; 
                    }

                    $sql="SELECT * FROM nomenclatura_oficial WHERE idtorre=".$arr_torres[$x].' order by nomenclatura asc';
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                    $j=0;

                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                        // extract row
                        // this will make $row['name'] to
                        // just $name only
                        extract($row);
                        $elcount=$elcount;

                        if($contador==0){ $html_tabla.='<tr>
                                    <td rowspan="'.$elcount.'"><b>'.strtoupper($arr_nametorres[$x]).'</b></td>
                                    <td class="checkitem" id="td'.$id.'" data-id="'.$id.'"><label>
                                    <input style="display:none" type="checkbox" value="'.$id.'" /> <span>
                                    </span></label> '.$nomenclatura.'<br><div class="tdclase"></div></td>
                                    <!--<td><select id="tipoapto'.$id.'" onchange="setClaseapto(this, '.$id.');" name="tipoapto" class="tipoapto">
                                    </select></td>-->
                                    <td><form data-id="'.$id.'" enctype="multipart/form-data" class="frm_reformas" 
                                    name="frm_reformas" id="frm_reformas'.$id.'" method="post" action="">
                                    <input type="hidden" name="idp" value="'.$arr_post['idp'].'" />
                                    <input type="hidden" name="idnomenc" value="'.$id.'" />
                                    <input type="hidden" name="clase" id="clase'.$id.'" />
                                    <input type="hidden" name="accion" value="savereforma" />
                                    
									<input onchange="alert(\'Archivos adjuntados con éxito\');" 
									type="file" class="file_reforma" 
                                    name="file_reforma[]" multiple="multiple" id="file_reforma'.$id.'" 
									data-id="'.$id.'" style="display:none" />
									
                                    <img onclick="$(\'#file_reforma'.$id.'\').click();" src="img/doc.png" width="35"></form>
                                    <div id="namer'.$id.'"></div></td>
                                    <!--<td><a id="btnreforma'.$id.'" class="btnreforma waves-effect waves-light btn modal-trigger" 
                                        href="javascript:void(0);" onclick="saveReformas('.$id.');">GUARDAR</a></td>-->
                                </tr>';
                        }else{
                            $html_tabla.='<tr>
                                    <td class="checkitem" id="td'.$id.'" data-id="'.$id.'"><label>
                                    <input style="display:none" type="checkbox" value="'.$id.'" /> <span>
                                    </span></label> '.$nomenclatura.'<br><div class="tdclase"></div>
                                    <!--<td><select id="tipoapto'.$id.'" onchange="setClaseapto(this, '.$id.');" name="tipoapto" class="tipoapto">
                                    </select></td>-->
                                    <td><form enctype="multipart/form-data" class="frm_reformas" name="frm_reformas" 
                                    id="frm_reformas'.$id.'" method="post" action="">
                                    <input type="hidden" name="idnomenc" value="'.$id.'" />
                                    <input type="hidden" name="clase" id="clase'.$id.'" />
                                    <input type="hidden" name="accion" value="savereforma" />
									
                                    <input onchange="alert(\'Archivos adjuntados con éxito\');" type="file" 
									class="file_reforma" 
                                    name="file_reforma[]" multiple="multiple"  
									id="file_reforma'.$id.'" style="display:none" />
									
                                    <img onclick="$(\'#file_reforma'.$id.'\').click();" src="img/doc.png" width="35"></form></td>
                                    <!--<td><a id="btnreforma'.$id.'" class="btnreforma waves-effect waves-light btn modal-trigger" 
                                        href="javascript:void(0);" onclick="saveReformas('.$id.');">GUARDAR</a></td>-->
                                </tr>';
                        }
                        $contador++;
                        $j++; 
                    }
                    $contador=0;
                }
                $html_tabla.='</table></div>';
                $html_tabla.='<p>&nbsp;</p>';
                $html_tabla.='<a class="waves-effect waves-light btn" 
                            href="javascript:void(0);" id="btnsaver" onclick="saveReformas();">GUARDAR</a>
                            <!--<a class="waves-effect waves-light btn" 
                            href="javascript:void(0);" id="btnfinish" onclick="location.reload();">TERMINAR</a>-->';
                
                return $arr_res = array( 'res' => 1, 'tabla' => $html_tabla );
            break;
            case 'tipos':
                $idp = $arr_post['idp'];
                $idtipo = '';
                $ntipo = '';
                $naptos = '';
                
                $sql = "SELECT * FROM tipos_aptos WHERE proyecto=".$idp;
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $idtipo .= $row['id'].';';
		            $ntipo .= $row['nombre'].';';
                }

                return $arr_res = array( 'res' => 1, 'idtipo' => $idtipo, 'ntipo' => $ntipo );
            break;
            case 'savenomenc':
                $sql = "INSERT INTO nomenclatura(idtorre,idproyecto,desde_piso,hasta_piso,naptos,nomenc1,nomenc2)
	                    VALUES (".$_POST['idtorre'].",".$_POST['idproyecto'].",".$_POST['desde'].",".$_POST['hasta'].",".$_POST['naptos'].",
                        '".$_POST['nm1']."','".$_POST['nm2']."')";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array('res' => 1);
            break;
            case 'savereforma':
                $reforma = '';
                $subido = true;
                if(isset($files["file_reforma"]["name"]) and ( is_array($files["file_reforma"]["name"]) and count( $files["file_reforma"]["name"] ) >0 ) ){
                    for($x=0;$x<count($files["file_reforma"]["name"]);$x++){
						if(!(empty( $files["file_reforma"]["name"][$x] ))){
							$temporary = explode(".", $files["file_reforma"]["name"][$x]);
							$file_extension = end($temporary);
							if($file_extension=='xls' or $file_extension=='xlsx'){ //solo excel
								$files["file_reforma"]["name"][$x]=$this->generateRandomString().'.'.$file_extension;
								$sourcePath = $files['file_reforma']['tmp_name'][$x]; // Storing source path of the file in a variable
								$targetPath = $uploads."uploads/proyectos/reformas/".$files["file_reforma"]["name"][$x]; // Target path where file is to be stored
								//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
								$subido=move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
								$reforma .= $files['file_reforma']['name'][$x].',';
							}
						}
					}
                }
                
                $sql = "UPDATE nomenclatura_oficial set clase=".$arr_post['clase'].", reforma='".$reforma."' 
			            where id=".$arr_post['idnomenc'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                if(isset($arr_post['idp'])){
					$sql = "delete from proyecto_complete where idp=".$arr_post['idp'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}

                if($subido) return array( 'res' => 1 );
	            else return array( 'res' => 0 );
            break;
            case 'setvisita':
	
                $sql = "INSERT INTO visitas(idp, user, idapto, fecha) 
                        VALUES(".$arr_post['idp'].", ".$arr_post['iduser'].", ".$arr_post['idapto'].", now())";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array( 'res' => 1 );
            break;
            case 'nuser':
	
                $sql = "INSERT INTO usuarios(nombre, email, pass, nivel) 
                        VALUES('".$arr_post['nombre']."', '".$arr_post['email']."', '".md5($arr_post['pass'])."', ".$arr_post['nivelu'].")";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array( 'res' => 1 );
            break;
            case 'nreforma':
                $reforma = '';
                $subido = true;
                if(isset($files["file_reforma"]["name"]) and !(empty( $files["file_reforma"]["name"] ))){
                    $temporary = explode(".", $files["file_reforma"]["name"]);
                    $file_extension = end($temporary);
                    $files["file_reforma"]["name"]=$this->generateRandomString().'.'.$file_extension;
                    if($file_extension=='xls' or $file_extension=='xlsx'){
						$sourcePath = $files['file_reforma']['tmp_name']; // Storing source path of the file in a variable
						$targetPath = $uploads."uploads/proyectos/reformas/".$files["file_reforma"]["name"]; // Target path where file is to be stored
						//$targetPath = "uploads/proyectos/".md5(date('d-m-Y H:m:s')).'.'.$file_extension; // Target path where file is to be stored
						$subido=move_uploaded_file($sourcePath,$targetPath); // Moving Uploaded file
						$reforma = $files['file_reforma']['name'];

						 $sql="SELECT reforma FROM nomenclatura_oficial 
							   WHERE id=".$arr_post['idapto'];
						 $stmt = $this->conn->prepare($sql);
						 $stmt->execute();
						 while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
							$reforma=$reforma.','.$row['reforma'];
						 }
						 
						 $sql = "UPDATE nomenclatura_oficial set reforma='".$reforma."' 
								where id=".$arr_post['idapto'];
						 $stmt = $this->conn->prepare($sql);
						 $stmt->execute();

						 /*$sql="DELETE FROM reformas WHERE idapto=".$arr_post['idapto'];
						 $stmt = $this->conn->prepare($sql);
						 $stmt->execute();*/
					}
                }

               return array( 'res' => 1, 'nreforma' => $reforma );
	        
            break;
            case 'estadocarac':
                $add="";
                $estadogen=$arr_post['estadogen'];
				
                if(isset($arr_post['obs'])) $add.="obs='".$arr_post['obs']."',obs2='".$arr_post['obs2']."',";
				
                if(isset($arr_post['imgs']) and !(empty($arr_post['imgs']))) $add.=" imgs='".$arr_post['imgs']."',";
                
                if(isset($arr_post['nvisita']) and !(isset( $arr_post['entrega']) )){ //solo registra historial si se confirma q es nueva visita
                    $sql="INSERT INTO secciones_caracs_history(carac,obs,obs2,imgs,aprobado,fecha, idapto,custom) 
                        VALUES(".$arr_post['idcarac'].",'".$arr_post['obs']."','".$arr_post['obs2']."','".$arr_post['imgs']."',
                        ".$arr_post['estado'].",now(), ".$arr_post['idapto'].", ".$arr_post['custom'].")";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();
                }
                
                $tabla=(isset($arr_post['entrega']))?'obs_entrega':'revision_apto';
				
				//consultar si hay observ. de entrega
				if(isset($arr_post['entrega'])){
					$sql="SELECT aprobado FROM obs_entrega WHERE idapto=".$arr_post['idapto'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
					$counto=$stmt->rowCount();
					if($counto==0){
						$sql="INSERT INTO obs_entrega(idapto,caracs,obs,aprobado,fecha,custom) 
							  VALUES(".$arr_post['idapto'].", ".$arr_post['idcarac'].", 
							  '".$arr_post['obs']."', ".$arr_post['estado'].", now(), 
							  ".$arr_post['custom'].")";
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}else{
						$sql="UPDATE obs_entrega SET aprobado=".$arr_post['estado'].", 
						  ".$add." fecha=now(), custom=".$arr_post['custom']."   
					      WHERE caracs=".$arr_post['idcarac'].' 
						  and idapto='.$arr_post['idapto']." 
						  and custom=".$arr_post['custom'];
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}
				}else{
					$sql="UPDATE ".$tabla." SET aprobado=".$arr_post['estado'].", 
						  ".$add." fecha=now()    
						  WHERE carac=".$arr_post['idcarac'].' and 
						  idapto='.$arr_post['idapto']." 
						  and custom=".$arr_post['custom'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}
                
                /*if(!(isset($arr_post['entrega']))){
					$sql="UPDATE secciones_caracs SET aprobado=".$arr_post['estado'].", 
						  ".$add." fecha_creacion=now()   
						  WHERE id=".$arr_post['idcarac'];
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}*/

                return array( 'res' => 1 );
            break;
            case 'estadoapto':
                $estadogen=$arr_post['estadogen'];
				$add="";
                if(!(empty($arr_post['obsg']))) $add.="obs_generales='".$arr_post['obsg']."',";
                if(isset($arr_post['imgs']) and !(empty($arr_post['imgs']))) $add.=" imgs_generales='".$arr_post['imgs']."',";
                
                
                //el estado varia si se está en entregas o no
				$campo_estado=(isset($arr_post['entrega']))?'entregado':'aprobado';
				$para_entrega=(isset($arr_post['entrega']))?1:$estadogen;
				$sql="UPDATE nomenclatura_oficial SET ".$add." fecha=now(), 
                      ".$campo_estado."=".$estadogen.", entrega=".$para_entrega." 
                      WHERE id=".$arr_post['idapto'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
				
				if(isset($arr_post['nvisita'])){ //solo registra historial si se confirma q es nueva visita
                    $sql="SELECT * FROM nomenclatura_history WHERE fecha=now()";
					$stmt = $this->conn->prepare($sql);
                    $stmt->execute();
					$rows=$stmt->rowCount();
					//solo inserta si la fecha es diferente (si no hay fechas repetidas)
					if($rows==0){
						$sql="INSERT INTO nomenclatura_history(entrega, clase, idapto, ".$campo_estado.", 
						reforma, 
						fecha, obs_generales, imgs_generales) 
						VALUES(".$para_entrega.",".$arr_post['tipoid'].",".$arr_post['idapto'].", 
						".$estadogen.", '".$arr_post['nreforma']."', now(), 
						'".$arr_post['obsg']."', '".$arr_post['imgs']."')";
						$stmt = $this->conn->prepare($sql);
						$stmt->execute();
					}
                }
				
				if(!(isset($arr_post['entrega'])) and $estadogen==1){
					//echo 'ids de c->'.count($arr_post['caracs']);
					//exit;
					if(isset($arr_post['caracs'])){
						for($x=0;$x<count($arr_post['caracs']);$x++){
							$sql="INSERT INTO obs_entrega(idapto, caracs) VALUES(".$arr_post['idapto'].", ".$arr_post['caracs'][$x].")";
							$stmt = $this->conn->prepare($sql);
							$stmt->execute();
						}
					}
				}

                return array( 'res' => 1 );
            break;
            case 'savetablar':
                $cont=$arr_post['cont'];
				$sql1="";
				$sql2="";
				$tabla=(isset($arr_post['entrega']))?'reformas_entrega':'reformas';
				
				//consultar si la tabla de reformas ya contiene data 
				//para el apto y el indice q se envía
				$sql="SELECT idapto FROM ".$tabla." 
					  WHERE idapto=".$arr_post['idapto']." and 
					  indice=".$arr_post['indice'];
				$stmt = $this->conn->prepare($sql);
                $stmt->execute();
				$refs=$stmt->rowCount();
				//si ya existe en bd puedo borrarla
				if($refs>0 and !(empty($arr_post['idref']))){
					/*$sql="DELETE FROM ".$tabla." 
						  WHERE idapto=".$arr_post['idapto']." 
						  AND indice=".$arr_post['indice'];
                    $stmt = $this->conn->prepare($sql);
                    $stmt->execute();*/
					$sql1="update ".$tabla." set aprobado=".$arr_post['estado'].", 
						   observaciones='".$arr_post['obsr']."'  
						   where id=".$arr_post['idref'];
					$stmt = $this->conn->prepare($sql1);
					$stmt->execute();
				}else{
					$sql1="INSERT INTO ".$tabla."(indice, idapto, descripcion, cantidad, valorx, valory, 
					  observaciones, fotos, aprobado, fecha) 
                      VALUES(".$arr_post['indice'].", ".$arr_post['idapto'].", '".$arr_post['desc']."', 
					  ".$arr_post['cant'].", '".$arr_post['vx']."', 
                      '".$arr_post['vy']."', '".$arr_post['obsr']."', '".$arr_post['imgs']."', 
					  ".$arr_post['estado'].", now())";
					$stmt = $this->conn->prepare($sql1);
					$stmt->execute();
				}

                if(isset($arr_post['nvisita'])){ //solo registra historial si se confirma q es nueva visita
                    $sql2="INSERT INTO reformas_h(idapto, indice, descripcion, cantidad, observaciones, fotos, aprobado, fecha) 
                        VALUES(".$arr_post['idapto'].", ".$arr_post['indice'].", 
							   '".$arr_post['desc']."', ".$arr_post['cant'].", 
						       '".$arr_post['obsr']."', '".$arr_post['imgs']."', 
							   ".$arr_post['estado'].", now())";
                    $stmt = $this->conn->prepare($sql2);
                    $stmt->execute();
                }

                return array( 'res' => 1, 's1' => $sql1, 's2' => $sql2 );
            break;
            case 'generales':
                $tabla=(isset($arr_post['entrega']))?'obs_generales_entrega':'obs_generales';
				if(!(empty($arr_post['obs'])) or !(empty($arr_post['fotos']))){
					$sql="INSERT INTO ".$tabla."(idapto, obs, imgs,  fecha) 
						  VALUES(".$arr_post['idapto'].", '".$arr_post['obs']."', 
						  '".$arr_post['fotos']."', now())";
					$stmt = $this->conn->prepare($sql);
					$stmt->execute();
				}

                return array( 'res' => 1 );
            break;
            case 'delproyecto':
                $sql="update proyectos set desactivado=1 WHERE id=".$arr_post['idp'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                $sql="delete from proyecto_complete WHERE idp=".$arr_post['idp'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                return array( 'res' => 1 );
            break;
            case 'delsec':
                $sql="delete from secciones WHERE id=".$arr_post['id'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                
                $sql="delete from secciones_caracs WHERE seccion=".$arr_post['id'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array( 'res' => 1 );
            break;
            case 'updsec':
                $sql="update secciones set seccion='".$arr_post['sec']."' WHERE id=".$arr_post['id'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array( 'res' => 1 );
            break;
             case 'updcaracs':
                $sql="delete from secciones_caracs WHERE seccion=".$arr_post['id'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();
                
                $sql="insert into secciones_caracs(caracs,seccion) values('".$arr_post['carac']."',".$arr_post['id'].")";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array( 'res' => 1 );
            break;
            case 'updtype':
                $sql="update tipos_aptos set nombre='".$arr_post['tipo']."' WHERE id=".$arr_post['id'];
                $stmt = $this->conn->prepare($sql);
                $stmt->execute();

                return array( 'res' => 1 );
            break;
            case 'getconfig';
            $sql = "SELECT proyecto_complete.fechap, proyecto_complete.idp, proyectos.nombre 
                    FROM proyecto_complete, proyectos  
                    WHERE proyectos.id=proyecto_complete.idp and proyecto_complete.iduser=".$arr_post['iduser'];
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $idp='';
            $namep='';
            if($stmt->rowCount() >0){
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $idp .= $row['idp'].';';
                    $namep .= $row['nombre'].';';
                }
                return array( 'res' => 1, 'idps' => $idp, 'names' => $namep );
            }else return array( 'res' => 0 );

        break;
        case 'desistir':
            $sql = "UPDATE nomenclatura_oficial set desistido=1, aprobado=0, entrega=0 where id=".$arr_post['apto'];
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
			
			$sql = "INSERT INTO desist_history(idapto, fecha) values(".$arr_post['apto'].", now())";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return array( 'res' => 1 );
        break;
        }
       
        // query to insert record
        
    
        // sanitize
        /*$this->name=htmlspecialchars(strip_tags($this->name));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->category_id=htmlspecialchars(strip_tags($this->category_id));
        $this->created=htmlspecialchars(strip_tags($this->created));
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":category_id", $this->category_id);
        $stmt->bindParam(":created", $this->created);*/
    
        
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
?>